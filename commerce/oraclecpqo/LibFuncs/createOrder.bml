/**
@name Create Order
@api createOrder
@summary Submit the Order Payload to JDE API
@attribute ShippingAddress1_t
@attribute _document_number
@attribute _part_desc
@attribute _system_buyside_id
@attribute _system_supplier_company_name
@attribute _system_user_job_title
@attribute _system_user_login
@attribute _transaction_customer_t_company_name
@attribute actionMessages_t
@attribute buildingLabel_l
@attribute businessUnit_t
@attribute carrierNumber_t
@attribute deliveryInstruction1small_t
@attribute deliveryInstruction2small_t
@attribute description2_l
@attribute freightMethod_t
@attribute integrationPartNumber_l
@attribute jobContactName_t
@attribute jobContactPhone_t
@attribute jobName_t
@attribute lineComment_l
@attribute netPrice_l
@attribute numRolls_l
@attribute orderDate_t
@attribute orderHistoryJSON_t
@attribute orderSubmitCount_t
@attribute orderTakenBy_t
@attribute orderedBy_t
@attribute partDescScrubbed_l
@attribute purchaseOrderNumber_t
@attribute requestedDeliveryDateOnly_t
@attribute requestedQuantity_l
@attribute rollLength_l
@attribute rollPlacementId_l
@attribute scheduledPickDate_t
@attribute sectionLabel_l
@attribute section_l
@attribute shipToName_t
@attribute shipToPartyID_t
@attribute shippingAddress2_t
@attribute shippingCity_t
@attribute shippingContact_t
@attribute shippingPhone_t
@attribute shippingStateMenu_t
@attribute shippingZip_t
@attribute soldToPartyID_t
@attribute tab_l
@attribute transactionID_t
@attribute warningMessages_t
@function Json createActionMessageJasonObject(String actionName, String actionStatus, String actionMessage)
@function String Dictionary getEnvironmentVariableValues(String siteName, String[] variables)
@function String sendAPIRequest(String urlType, String method, String requestBody)
@return String
@revision
Rev. Date |Developer            |Notes / Comments
----------|---------------------|-------------------------------------------------------------------------
2019-05-13|RLamsal              |Initial version
2019-10-15|JSerna               |Set create order request/response payload attrs.
2020-05-05|shansen              |Changed to an individual template for each line for flexibility.  Also sending positive credit for SP-153
2020-05-05|shansen              |Only include doors when door pricing is chosen
2020-06-24|shansen              |Changed to use a json structure so that order can be consistent with grid
*/
/* RLamsal - Created - May 13, 2019 */
ret = "";
envVars = String[]{"jdeUsername", "jdePassword"};
envVarDict = util.getEnvironmentVariableValues(_system_supplier_company_name, envVars);
orderStartFile = "$BASE_PATH$/Templates/OrderRequestStart.txt";
orderLineFile = "$BASE_PATH$/Templates/OrderRequestLine.txt";
orderEndFile = "$BASE_PATH$/Templates/OrderRequestEnd.txt";

requestId = _system_buyside_id + "-" + string(orderSubmitCount_t);
templateDict = dict("string");
if(len(requestedDeliveryDateOnly_t) > 0) {
	reqDelDate = strtojavadate(requestedDeliveryDateOnly_t, "yyyy-MM-dd");
	RequestedDeliveryDate = datetostr(reqDelDate, "yyyy-MM-dd");
	put(templateDict, "RequestedDeliveryDate", RequestedDeliveryDate);
}
OrderDate = datetostr(getdate(), "yyyy-MM-dd", "GMT-4");
put(templateDict, "Username", get(envVarDict, "jdeUsername"));
put(templateDict, "Password", get(envVarDict, "jdePassword"));
put(templateDict, "RequestId", requestId);
put(templateDict, "OrderDate", OrderDate);
put(templateDict, "orderTakenBy", _system_user_job_title);

put(templateDict, "deliveryInstruction1small", upper(deliveryInstruction1small_t));
put(templateDict, "deliveryInstruction2small", upper(deliveryInstruction2small_t));

put(templateDict, "orderedBy", upper(orderedBy_t));

jobName = upper(jobName_t);
if (orderJobName_t <> "") {
	jobName = upper(orderJobName_t);
}
put(templateDict, "ShippingAddress1", jobName);
put(templateDict, "shippingAddress2", upper(shippingAddress1_t));
put(templateDict, "ShippingAddress3", upper(ShippingAddress2_t));
put(templateDict, "shippingAddress4", upper(shippingContact_t) + " " + shippingPhone_t);
put(templateDict, "shippingCity", upper(shippingCity_t));
put(templateDict, "shipToName", upper(shipToName_t));




orderRequestStart = applytemplate(orderStartFile, templateDict);

orderRequestLineInfo = "";
//build line details out through bml so that we have more control
orderLinesJInfo = json();
for line in transactionLine{
	if(line.integrationPartNumber_l<>"" AND (NOT line.door_l OR includeDoorPricing_t)){
		lineInfo = dict("string");

		//static values
		put(lineInfo, "_document_number", line._document_number);
		put(lineInfo, "description2_l", line.description2_l);
		put(lineInfo, "integrationPartNumber_l", line.integrationPartNumber_l);
		put(lineInfo, "buildingLabel_l", line.buildingLabel_l);
		put(lineInfo, "sectionLabel_l", line.sectionLabel_l);

		if(line.lineComment_l<>""){
			put(lineInfo, "lineComment_l", line.lineComment_l);
		}

		if(line.numRolls_l<>""){
			put(lineInfo, "numRolls_l", line.numRolls_l);
		}

		if(line.isCreditLine_l){
			netPrice = fabs(line.netPrice_l);
			put(lineInfo, "netPrice_l", string(netPrice));
			put(lineInfo, "requestedQuantity_l", string(line.requestedQuantity_l * -1));
		}
		else{
			put(lineInfo, "netPrice_l", string(line.netPrice_l));
			put(lineInfo, "requestedQuantity_l", string(line.requestedQuantity_l));
		}

		if(line.rollLength_l<>""){
			put(lineInfo, "rollLength_l", line.rollLength_l);
		}

		if(line.rollPlacementId_l<>""){
			put(lineInfo, "rollPlacementId_l", line.rollPlacementId_l);
		}

		if(line.orderLineTabDescription_l<>""){
			put(lineInfo, "orderLineTabDescription_l", line.orderLineTabDescription_l);
		}
		orderRequestLineInfo = applytemplate(orderLineFile, lineInfo);
		jsonput(orderLinesJInfo, string(line._sequence_number), orderRequestLineInfo);
		//print orderRequestLineInfo;
		
	}
}
orderRequestLineInfo = "";
orderLineKeys = jsonkeys(orderLinesJInfo);
sort(orderLineKeys, "asc", "numeric"); //sort according to grid and now doc numbers
for lKey in orderLineKeys{
	lInfo = jsonget(orderLinesJInfo, lKey, "string", "");
	if(lInfo<>""){
		orderRequestLineInfo = orderRequestLineInfo + lInfo;
	}
}
orderRequest = orderRequestStart + orderRequestLineInfo + applytemplate(orderEndFile, templateDict);

orderResponse = util.sendAPIRequest("endPoint_Order", "POST", orderRequest);
//print orderResponse;
orderStatus = "SUCCESS";
warningMsg = "";
quoteStatus = "ORDERSUBMITTED";
if(startswith(orderResponse, "ERROR")) {
	orderStatus = "FAILED";
	quoteStatus = "ORDERFAILED";
	warningMsg = orderResponse;
}
else {
	xpaths = string[2];
	xpaths[0] = "//ns0:processSalesOrderResponse/status";
	xpaths[1] = "//ns0:processSalesOrderResponse/message";
	dictResp = readxmlsingle(orderResponse, xpaths);
	if(containskey(dictResp, xpaths[0])) {
		respStatus = get(dictResp, xpaths[0]);
		if(respStatus == "SUCCESS") {
			warningMsg = get(dictResp, xpaths[1]);
		}
		else {
			orderStatus = "FAILED";
			quoteStatus = "ORDERFAILED";
			warningMsg = get(dictResp, xpaths[1]);
		}
	}
	else {
		orderStatus = "FAILED";
		quoteStatus = "ORDERFAILED";
		warningMsg = "The Order Submit failed without any message";
	}

}
//ret = "1~warningMessages_t~" + warningMsg + "|";
orderJObj = json();
lineCount = 1;
if(orderHistoryJSON_t <> "") {
	orderJObj = json(orderHistoryJSON_t);
	lineCount = jsonget(orderJObj, "LineCount", "integer", 1) + 1; 
}
jsonput(orderJObj, "LineCount", lineCount);
lineObj = json();
jsonput(lineObj, "Action", "Order Submit");
jsonput(lineObj, "Date", datetostr(getdate()));
jsonput(lineObj, "User", _system_user_login);
jsonput(lineObj, "Quote Status", quoteStatus);
jsonput(lineObj, "Message", warningMsg);
jsonput(lineObj, "Payload", orderRequest);

jsonput(orderJObj, string(lineCount), lineObj);

actionmsg = util.createActionMessageJasonObject("Order Submit" , orderStatus, warningMsg);
//ret = ret + "1~actionMessages_t~" + jsontostr(actionmsg) + "|";
ret = ret + "1~warningMessages_t~" + warningMsg + "|";
ret = ret + "1~orderHistoryJSON_t~" + jsontostr(orderJObj) + "|";
ret = ret + "1~orderSubmitStatus_t~" + orderStatus + "|";
ret = ret + "1~status_t~" + quoteStatus + "|";
ret = ret + "1~oRCL_ERP_OrderStatus_t~" + quoteStatus + "|";
ret = ret + "1~jdeCreateOrderRequestPayload_t~" + orderRequest + "|";
ret = ret + "1~jdeCreateOrderResponsePayload_t~" + orderResponse + "|";
//print orderResponse;
return ret;