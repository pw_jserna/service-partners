<fo:table table-layout="fixed" float="center" border="1px solid #EE2E24">
	<fo:table-column column-width="100%"/>
		<fo:table-body>
		    <fo:table-row>
		        <fo:table-cell padding="10px 0px 0px 10px"> 
		            <fo:block text-align="center">
						<fo:inline color="#EE2E24" text-transform="uppercase" font-weight="bold" font-size="12pt" font-family="Helvetica">
						    <xsl:value-of select="$_dsMain1/outputSpecialNotationTitle_t"/>
						</fo:inline>
					</fo:block>  		        
			    </fo:table-cell>
		    </fo:table-row>
			<fo:table-row>
				<fo:table-cell padding="10px"> 
					<fo:block linefeed-treatment="preserve" white-space-treatment="preserve" white-space-collapse="false" line-height="12pt" text-align="left" background-color="transparent">
						<fo:inline font-size="12pt" font-family="Helvetica">
						<rte>
						    <xsl:apply-templates select="$_dsMain1/specialNotation_t"/>
						</rte>
						</fo:inline>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
		</fo:table-body>
</fo:table>