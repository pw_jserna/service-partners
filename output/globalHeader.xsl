<!-- Recursive template to display partition walls on cut sheet-->
<xsl:template name="partitionWallGroup">	
    <xsl:param name="i"/>   
    <xsl:param name="total"/>   
    <xsl:param name="buildingDocNum"/>    
    <xsl:param name="buildingName"/>
    
    <xsl:if test="$i &lt;= $total">    
        <xsl:variable name="sectionLabel" select="concat('A/W ', string($i))"/>        
        <xsl:variable name="pwRollTotal">
            <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and ./sectionLabel_l=concat('AW ', string($i))]">
                <q>
                    <xsl:value-of select="substring-before(./description2_l,' RL')"/>
                </q>
            </xsl:for-each>		
        </xsl:variable> 
        <xsl:variable name="pwTotalSF" select="sum(/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and ./sectionLabel_l=concat('AW ', string($i))]/requestedQuantity_l)"/>        
        <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and ./sectionLabel_l=concat('AW ', string($i))]">                  
            <fo:table-row>
                <fo:table-cell padding-right="30px" padding-left="10px">
                    <fo:block line-height="17pt" text-align="left">
                        <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">                            
                            <xsl:variable name="partDesc">
                                <xsl:choose>
                                    <xsl:when test="./partDescForOutputDoc_l!=''">
                                        <xsl:value-of select="./partDescForOutputDoc_l"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="./item_l/_part_desc"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:variable>
                            <xsl:choose>                                        
                                <xsl:when test="./outputCutSheetTabDescription_l!=''">
                                    <!-- <xsl:value-of select="concat(./item_l/_part_desc, ' ', ./tab_l)"/> " TAB -->
                                    <xsl:value-of select="concat($partDesc, ' ', ./outputCutSheetTabDescription_l)"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <!-- <xsl:value-of select="./item_l/_part_desc"/> -->
                                    <xsl:value-of select="$partDesc"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </fo:inline>
                    </fo:block>
                </fo:table-cell>
                <fo:table-cell>
                    <fo:block line-height="17pt" text-align="left" font-weight="bold">
                        <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">
                            <xsl:value-of select="concat(./description2_l, ' ', $buildingName, ' ', $sectionLabel)"/>
                        </fo:inline>
                    </fo:block>
                </fo:table-cell>
                <fo:table-cell padding-right="15px">
                    <fo:block line-height="17pt" text-align="right">
                        <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">
                            <xsl:value-of select="concat(format-number(./requestedQuantity_l,'###,###,###'), ' SF')"/>
                        </fo:inline>
                    </fo:block>
                </fo:table-cell>                
            </fo:table-row> 
            <xsl:if test="position()=last()">
                <fo:table-row>
                    <fo:table-cell padding-right="30px">
                        <fo:block line-height="17pt" text-align="right" font-weight="bold">
                            <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">
                                <xsl:value-of select="concat($sectionLabel, ' ', $buildingName, ' TOTAL')"/>
                            </fo:inline>
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell>
                        <fo:block line-height="17pt" text-align="left" font-weight="bold">
                            <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">
                            <xsl:value-of select="sum(exsl:node-set($pwRollTotal)/q)"/> Rolls</fo:inline>
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding-right="15px">
                        <fo:block line-height="17pt" text-align="right" font-weight="bold">
                            <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">
                            <xsl:value-of select="format-number($pwTotalSF,'###,###,###')"/> SF</fo:inline>
                        </fo:block>
                    </fo:table-cell>                
                </fo:table-row>               
                <fo:table-row>
                    <fo:table-cell>                            
                        <fo:block line-height="17pt">
                            <fo:inline> </fo:inline>
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell>
                        <fo:block line-height="17pt">
                            <fo:inline> </fo:inline>
                        </fo:block>
                    </fo:table-cell>
                    <fo:table-cell>
                        <fo:block line-height="17pt">
                            <fo:inline> </fo:inline>
                        </fo:block>
                    </fo:table-cell>                
                </fo:table-row>  
            </xsl:if>        
        </xsl:for-each>            
        <xsl:call-template name="partitionWallGroup">         				
            <xsl:with-param name="buildingDocNum" select="$buildingDocNum"/>
            <xsl:with-param name="buildingName" select="$buildingName"/>
            <xsl:with-param name="i" select="$i + 1"/>
            <xsl:with-param name="total" select="$total"/>            
        </xsl:call-template>	
    </xsl:if>
</xsl:template>	

<!-- Blank table row template -->
<xsl:template name="blankTableRow">
    <fo:table-row>
        <fo:table-cell>
            <fo:block line-height="17pt">
                <fo:inline> </fo:inline>
            </fo:block>
        </fo:table-cell>
        <fo:table-cell>
            <fo:block line-height="17pt">
                <fo:inline> </fo:inline>
            </fo:block>
        </fo:table-cell>
        <fo:table-cell>
            <fo:block line-height="17pt">
                <fo:inline> </fo:inline>
            </fo:block>
        </fo:table-cell>                
    </fo:table-row>	  
</xsl:template>