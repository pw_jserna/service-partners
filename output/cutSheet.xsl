    <fo:region-before extent="40pt" border-style="solid" border-width="medium"/>
    <fo:region-after extent="40pt" border-style="dotted" border-width="medium"/>
    <fo:region-start extent="5pt" border-style="solid" border-width="thin" reference-orientation="270"/>
    <fo:region-end extent="5pt" border-style="solid" border-width="thin" reference-orientation="270"/>
    <fo:region-body margin-top="56pt" margin-left="50pt" margin-right="50pt" margin-bottom="56pt" border-style="dashed" border-width="thin"/>
        <xsl:variable name="mainDoc" select="/transaction/data_xml/document[@document_var_name='transaction']"/>
        <xsl:variable name="subDoc" select="/transaction/data_xml/document"/>
        <!-- <xsl:variable name="initialValue" select="0"/> -->
        <!-- <xsl:variable name="totalSFValue" select="format-number($initialValue,'#')"/> -->
        
        <fo:table border-collapse="collapse" table-layout="fixed" float="center">
            <fo:table-column column-width="48%"/>
            <fo:table-column column-width="40%"/>
            <fo:table-column column-width="12%"/>
            <fo:table-body>
                <xsl:for-each select="/transaction/data_xml/document[normalize-space(./@data_type)='2' and ./_parent_doc_number='']">
                    <xsl:variable name="buildingDocNum" select="./_document_number"/>
                    <xsl:variable name="buildingName" select="./model_l/_model_name"/>

                    <!-- ROOF LINER - START -->
                    <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and contains(./section_l,'Roof') and ./partType_l = 'Liner']">
                        <fo:table-row>
                            <fo:table-cell padding-right="30px" padding-left="10px">
                                <fo:block line-height="17pt" text-align="left">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWDem">                                                                             
                                        <xsl:value-of select="./linerDesc_l"/>                                      
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell>
                                <fo:block line-height="17pt" text-align="left" font-weight="bold">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWBoo">
                                        <xsl:value-of select="./sectionLabel_l"/>                                       
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding-right="15px">
                                <fo:block line-height="17pt" text-align="right">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:value-of select="concat(format-number(./requestedQuantity_l,'###,###,###'), ' SF')"/>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>             
                    </xsl:for-each>
                    <!-- ROOF LINER - END -->
                    <!-- ROOF - START -->
                    <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and ./section_l='Roof']">
                        <fo:table-row>
                            <fo:table-cell padding-right="30px" padding-left="10px">
                                <fo:block line-height="17pt" text-align="left">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:variable name="partDesc">
                                            <xsl:choose>
                                                <xsl:when test="./partDescForOutputDoc_l!=''">
                                                    <xsl:value-of select="./partDescForOutputDoc_l"/>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:value-of select="./item_l/_part_desc"/>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:variable>
                                        <xsl:choose>                                        
                                            <xsl:when test="./outputCutSheetTabDescription_l!=''">
                                                <!-- <xsl:value-of select="concat(./item_l/_part_desc, ' ', ./tab_l)"/> " TAB -->
                                                <xsl:value-of select="concat($partDesc, ' ', ./outputCutSheetTabDescription_l)"/>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <!-- <xsl:value-of select="./item_l/_part_desc"/> -->
                                                <xsl:value-of select="$partDesc"/>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell>
                                <fo:block line-height="17pt" text-align="left" font-weight="bold">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWBoo">
                                        <xsl:value-of select="concat(./description2_l, ' ', $buildingName, ' ROOF')"/>
                                        <!-- <xsl:value-of select="concat(./description2_l, ' ', $buildingName, ./section_l)"/> -->
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding-right="15px">
                                <fo:block line-height="17pt" text-align="right">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:value-of select="concat(format-number(./requestedQuantity_l,'###,###,###'), ' SF')"/>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>             
                    </xsl:for-each>
                    
                    <xsl:variable name="finalValueRoofSF">
                        <xsl:value-of select="sum(/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and ./section_l='Roof']/requestedQuantity_l)"/>
                    </xsl:variable>
                    
                    <xsl:variable name="finalValueRollsRoof">
                        <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and ./section_l='Roof']">
                            <q>
                                <xsl:value-of select="substring-before(./description2_l,' RL')"/>
                            </q>
                        </xsl:for-each>     
                    </xsl:variable>
                    
                    <!-- ROOF TOTAL-->
                    <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and ./section_l='Roof']">
                        <xsl:if test="position()=last()">
                            <fo:table-row>
                                <fo:table-cell padding-right="30px">
                                    <fo:block line-height="17pt" text-align="right" font-weight="bold">
                                        <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">
                                            <xsl:value-of select="concat('ROOF ', $buildingName, ' TOTAL')"/>
                                        </fo:inline>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell>
                                    <fo:block line-height="17pt" text-align="left" font-weight="bold">
                                        <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:value-of select="sum(exsl:node-set($finalValueRollsRoof)/q)"/>
                                        Rolls</fo:inline>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell padding-right="15px">
                                    <fo:block line-height="17pt" text-align="right" font-weight="bold">
                                        <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:value-of select="format-number($finalValueRoofSF,'###,###,###')"/> SF</fo:inline>
                                    </fo:block>
                                </fo:table-cell>                
                            </fo:table-row>
                        
                            <!-- BLANK ROW -->
                            <xsl:call-template name="blankTableRow"/>                   
                        </xsl:if>
                    </xsl:for-each>
                    <!-- ROOF - END -->
                    <!-- ROOF-TOP LAYER - START -->
                    <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='Roof-Top Layer']">
                        <fo:table-row>
                            <fo:table-cell padding-right="30px" padding-left="10px">
                                <fo:block line-height="17pt" text-align="left">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:variable name="partDesc">
                                            <xsl:choose>
                                                <xsl:when test="./partDescForOutputDoc_l!=''">
                                                    <xsl:value-of select="./partDescForOutputDoc_l"/>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:value-of select="./item_l/_part_desc"/>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:variable>
                                        <xsl:choose>                                        
                                            <xsl:when test="./outputCutSheetTabDescription_l!=''">
                                                <!-- <xsl:value-of select="concat(./item_l/_part_desc, ' ', ./tab_l)"/> " TAB -->
                                                <xsl:value-of select="concat($partDesc, ' ', ./outputCutSheetTabDescription_l)"/>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <!-- <xsl:value-of select="./item_l/_part_desc"/> -->
                                                <xsl:value-of select="$partDesc"/>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell>
                                <fo:block line-height="17pt" text-align="left" font-weight="bold">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWBoo">
                                        <xsl:value-of select="concat(./description2_l, ' ', $buildingName, ' ROOF-TOP')"/>
                                        <!-- <xsl:value-of select="concat(./description2_l, ' ', $buildingName, ./section_l)"/> -->
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding-right="15px">
                                <fo:block line-height="17pt" text-align="right">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:value-of select="concat(format-number(./requestedQuantity_l,'###,###,###'), ' SF')"/>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>             
                    </xsl:for-each>
                    
                    <xsl:variable name="finalValueRoofTopSF">
                        <xsl:value-of select="sum(/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='Roof-Top Layer']/requestedQuantity_l)"/>
                    </xsl:variable>
                    
                    <xsl:variable name="finalValueRollsRoofTop">
                        <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='Roof-Top Layer']">
                            <q>
                                <xsl:value-of select="substring-before(./description2_l,' RL')"/>
                            </q>
                        </xsl:for-each>     
                    </xsl:variable>
                    
                    <!-- ROOF-TOP LAYER TOTAL-->
                    <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='Roof-Top Layer']">
                        <xsl:if test="position()=last()">
                            <fo:table-row>
                                <fo:table-cell padding-right="30px">
                                    <fo:block line-height="17pt" text-align="right" font-weight="bold">
                                        <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWDem">
                                            <xsl:value-of select="concat('ROOF-TOP ', $buildingName, ' TOTAL')"/>
                                        </fo:inline>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell>
                                    <fo:block line-height="17pt" text-align="left" font-weight="bold">
                                        <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:value-of select="sum(exsl:node-set($finalValueRollsRoofTop)/q)"/>
                                        Rolls</fo:inline>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell padding-right="15px">
                                    <fo:block line-height="17pt" text-align="right" font-weight="bold">
                                        <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:value-of select="format-number($finalValueRoofTopSF,'###,###,###')"/> SF</fo:inline>
                                    </fo:block>
                                </fo:table-cell>                
                            </fo:table-row>
                        
                            <!-- BLANK ROW -->
                            <xsl:call-template name="blankTableRow"/>                   
                        </xsl:if>
                    </xsl:for-each>
                    <!-- ROOF-TOP LAYER - END -->
                    <!-- ROOF-BOTTOM LAYER - START -->
                    <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='Roof-Bottom Layer']">
                        <fo:table-row>
                            <fo:table-cell padding-right="30px" padding-left="10px"> 
                                <fo:block line-height="17pt" text-align="left">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:variable name="partDesc">
                                            <xsl:choose>
                                                <xsl:when test="./partDescForOutputDoc_l!=''">
                                                    <xsl:value-of select="./partDescForOutputDoc_l"/>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:value-of select="./item_l/_part_desc"/>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:variable>
                                        <xsl:choose>                                        
                                            <xsl:when test="./outputCutSheetTabDescription_l!=''">
                                                <!-- <xsl:value-of select="concat(./item_l/_part_desc, ' ', ./tab_l)"/> " TAB -->
                                                <xsl:value-of select="concat($partDesc, ' ', ./outputCutSheetTabDescription_l)"/>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <!-- <xsl:value-of select="./item_l/_part_desc"/> -->
                                                <xsl:value-of select="$partDesc"/>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell>
                                <fo:block line-height="17pt" text-align="left" font-weight="bold">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWBoo">
                                        <xsl:value-of select="concat(./description2_l, ' ', $buildingName, ' ROOF-BOTTOM')"/>
                                        <!-- <xsl:value-of select="concat(./description2_l, ' ', $buildingName, ./section_l)"/> -->
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding-right="15px">
                                <fo:block line-height="17pt" text-align="right">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:value-of select="concat(format-number(./requestedQuantity_l,'###,###,###'), ' SF')"/>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>             
                    </xsl:for-each>
                    
                    <xsl:variable name="finalValueRoofBottomSF">
                        <xsl:value-of select="sum(/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='Roof-Bottom Layer']/requestedQuantity_l)"/>
                    </xsl:variable>
                    
                    <xsl:variable name="finalValueRollsRoofBottom">
                        <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='Roof-Bottom Layer']">
                            <q>
                                <xsl:value-of select="substring-before(./description2_l,' RL')"/>
                            </q>
                        </xsl:for-each>     
                    </xsl:variable>
                    
                    <!-- ROOF-BOTTOM LAYER TOTAL-->
                    <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='Roof-Bottom Layer']">
                        <xsl:if test="position()=last()">
                            <fo:table-row>
                                <fo:table-cell padding-right="30px">
                                    <fo:block line-height="17pt" text-align="right" font-weight="bold" padding-right="14px" padding-left="8px">
                                        <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">
                                            <xsl:value-of select="concat('ROOF-BOTTOM ', $buildingName, ' TOTAL')"/>
                                        </fo:inline>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell>
                                    <fo:block line-height="17pt" text-align="left" font-weight="bold">
                                        <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:value-of select="sum(exsl:node-set($finalValueRollsRoofBottom)/q)"/>
                                        Rolls</fo:inline>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell padding-right="15px">
                                    <fo:block line-height="17pt" text-align="right" font-weight="bold">
                                        <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:value-of select="format-number($finalValueRoofBottomSF,'###,###,###')"/> SF</fo:inline>
                                    </fo:block>
                                </fo:table-cell>                
                            </fo:table-row>
                        
                            <!-- BLANK ROW -->
                            <xsl:call-template name="blankTableRow"/>                   
                        </xsl:if>
                    </xsl:for-each>
                    <!-- ROOF-BOTTOM LAYER - END -->
                    <!-- SIDE WALL 1 - START -->
                    <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='SideWall1']">
                        <fo:table-row>
                            <fo:table-cell padding-right="30px" padding-left="10px">
                                <fo:block line-height="17pt" text-align="left">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:variable name="partDesc">
                                            <xsl:choose>
                                                <xsl:when test="./partDescForOutputDoc_l!=''">
                                                    <xsl:value-of select="./partDescForOutputDoc_l"/>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:value-of select="./item_l/_part_desc"/>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:variable>
                                        <xsl:choose>                                        
                                            <xsl:when test="./outputCutSheetTabDescription_l!=''">
                                                <!-- <xsl:value-of select="concat(./item_l/_part_desc, ' ', ./tab_l)"/> " TAB -->
                                                <xsl:value-of select="concat($partDesc, ' ', ./outputCutSheetTabDescription_l)"/>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <!-- <xsl:value-of select="./item_l/_part_desc"/> -->
                                                <xsl:value-of select="$partDesc"/>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell>
                                <fo:block line-height="17pt" text-align="left" font-weight="bold">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:value-of select="concat(./description2_l, ' ', $buildingName, ' S/W 1')"/>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding-right="15px">
                                <fo:block line-height="17pt" text-align="right">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:value-of select="concat(format-number(./requestedQuantity_l,'###,###,###'), ' SF')"/>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>                
                        </fo:table-row>
                    </xsl:for-each>
                    
                    <xsl:variable name="finalValueSideWall_1SF">
                        <xsl:value-of select="sum(/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='SideWall1']/requestedQuantity_l)"/>
                    </xsl:variable>
                    
                    <xsl:variable name="finalValueSideWall_1Roll">
                        <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='SideWall1']">
                            <q>
                                <xsl:value-of select="substring-before(./description2_l,' RL')"/>
                            </q>
                        </xsl:for-each>     
                    </xsl:variable>
                    
                    <!-- SIDE WALL 1 TOTAL-->
                    <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='SideWall1']">
                        <xsl:if test="position()=last()">
                            <fo:table-row>
                                <fo:table-cell padding-right="30px">
                                    <fo:block line-height="17pt" text-align="right" font-weight="bold">
                                        <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">
                                            <xsl:value-of select="concat('S/W 1 ', $buildingName, ' TOTAL')"/>
                                        </fo:inline>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell>
                                    <fo:block line-height="17pt" text-align="left" font-weight="bold">
                                        <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:value-of select="sum(exsl:node-set($finalValueSideWall_1Roll)/q)"/> Rolls</fo:inline>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell padding-right="15px">
                                    <fo:block line-height="17pt" text-align="right" font-weight="bold">
                                        <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:value-of select="format-number($finalValueSideWall_1SF,'###,###,###')"/> SF</fo:inline>
                                    </fo:block>
                                </fo:table-cell>                
                            </fo:table-row>
                            <!-- BLANK ROW -->
                            <xsl:call-template name="blankTableRow"/>                   
                        </xsl:if>
                    </xsl:for-each>
                    <!-- SIDE WALL 1 - END -->
                    <!-- SIDE WALL 2 - START-->
                    <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='SideWall2']">
                        <fo:table-row>
                            <fo:table-cell padding-right="30px" padding-left="10px">
                                <fo:block line-height="17pt" text-align="left">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:variable name="partDesc">
                                            <xsl:choose>
                                                <xsl:when test="./partDescForOutputDoc_l!=''">
                                                    <xsl:value-of select="./partDescForOutputDoc_l"/>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:value-of select="./item_l/_part_desc"/>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:variable>
                                        <xsl:choose>                                        
                                            <xsl:when test="./outputCutSheetTabDescription_l!=''">
                                                <!-- <xsl:value-of select="concat(./item_l/_part_desc, ' ', ./tab_l)"/> " TAB -->
                                                <xsl:value-of select="concat($partDesc, ' ', ./outputCutSheetTabDescription_l)"/>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <!-- <xsl:value-of select="./item_l/_part_desc"/> -->
                                                <xsl:value-of select="$partDesc"/>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell>
                                <fo:block line-height="17pt" text-align="left" font-weight="bold">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWDem" font-weight="bold">
                                        <xsl:value-of select="concat(./description2_l, ' ', $buildingName, ' S/W 2')"/>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding-right="15px">
                                <fo:block line-height="17pt" text-align="right">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:value-of select="concat(format-number(./requestedQuantity_l,'###,###,###'), ' SF')"/>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>                
                        </fo:table-row>
                    </xsl:for-each>
                    
                    <xsl:variable name="finalValueSideWall_2SF">
                        <xsl:value-of select="sum(/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='SideWall2']/requestedQuantity_l)"/>
                    </xsl:variable>
                    
                    <xsl:variable name="finalValueSideWall_2Roll">
                        <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='SideWall2']">
                            <q>
                                <xsl:value-of select="substring-before(./description2_l,' RL')"/>
                            </q>
                        </xsl:for-each>     
                    </xsl:variable>
                    
                    <!-- SIDE WALL 2 TOTAL-->
                    <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='SideWall2']">
                        <xsl:if test="position()=last()">
                            <fo:table-row>
                                <fo:table-cell padding-right="30px">
                                    <fo:block line-height="17pt" text-align="right" font-weight="bold">
                                        <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">
                                            <xsl:value-of select="concat('S/W 2 ', $buildingName, ' TOTAL')"/>
                                        </fo:inline>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell>
                                    <fo:block line-height="17pt" text-align="left" font-weight="bold">
                                        <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:value-of select="sum(exsl:node-set($finalValueSideWall_2Roll)/q)"/> Rolls</fo:inline>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell padding-right="15px">
                                    <fo:block line-height="17pt" text-align="right" font-weight="bold">
                                        <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:value-of select="format-number($finalValueSideWall_2SF,'###,###,###')"/> SF</fo:inline>
                                    </fo:block>
                                </fo:table-cell>                
                            </fo:table-row>
                        
                            <!-- BLANK ROW -->
                            <xsl:call-template name="blankTableRow"/>                   
                        </xsl:if>
                    </xsl:for-each>
                    <!-- SIDE WALL 2 - END-->
                    <!-- END WALL 1 - START -->
                    <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='EndWall1']">
                        <fo:table-row>
                            <fo:table-cell padding-right="30px" padding-left="10px">
                                <fo:block line-height="17pt" text-align="left">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:variable name="partDesc">
                                            <xsl:choose>
                                                <xsl:when test="./partDescForOutputDoc_l!=''">
                                                    <xsl:value-of select="./partDescForOutputDoc_l"/>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:value-of select="./item_l/_part_desc"/>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:variable>
                                        <xsl:choose>                                        
                                            <xsl:when test="./outputCutSheetTabDescription_l!=''">
                                                <!-- <xsl:value-of select="concat(./item_l/_part_desc, ' ', ./tab_l)"/> " TAB -->
                                                <xsl:value-of select="concat($partDesc, ' ', ./outputCutSheetTabDescription_l)"/>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <!-- <xsl:value-of select="./item_l/_part_desc"/> -->
                                                <xsl:value-of select="$partDesc"/>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell>
                                <fo:block line-height="17pt" text-align="left" font-weight="bold">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:value-of select="concat(./description2_l, ' ', $buildingName, ' E/W 1')"/>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding-right="15px">
                                <fo:block line-height="17pt" text-align="right">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:value-of select="concat(format-number(./requestedQuantity_l,'###,###,###'), ' SF')"/>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>                
                        </fo:table-row>
                    </xsl:for-each>
                    
                    <xsl:variable name="finalValueEndWall_1SF">
                        <xsl:value-of select="sum(/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='EndWall1']/requestedQuantity_l)"/>
                    </xsl:variable>
                    
                    <xsl:variable name="finalValueEndWall_1Roll">
                        <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='EndWall1']">
                            <q>
                                <xsl:value-of select="substring-before(./description2_l,' RL')"/>
                            </q>
                        </xsl:for-each>     
                    </xsl:variable>
                    
                    <!-- END WALL 1 TOTAL-->
                    <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='EndWall1']">
                        <xsl:if test="position()=last()">
                            <fo:table-row>
                                <fo:table-cell padding-right="30px">
                                    <fo:block line-height="17pt" text-align="right" font-weight="bold">
                                        <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">
                                            <xsl:value-of select="concat('E/W 1 ', $buildingName, ' TOTAL')"/>
                                        </fo:inline>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell>
                                    <fo:block line-height="17pt" text-align="left" font-weight="bold">
                                        <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:value-of select="sum(exsl:node-set($finalValueEndWall_1Roll)/q)"/> Rolls</fo:inline>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell padding-right="15px">
                                    <fo:block line-height="17pt" text-align="right" font-weight="bold">
                                        <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:value-of select="format-number($finalValueEndWall_1SF,'###,###,###')"/> SF</fo:inline>
                                    </fo:block>
                                </fo:table-cell>                
                            </fo:table-row>
                            <!-- BLANK ROW -->
                            <xsl:call-template name="blankTableRow"/>                   
                        </xsl:if>
                    </xsl:for-each>
                    <!-- END WALL 1 - END -->
                    <!-- END WALL 2 - START -->
                    <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='EndWall2']">
                        <fo:table-row>
                            <fo:table-cell padding-right="30px" padding-left="10px">
                                <fo:block line-height="17pt" text-align="left">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:variable name="partDesc">
                                            <xsl:choose>
                                                <xsl:when test="./partDescForOutputDoc_l!=''">
                                                    <xsl:value-of select="./partDescForOutputDoc_l"/>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:value-of select="./item_l/_part_desc"/>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:variable>
                                        <xsl:choose>                                        
                                            <xsl:when test="./outputCutSheetTabDescription_l!=''">
                                                <!-- <xsl:value-of select="concat(./item_l/_part_desc, ' ', ./tab_l)"/> " TAB -->
                                                <xsl:value-of select="concat($partDesc, ' ', ./outputCutSheetTabDescription_l)"/>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <!-- <xsl:value-of select="./item_l/_part_desc"/> -->
                                                <xsl:value-of select="$partDesc"/>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell>
                                <fo:block line-height="17pt" text-align="left" font-weight="bold">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:value-of select="concat(./description2_l, ' ', $buildingName, ' E/W 2')"/>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding-right="15px">
                                <fo:block line-height="17pt" text-align="right">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:value-of select="concat(format-number(./requestedQuantity_l,'###,###,###'), ' SF')"/>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>                
                        </fo:table-row>
                    </xsl:for-each>
                    
                    <xsl:variable name="finalValueEndWall_2SF">
                        <xsl:value-of select="sum(/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='EndWall2']/requestedQuantity_l)"/>
                    </xsl:variable>
                    
                    <xsl:variable name="finalValueEndWall_2Roll">
                        <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='EndWall2']">
                            <q>
                                <xsl:value-of select="substring-before(./description2_l,' RL')"/>
                            </q>
                        </xsl:for-each>     
                    </xsl:variable>
                    
                    <!-- END WALL 2 TOTAL-->
                    <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='EndWall2']">
                        <xsl:if test="position()=last()">
                            <fo:table-row>
                                <fo:table-cell padding-right="30px">
                                    <fo:block line-height="17pt" text-align="right" font-weight="bold">
                                        <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">
                                            <xsl:value-of select="concat('E/W 2 ', $buildingName, ' TOTAL')"/>
                                        </fo:inline>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell>
                                    <fo:block line-height="17pt" text-align="left" font-weight="bold">
                                        <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:value-of select="sum(exsl:node-set($finalValueEndWall_2Roll)/q)"/> Rolls</fo:inline>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell padding-right="15px">
                                    <fo:block line-height="17pt" text-align="right" font-weight="bold">
                                        <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:value-of select="format-number($finalValueEndWall_2SF,'###,###,###')"/> SF</fo:inline>
                                    </fo:block>
                                </fo:table-cell>                
                            </fo:table-row>
                            <!-- Empty row-->
                            <fo:table-row>
                                <fo:table-cell>
                                    <fo:block line-height="17pt">
                                        <fo:inline> </fo:inline>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell>
                                    <fo:block line-height="17pt">
                                        <fo:inline> </fo:inline>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell>
                                    <fo:block line-height="17pt">
                                        <fo:inline> </fo:inline>
                                    </fo:block>
                                </fo:table-cell>                
                            </fo:table-row>  
                        </xsl:if>
                    </xsl:for-each>
                    <!-- END WALL 2 - END -->

                    <!-- COMBINED WALLS LINER - START -->
                    <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and contains(./section_l,'CombinedWalls') and ./partType_l = 'Liner']">
                        <fo:table-row>
                            <fo:table-cell padding-right="30px" padding-left="10px">
                                <fo:block line-height="17pt" text-align="left">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWDem">                                                                             
                                        <xsl:value-of select="./linerDesc_l"/>                                      
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell>
                                <fo:block line-height="17pt" text-align="left" font-weight="bold">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWBoo">
                                        <xsl:value-of select="./sectionLabel_l"/>                                       
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding-right="15px">
                                <fo:block line-height="17pt" text-align="right">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:value-of select="concat(format-number(./requestedQuantity_l,'###,###,###'), ' SF')"/>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>             
                    </xsl:for-each>
                    <!-- COMBINED WALLS LINER - END -->

                    <!-- COMBINED WALLS - START -->
                    <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and contains(./section_l,'CombinedWalls') and ./partType_l != 'Liner']">
                        <fo:table-row>
                            <fo:table-cell padding-right="30px" padding-left="10px">
                                <fo:block line-height="17pt" text-align="left">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:variable name="partDesc">
                                            <xsl:choose>
                                                <xsl:when test="./partDescForOutputDoc_l!=''">
                                                    <xsl:value-of select="./partDescForOutputDoc_l"/>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:value-of select="./item_l/_part_desc"/>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:variable>
                                        <xsl:choose>                                        
                                            <xsl:when test="./outputCutSheetTabDescription_l!=''">
                                                <!-- <xsl:value-of select="concat(./item_l/_part_desc, ' ', ./tab_l)"/> " TAB -->
                                                <xsl:value-of select="concat($partDesc, ' ', ./outputCutSheetTabDescription_l)"/>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <!-- <xsl:value-of select="./item_l/_part_desc"/> -->
                                                <xsl:value-of select="$partDesc"/>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell>
                                <fo:block line-height="17pt" text-align="left" font-weight="bold">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:value-of select="concat(./description2_l, ' ', $buildingName, ' WALLS')"/>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding-right="15px">
                                <fo:block line-height="17pt" text-align="right">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:value-of select="concat(format-number(./requestedQuantity_l,'###,###,###'), ' SF')"/>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>                
                        </fo:table-row>
                    </xsl:for-each>
                    
                    <xsl:variable name="finalValueCombinedWallsSF">
                        <xsl:value-of select="sum(/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and contains(./section_l,'CombinedWalls') and ./partType_l != 'Liner']/requestedQuantity_l)"/>
                    </xsl:variable>
                    
                    <xsl:variable name="finalValueCombinedWallsRoll">
                        <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and contains(./section_l,'CombinedWalls') and ./partType_l != 'Liner']">
                            <q>
                                <xsl:value-of select="substring-before(./description2_l,' RL')"/>
                            </q>
                        </xsl:for-each>     
                    </xsl:variable>

                    <!-- COMBINED WALLS TOTAL-->
                    <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and contains(./section_l,'CombinedWalls')]">
                        <xsl:if test="position()=last()">
                            <fo:table-row>
                                <fo:table-cell padding-right="30px">
                                    <fo:block line-height="17pt" text-align="right" font-weight="bold">
                                        <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">
                                            <xsl:value-of select="concat('WALLS ', $buildingName, ' TOTAL')"/>
                                        </fo:inline>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell>
                                    <fo:block line-height="17pt" text-align="left" font-weight="bold">
                                        <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:value-of select="sum(exsl:node-set($finalValueCombinedWallsRoll)/q)"/> Rolls</fo:inline>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell padding-right="15px">
                                    <fo:block line-height="17pt" text-align="right" font-weight="bold">
                                        <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:value-of select="format-number($finalValueCombinedWallsSF,'###,###,###')"/> SF</fo:inline>
                                    </fo:block>
                                </fo:table-cell>                
                            </fo:table-row>
                            <!-- Empty row-->
                            <fo:table-row>
                                <fo:table-cell>
                                    <fo:block line-height="17pt">
                                        <fo:inline> </fo:inline>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell>
                                    <fo:block line-height="17pt">
                                        <fo:inline> </fo:inline>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell>
                                    <fo:block line-height="17pt">
                                        <fo:inline> </fo:inline>
                                    </fo:block>
                                </fo:table-cell>                
                            </fo:table-row>  
                        </xsl:if>
                    </xsl:for-each>
                    <!-- COMBINED WALLS - END -->

                    <!-- PARTITION WALLS - START -->    
                    <xsl:variable name="partitionWallCount" select="count(/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and ./model_l/_model_variable_name = 'partitionWall'])"/>                    
                    <xsl:call-template name="partitionWallGroup">                       
                            <xsl:with-param name="buildingDocNum" select="$buildingDocNum"/>
                            <xsl:with-param name="buildingName" select="$buildingName"/>
                            <xsl:with-param name="i" select="number(1)"/>
                            <xsl:with-param name="total" select="$partitionWallCount"/>
                    </xsl:call-template>                    
                    <!-- PARTITION WALLS TOTAL-->
                    <xsl:variable name="finalPartitionWallSF">
                        <xsl:value-of select="sum(/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='PartitionWall']/requestedQuantity_l)"/>
                    </xsl:variable>
                    
                    <xsl:variable name="finalPartitionWallRolls">
                        <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='PartitionWall']">
                            <q>
                                <xsl:value-of select="substring-before(./description2_l,' RL')"/>
                            </q>
                        </xsl:for-each>     
                    </xsl:variable>             
                    <!-- PARTITION WALLS - END -->

                    <!-- COMBINED PARTITION WALLS LINER - START -->
                    <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and contains(./section_l,'CombinedPartitionWalls') and ./partType_l = 'Liner']">
                        <fo:table-row>
                            <fo:table-cell padding-right="30px" padding-left="10px">
                                <fo:block line-height="17pt" text-align="left">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWDem">                                                                             
                                        <xsl:value-of select="./linerDesc_l"/>                                      
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell>
                                <fo:block line-height="17pt" text-align="left" font-weight="bold">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWBoo">
                                        <xsl:value-of select="./sectionLabel_l"/>                                       
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding-right="15px">
                                <fo:block line-height="17pt" text-align="right">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:value-of select="concat(format-number(./requestedQuantity_l,'###,###,###'), ' SF')"/>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>             
                    </xsl:for-each>
                    <!-- COMBINED PARTITION WALLS LINER - END -->

                    <!-- COMBINED PARTITION WALLS - START -->
                    <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='CombinedPartitionWalls']">
                        <fo:table-row>
                            <fo:table-cell padding-right="30px" padding-left="10px">
                                <fo:block line-height="17pt" text-align="left">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:variable name="partDesc">
                                            <xsl:choose>
                                                <xsl:when test="./partDescForOutputDoc_l!=''">
                                                    <xsl:value-of select="./partDescForOutputDoc_l"/>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:value-of select="./item_l/_part_desc"/>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:variable>
                                        <xsl:choose>                                        
                                            <xsl:when test="./outputCutSheetTabDescription_l!=''">
                                                <!-- <xsl:value-of select="concat(./item_l/_part_desc, ' ', ./tab_l)"/> " TAB -->
                                                <xsl:value-of select="concat($partDesc, ' ', ./outputCutSheetTabDescription_l)"/>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <!-- <xsl:value-of select="./item_l/_part_desc"/> -->
                                                <xsl:value-of select="$partDesc"/>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell>
                                <fo:block line-height="17pt" text-align="left" font-weight="bold">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:value-of select="concat(./description2_l, ' ', $buildingName, ' ADDITIONAL WALLS')"/>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding-right="15px">
                                <fo:block line-height="17pt" text-align="right">
                                    <fo:inline font-size="11pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:value-of select="concat(format-number(./requestedQuantity_l,'###,###,###'), ' SF')"/>
                                    </fo:inline>
                                </fo:block>
                            </fo:table-cell>                
                        </fo:table-row>
                    </xsl:for-each>
                    
                    <xsl:variable name="finalValueCombinedPartitionWallsSF">
                        <xsl:value-of select="sum(/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='CombinedPartitionWalls']/requestedQuantity_l)"/>
                    </xsl:variable>
                    
                    <xsl:variable name="finalValueCombinedPartitionWallsRoll">
                        <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='CombinedPartitionWalls']">
                            <q>
                                <xsl:value-of select="substring-before(./description2_l,' RL')"/>
                            </q>
                        </xsl:for-each>     
                    </xsl:variable>

                    <!-- COMBINED WALLS TOTAL-->
                    <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='CombinedPartitionWalls']">
                        <xsl:if test="position()=last()">
                            <fo:table-row>
                                <fo:table-cell padding-right="30px">
                                    <fo:block line-height="17pt" text-align="right" font-weight="bold">
                                        <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">
                                            <xsl:value-of select="concat('ADDITIONAL WALLS ', $buildingName, ' TOTAL')"/>
                                        </fo:inline>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell>
                                    <fo:block line-height="17pt" text-align="left" font-weight="bold">
                                        <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:value-of select="sum(exsl:node-set($finalValueCombinedPartitionWallsRoll)/q)"/> Rolls</fo:inline>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell padding-right="15px">
                                    <fo:block line-height="17pt" text-align="right" font-weight="bold">
                                        <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">
                                        <xsl:value-of select="format-number($finalValueCombinedPartitionWallsSF,'###,###,###')"/> SF</fo:inline>
                                    </fo:block>
                                </fo:table-cell>                
                            </fo:table-row>
                            <!-- Empty row-->
                            <fo:table-row>
                                <fo:table-cell>
                                    <fo:block line-height="17pt">
                                        <fo:inline> </fo:inline>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell>
                                    <fo:block line-height="17pt">
                                        <fo:inline> </fo:inline>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell>
                                    <fo:block line-height="17pt">
                                        <fo:inline> </fo:inline>
                                    </fo:block>
                                </fo:table-cell>                
                            </fo:table-row>  
                        </xsl:if>
                    </xsl:for-each>
                    <!-- COMBINED PARTITION WALLS - END -->

                    <!-- BUILDING GRAND TOTAL - START -->
                    <fo:table-row>
                        <fo:table-cell padding-right="30px">
                            <fo:block line-height="17pt" text-align="right" font-weight="bold" padding-right="14px" padding-left="8px">
                                <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">
                                    <xsl:value-of select="concat($buildingName, ' GRAND TOTAL')"/>
                                </fo:inline>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell>
                            <fo:block line-height="17pt" text-align="left" font-weight="bold">
                                <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">
                                    <xsl:value-of select="
                                        sum(exsl:node-set($finalValueRollsRoof)/q) +
                                        sum(exsl:node-set($finalValueRollsRoofTop)/q) +
                                        sum(exsl:node-set($finalValueRollsRoofBottom)/q) +
                                        sum(exsl:node-set($finalValueSideWall_1Roll)/q) +
                                        sum(exsl:node-set($finalValueSideWall_2Roll)/q) +
                                        sum(exsl:node-set($finalValueEndWall_1Roll)/q) +
                                        sum(exsl:node-set($finalValueEndWall_2Roll)/q) + 
                                        sum(exsl:node-set($finalValueCombinedWallsRoll)/q) + 
                                        sum(exsl:node-set($finalPartitionWallRolls)/q) +
                                        sum(exsl:node-set($finalValueCombinedPartitionWallsRoll)/q)
                                    "/>
                                Rolls</fo:inline>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding-right="15px">
                            <fo:block line-height="17pt" text-align="right" font-weight="bold">
                                <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">                         
                                    <xsl:value-of select="format-number($finalValueRoofSF + 
                                                                        $finalValueRoofTopSF + 
                                                                        $finalValueRoofBottomSF + 
                                                                        $finalValueSideWall_1SF + 
                                                                        $finalValueSideWall_2SF + 
                                                                        $finalValueEndWall_1SF + 
                                                                        $finalValueEndWall_2SF +
                                                                        $finalValueCombinedWallsSF +
                                                                        $finalValueCombinedPartitionWallsSF +
                                                                        $finalPartitionWallSF, '###,###,###')"/> SF</fo:inline>
                            </fo:block>
                        </fo:table-cell>                
                    </fo:table-row>


                    <!-- BLANK ROW -->
                    <xsl:call-template name="blankTableRow"/>                               
                </xsl:for-each>
                <!-- BUILDING GRAND TOTAL - END -->
                <!-- PROJECT GRAND TOTAL - START -->
                <xsl:for-each select="/transaction/data_xml/document[normalize-space(./@data_type)='2' and ./_parent_doc_number='']">
                    <xsl:variable name="buildingDocNum" select="./_document_number"/>       
                    <xsl:variable name="TotalValueRoofSF">
                        <!-- <xsl:value-of select="sum(/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and ./section_l='Roof']/requestedQuantity_l)"/> -->
                        <xsl:value-of select="sum(/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./section_l='Roof']/requestedQuantity_l)"/>
                    </xsl:variable>
                    
                    <xsl:variable name="TotalValueRoofTopSF">
                        <!-- <xsl:value-of select="sum(/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and ./section_l='Roof']/requestedQuantity_l)"/> -->
                        <xsl:value-of select="sum(/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and normalize-space(./section_l)='Roof-Top Layer']/requestedQuantity_l)"/>
                    </xsl:variable>
                    
                    <xsl:variable name="TotalValueRoofBottomSF">
                        <!-- <xsl:value-of select="sum(/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and ./section_l='Roof']/requestedQuantity_l)"/> -->
                        <xsl:value-of select="sum(/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and normalize-space(./section_l)='Roof-Bottom Layer']/requestedQuantity_l)"/>
                    </xsl:variable>
                
                    <xsl:variable name="TotalValueSide1SF">
                        <!-- <xsl:value-of select="sum(/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='SideWall1']/requestedQuantity_l)"/> -->
                        <xsl:value-of select="sum(/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and normalize-space(./section_l)='SideWall1']/requestedQuantity_l)"/>
                    </xsl:variable>
                
                    <xsl:variable name="TotalValueSide2SF">
                        <!-- <xsl:value-of select="sum(/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='SideWall2']/requestedQuantity_l)"/> -->
                        <xsl:value-of select="sum(/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and normalize-space(./section_l)='SideWall2']/requestedQuantity_l)"/>
                    </xsl:variable>
                
                    <xsl:variable name="TotalValueEndWall1SF">
                        <!-- <xsl:value-of select="sum(/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='Endwall1']/requestedQuantity_l)"/> -->
                        <xsl:value-of select="sum(/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and normalize-space(./section_l)='EndWall1']/requestedQuantity_l)"/>
                    </xsl:variable>
                
                    <xsl:variable name="TotalValueEndWall2SF">
                        <!-- <xsl:value-of select="sum(/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='Endwall2']/requestedQuantity_l)"/> -->
                        <xsl:value-of select="sum(/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and normalize-space(./section_l)='EndWall2']/requestedQuantity_l)"/>
                    </xsl:variable>

                    <xsl:variable name="TotalValueCombinedWallsSF">
                        <!-- <xsl:value-of select="sum(/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='Endwall2']/requestedQuantity_l)"/> -->
                        <xsl:value-of select="sum(/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and contains(./section_l,'CombinedWalls') and ./partType_l != 'Liner']/requestedQuantity_l)"/>
                    </xsl:variable>

                    <xsl:variable name="TotalValuePartitionWallSF">                     
                        <xsl:value-of select="sum(/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and normalize-space(./section_l)='PartitionWall']/requestedQuantity_l)"/>
                    </xsl:variable>

                    <xsl:variable name="TotalValueCombinedPartitionWallsSF">
                        <!-- <xsl:value-of select="sum(/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='Endwall2']/requestedQuantity_l)"/> -->
                        <xsl:value-of select="sum(/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and normalize-space(./section_l)='CombinedPartitionWalls']/requestedQuantity_l)"/>
                    </xsl:variable>
                
                    <xsl:variable name="totalSFValue">
                        <xsl:value-of select="format-number($TotalValueRoofSF,'#') + 
                            format-number($TotalValueRoofTopSF,'#') + 
                            format-number($TotalValueRoofBottomSF,'#') + 
                            format-number($TotalValueSide1SF,'#') + 
                            format-number($TotalValueSide2SF,'#') + 
                            format-number($TotalValueEndWall1SF,'#') + 
                            format-number($TotalValueEndWall2SF,'#') +
                            format-number($TotalValueCombinedWallsSF,'#') +
                            format-number($TotalValueCombinedPartitionWallsSF,'#') +
                            format-number($TotalValuePartitionWallSF, '#')"
                        />
                    </xsl:variable>
                
                    <xsl:variable name="TotalValueRollRoof">
                        <!-- <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and ./section_l='Roof']"> -->
                        <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./section_l='Roof']">
                            <q>
                                <xsl:value-of select="substring-before(./description2_l,' RL')"/>
                            </q>
                        </xsl:for-each>     
                    </xsl:variable>
                    
                    <xsl:variable name="TotalValueRollRoofTop">
                        <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and normalize-space(./section_l)='Roof-Top Layer']">
                            <q>
                                <xsl:value-of select="substring-before(./description2_l,' RL')"/>
                            </q>
                        </xsl:for-each>     
                    </xsl:variable>
                    
                    <xsl:variable name="TotalValueRollRoofBottom">
                        <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and normalize-space(./section_l)='Roof-Bottom Layer']">
                            <q>
                                <xsl:value-of select="substring-before(./description2_l,' RL')"/>
                            </q>
                        </xsl:for-each>     
                    </xsl:variable>
                
                    <xsl:variable name="TotalValueRollSideWall1">
                        <!-- <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='SideWall1']"> -->
                        <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and normalize-space(./section_l)='SideWall1']">
                            <q>
                                <xsl:value-of select="substring-before(./description2_l,' RL')"/>
                            </q>
                        </xsl:for-each>     
                    </xsl:variable>
                
                    <xsl:variable name="TotalValueRollSideWall2">
                        <!-- <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='SideWall2']"> -->
                        <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and normalize-space(./section_l)='SideWall2']">
                            <q>
                                <xsl:value-of select="substring-before(./description2_l,' RL')"/>
                            </q>
                        </xsl:for-each>     
                    </xsl:variable>
                
                    <xsl:variable name="TotalValueRollEndWall1">
                        <!-- <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='Endwall1']"> -->
                        <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and normalize-space(./section_l)='EndWall1']">
                            <q>
                                <xsl:value-of select="substring-before(./description2_l,' RL')"/>
                            </q>
                        </xsl:for-each>     
                    </xsl:variable>
                
                    <xsl:variable name="TotalValueRollEndWall2">
                        <!-- <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='Endwall2']"> -->
                        <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and normalize-space(./section_l)='EndWall2']">
                            <q>
                                <xsl:value-of select="substring-before(./description2_l,' RL')"/>
                            </q>
                        </xsl:for-each>     
                    </xsl:variable>

                    <xsl:variable name="TotalValueRollCombinedWalls">
                        <!-- <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='Endwall2']"> -->
                        <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and contains(./section_l,'CombinedWalls') and ./partType_l != 'Liner']">
                            <q>
                                <xsl:value-of select="substring-before(./description2_l,' RL')"/>
                            </q>
                        </xsl:for-each>     
                    </xsl:variable>

                    <xsl:variable name="TotalValueRollPartitionWall">
                        <!-- <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='Endwall2']"> -->
                        <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and normalize-space(./section_l)='PartitionWall']">
                            <q>
                                <xsl:value-of select="substring-before(./description2_l,' RL')"/>
                            </q>
                        </xsl:for-each>     
                    </xsl:variable>

                    <xsl:variable name="TotalValueRollCombinedPartitionWalls">
                        <!-- <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./_parent_doc_number=$buildingDocNum and normalize-space(./section_l)='Endwall2']"> -->
                        <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and normalize-space(./section_l)='CombinedPartitionWalls']">
                            <q>
                                <xsl:value-of select="substring-before(./description2_l,' RL')"/>
                            </q>
                        </xsl:for-each>     
                    </xsl:variable>
                
                    <xsl:variable name="TotalValueRoll">
                        <xsl:value-of select="
                        sum(exsl:node-set($TotalValueRollRoof)/q) +
                        sum(exsl:node-set($TotalValueRollRoofTop)/q) +
                        sum(exsl:node-set($TotalValueRollRoofBottom)/q) +
                        sum(exsl:node-set($TotalValueRollSideWall1)/q) +
                        sum(exsl:node-set($TotalValueRollSideWall2)/q) +
                        sum(exsl:node-set($TotalValueRollEndWall1)/q) +
                        sum(exsl:node-set($TotalValueRollEndWall2)/q) +
                        sum(exsl:node-set($TotalValueRollCombinedWalls)/q) +
                        sum(exsl:node-set($TotalValueRollCombinedPartitionWalls)/q) +
                        sum(exsl:node-set($TotalValueRollPartitionWall)/q)"/>
                    </xsl:variable>
                    
                    <xsl:if test="position()=last()">
                        <fo:table-row>
                            <fo:table-cell>
                                <fo:block line-height="17pt">
                                    <fo:inline> </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell>
                                <fo:block line-height="17pt">
                                    <fo:inline> </fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell>
                                <fo:block line-height="17pt">
                                    <fo:inline> </fo:inline>
                                </fo:block>
                            </fo:table-cell>                
                        </fo:table-row>
                        
                        <fo:table-row>
                            <fo:table-cell padding-right="30px">
                                <fo:block line-height="17pt" text-align="right" font-weight="bold">
                                    <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">PROJECT TOTAL</fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell>
                                <fo:block line-height="17pt" text-align="left" font-weight="bold">
                                    <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">
                                    <xsl:value-of select="$TotalValueRoll"/> Rolls</fo:inline>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell padding-right="15px">
                                <fo:block line-height="17pt" text-align="right" font-weight="bold">
                                    <fo:inline font-size="12pt" color="#000000" font-family="FranklinGothicURWDem">
                                    <xsl:value-of select="format-number($totalSFValue,'###,###,###')"/> SF</fo:inline>
                                </fo:block>
                            </fo:table-cell>                
                        </fo:table-row>
                    </xsl:if>
                </xsl:for-each>
                <!-- PROJECT GRAND TOTAL - END --> 
                <!-- BLANK ROW -->
                <xsl:call-template name="blankTableRow"/>                            
            </fo:table-body>
        </fo:table>

        <xsl:if test="$mainDoc/includeDoorPricing_t='true'">
            <fo:table border-collapse="collapse" table-layout="fixed" float="center">
                <fo:table-column column-width="35%"/>
                <fo:table-column column-width="10%"/>
                <fo:table-column column-width="55%"/>
                <fo:table-body>
                    <!-- PROJECT SUMMARY OF DOORS - START -->
                    <xsl:for-each select="/transaction/data_xml/document[(normalize-space(./@data_type)='2' or normalize-space(./@data_type)='3') and ./door_l='true']">
                        <xsl:sort select="number(substring-after(./lineCustomSequenceNumber_l,'.'))" order="ascending" data-type="number" />
                        <xsl:if test="position()=1">
                            <fo:table-row>
                                <fo:table-cell number-columns-spanned="3" padding-top="1px" padding-right="30px" padding-left="10px">
                                    <fo:block text-align="left">
                                        <fo:inline color="#4e4e4f" font-weight="bold" font-size="12pt" font-family="Helvetica">
                                            PROJECT SUMMARY OF DOORS
                                        </fo:inline>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                        </xsl:if>
                        <fo:table-row>
                            <xsl:choose>
                                <xsl:when test="./model_l/_model_name='Door'">
                                    <fo:table-cell padding-top="5px" padding-left="10px" number-columns-spanned="3">
                                        <fo:block text-align="left" font-weight="bold">
                                            <fo:inline color="#4e4e4f" font-size="11pt" font-family="Helvetica">
                                                <xsl:value-of select="./item_l/_part_desc"/>
                                            </fo:inline>
                                        </fo:block>
                                    </fo:table-cell>   
                                </xsl:when>  
                                <xsl:otherwise>
                                    <fo:table-cell padding-top="1px" padding-right="0px" padding-left="10px">
                                        <fo:block text-align="left">
                                            <fo:inline color="#4e4e4f" font-size="11pt" font-family="Helvetica">
                                                <xsl:value-of select="translate(./item_l/_part_desc, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
                                            </fo:inline>
                                        </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell padding-top="1px" padding-right="5px" padding-left="10px">
                                        <fo:block text-align="right">
                                            <fo:inline color="#4e4e4f" font-size="11pt" font-family="Helvetica">
                                                <xsl:value-of select="format-number(./requestedQuantity_l,'###,###,###')"/>
                                            </fo:inline>
                                        </fo:block>
                                    </fo:table-cell>  
                                    <fo:table-cell padding-top="1px">
                                        <fo:block text-align="left">
                                            <fo:inline color="#4e4e4f" font-size="11pt" font-family="Helvetica">
                                                <xsl:value-of select="' EA'"/>
                                            </fo:inline>
                                        </fo:block>
                                    </fo:table-cell>   
                                </xsl:otherwise>
                            </xsl:choose>         
                        </fo:table-row>
                    </xsl:for-each>
                    <!-- BLANK ROW -->
                    <xsl:call-template name="blankTableRow"/>   
                </fo:table-body>
            </fo:table>
        </xsl:if>

        <fo:table border-collapse="collapse" table-layout="fixed" float="center">
            <fo:table-column column-width="35%"/>
            <fo:table-column column-width="10%"/>
            <fo:table-column column-width="55%"/>
            <fo:table-body>
                <!-- PROJECT SUMMARY OF ACCESSORIES - START -->
                <xsl:variable name="dataStr" select="$mainDoc/accessoryCutSheetItemBreakdown_t"/>
                <fo:table-row>
                    <fo:table-cell number-columns-spanned="3" padding-top="1px" padding-right="30px" padding-left="10px">
                        <fo:block text-align="left">
                            <fo:inline color="#4e4e4f" font-weight="bold" font-size="12pt" font-family="Helvetica">
                                PROJECT SUMMARY OF ACCESSORIES
                            </fo:inline>
                        </fo:block>
                    </fo:table-cell>
                </fo:table-row>
                <xsl:for-each xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exslt="http://exslt.org/strings" select="exslt:split($dataStr, '#@#')">           
                    <xsl:variable name="eachRow" select="."/>                    
                    <fo:table-row>
                        <xsl:for-each xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exslt="http://exslt.org/strings" select="exslt:split($eachRow, '#$#')">
                        <xsl:variable name="eachCell" select="."/>
                            <xsl:choose>
                                <xsl:when test="position()=1">
                                    <fo:table-cell padding-top="1px" padding-right="30px" padding-left="10px">
                                        <fo:block text-align="left">
                                            <fo:inline color="#4e4e4f" font-size="11pt" font-family="Helvetica">
                                            <xsl:value-of select="$eachCell"/>
                                            </fo:inline>
                                        </fo:block>
                                    </fo:table-cell>
                                </xsl:when>
                                <xsl:when test="position()=2">
                                    <fo:table-cell padding-top="1px" padding-right="5px" padding-left="10px">
                                        <fo:block text-align="right">
                                            <fo:inline color="#4e4e4f" font-size="11pt" font-family="Helvetica">
                                            <xsl:value-of select="$eachCell"/>
                                            </fo:inline>
                                        </fo:block>
                                    </fo:table-cell>
                                </xsl:when>
                                <xsl:otherwise>
                                    <fo:table-cell padding-top="1px">
                                        <fo:block text-align="left">
                                            <fo:inline color="#4e4e4f" font-size="11pt" font-family="Helvetica">
                                                <xsl:value-of select="$eachCell"/>
                                            </fo:inline>
                                        </fo:block>
                                    </fo:table-cell>
                                </xsl:otherwise>                               
                            </xsl:choose>
                        </xsl:for-each>             
                    </fo:table-row>
                </xsl:for-each>
                <!-- BLANK ROW -->
                <xsl:call-template name="blankTableRow"/>   
            </fo:table-body>
        </fo:table>
        <!-- PROJECT SUMMARY OF MISCELLANEOUS - START -->    
        <xsl:variable name="dataStr" select="$mainDoc/buildingItemBreakdown_t"/>
        <xsl:if xmlns:exslt="http://exslt.org/strings" test="contains($dataStr, 'MISCELLANEOUS')">    
            <fo:table border-collapse="collapse" table-layout="fixed" float="center">
                <fo:table-column column-width="30%"/>
                <fo:table-column column-width="20%"/>
                <fo:table-column column-width="50%"/>
                <fo:table-body>
                    <fo:table-row>
                        <fo:table-cell number-columns-spanned="3" padding-top="1px" padding-right="30px" padding-left="10px">
                            <fo:block text-align="left">
                                <fo:inline color="#4e4e4f" font-weight="bold" font-size="12pt" font-family="Helvetica">
                                    PROJECT SUMMARY OF MISCELLANEOUS
                                </fo:inline>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <xsl:for-each xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exslt="http://exslt.org/strings" select="exslt:split($dataStr, '@!@')">
                        <xsl:variable name="eachBuilding" select="."/>                    
                        <xsl:for-each xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exslt="http://exslt.org/strings" select="exslt:split($eachBuilding, '#@#')">
                            <xsl:variable name="eachRow" select="."/>
                            <fo:table-row>
                                <xsl:choose>
                                    <xsl:when test="starts-with($eachRow, 'MISC_')">
                                        <!-- Miscellaneous Items -->
                                        <xsl:for-each xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exslt="http://exslt.org/strings" select="exslt:split($eachRow, '#$#')">
                                            <xsl:variable name="eachCell" select="."/>
                                            <xsl:choose>
                                                <xsl:when test="position()=1">
                                                    <fo:table-cell padding-top="4px" padding-left="10px" number-columns-spanned="2">
                                                        <fo:block text-align="left">
                                                            <fo:inline color="#4e4e4f" font-size="11pt" font-family="Helvetica">
                                                                <xsl:value-of select="substring-after($eachCell, ':')"/>
                                                            </fo:inline>
                                                        </fo:block>
                                                    </fo:table-cell>
                                                </xsl:when>
                                                <xsl:when test="position()=2">
                                                    <fo:table-cell padding-top="4px" number-columns-spanned="1">
                                                        <fo:block text-align="left">
                                                            <fo:inline color="#4e4e4f" font-size="11pt" font-family="Helvetica">
                                                                <xsl:value-of select="$eachCell"/>
                                                            </fo:inline>
                                                        </fo:block>
                                                    </fo:table-cell>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:for-each>
                                    </xsl:when>
                                </xsl:choose>
                            </fo:table-row>
                        </xsl:for-each>
                    </xsl:for-each>
                    <!-- BLANK ROW -->
                    <xsl:call-template name="blankTableRow"/>                   
                </fo:table-body>
            </fo:table>
        </xsl:if>
        <!-- PROJECT SUMMARY OF MISCELLANEOUS - END -->     