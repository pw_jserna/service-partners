<xsl:variable name="mainDoc" select="/transaction/data_xml/document[@document_var_name='transaction']"/>
<xsl:variable name="subDoc" select="/transaction/data_xml/document"/>
<xsl:variable name="pricingDetail" select="$mainDoc/outputPricingDetail_t"/>
	
<fo:table table-layout="fixed" float="center" border="1px solid #EE2E24">
	<fo:table-column column-width="100%"/>
		<fo:table-body>
			<fo:table-row>
				<fo:table-cell padding-top="10px"> 
					<fo:block text-align="center">
						<fo:inline color="#EE2E24" font-weight="bold" font-size="12pt" 
						font-family="Helvetica">
						QUOTE SUMMARY						
						</fo:inline>
					</fo:block>
				</fo:table-cell>
			</fo:table-row>
			<fo:table-row>
				<fo:table-cell padding="8px">
					<fo:table table-layout="fixed" float="center">
					<fo:table-column column-width="15.12%"/>
					<fo:table-column column-width="27.10%"/>
					<fo:table-column column-width="27.19%"/>
					<fo:table-column column-width="8.5%"/>
					<fo:table-column column-width="9.58%"/>
					<fo:table-column column-width="12.51%"/>
					<!-- HEADER ROW -->
					<fo:table-header>
						<fo:table-row keep-together.within-page="1">
							<fo:table-cell>
								<fo:block text-align="right">
									<fo:inline font-weight="bold" color="#4e4e4f" font-size="8pt" font-family="Helvetica"> </fo:inline>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell padding-left="10px">
								<fo:block text-align="left">
									<fo:inline font-weight="bold" color="#4e4e4f" font-size="8pt" font-family="Helvetica">
									INSULATION &amp; SYSTEMS
									</fo:inline>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block text-align="center">
									<fo:inline font-weight="bold" color="#4e4e4f" font-size="8pt" font-family="Helvetica">
									FACING
									</fo:inline>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block text-align="right">
									<fo:inline font-weight="bold" color="#4e4e4f" font-size="8pt" font-family="Helvetica">
									QUANTITY
									</fo:inline>
								</fo:block>
							</fo:table-cell>
							<xsl:if test="$pricingDetail = 'detailed' or not($pricingDetail) or $pricingDetail = ''">
								<fo:table-cell>
									<fo:block text-align="right">
										<fo:inline font-weight="bold" color="#4e4e4f" font-size="8pt" font-family="Helvetica">
										UNIT PRICE
										</fo:inline>
									</fo:block>
								</fo:table-cell>
							</xsl:if>
							<fo:table-cell padding-right="7px">
								<fo:block text-align="right">
									<fo:inline font-weight="bold" color="#4e4e4f" font-size="8pt" font-family="Helvetica">
									TOTAL
									</fo:inline>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-header>
					<fo:table-body>
						<xsl:variable name="dataStr" select="$mainDoc/buildingItemBreakdown_t"/>
						<xsl:for-each xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exslt="http://exslt.org/strings" select="exslt:split($dataStr, '@!@')">
							<xsl:variable name="eachBuilding" select="."/>
							<xsl:for-each xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exslt="http://exslt.org/strings" select="exslt:split($eachBuilding, '#@#')">
								<xsl:variable name="eachRow" select="."/>
								<fo:table-row keep-together.within-page="1">
									<xsl:choose>
										<xsl:when test="position() = 1">
                                            <!-- Building Label -->
											<fo:table-cell background-color="#4e4e4f" padding-top="5px" number-columns-spanned="6"> 
												<fo:block text-align="left">
													<fo:inline color="#FFFFFF" font-weight="bold" font-size="8pt" 
													font-family="Helvetica">
													<xsl:value-of select="$eachRow"/>
													</fo:inline>
												</fo:block>
											</fo:table-cell>
										</xsl:when>
                                        <xsl:when test="starts-with($eachRow, 'LINER_ROOF') or starts-with($eachRow, 'LINER_WALLS') or starts-with($eachRow, 'LINER_ADDITIONAL') or starts-with($eachRow, 'LINER-MAND-ACC_') or starts-with($eachRow, 'LINER-OPT-ACC_')">
                                            <!-- Liner Items -->
                                            <xsl:choose>
                                                <xsl:when test="starts-with($eachRow, 'LINER_ROOF') or starts-with($eachRow, 'LINER_WALLS') or starts-with($eachRow, 'LINER_ADDITIONAL')">
                                                    <xsl:for-each xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exslt="http://exslt.org/strings" select="exslt:split($eachRow, '#$#')">
                                                        <xsl:variable name="eachCell" select="."/>
                                                        <xsl:choose>
                                                            <xsl:when test="position()=1">
                                                                <fo:table-cell padding-top="5px"> 
                                                                    <fo:block text-align="left">
                                                                        <fo:inline color="#4e4e4f" font-size="8pt" 
                                                                        font-family="Helvetica" font-weight="bold">
                                                                        <xsl:value-of select="substring-after($eachCell, 'LINER_')"/>
                                                                        </fo:inline>
                                                                    </fo:block>
                                                                </fo:table-cell>
                                                            </xsl:when>
                                                            <xsl:when test="position()=2">
                                                                <fo:table-cell padding-top="5px" number-columns-spanned="2"> 
                                                                    <fo:block text-align="left">
                                                                        <fo:inline color="#4e4e4f" font-size="8pt" 
                                                                        font-family="Helvetica">
                                                                        <xsl:value-of select="$eachCell"/>
                                                                        </fo:inline>
                                                                    </fo:block>
                                                                </fo:table-cell>
                                                            </xsl:when>
                                                            <xsl:when test="position()=3">
                                                                <fo:table-cell padding-top="4px">
                                                                    <fo:block text-align="right">
                                                                        <fo:inline color="#4e4e4f" font-size="8pt" font-family="Helvetica">
                                                                        <xsl:value-of select="$eachCell"/>
                                                                        </fo:inline>
                                                                    </fo:block>
                                                                </fo:table-cell>
                                                            </xsl:when>
															<xsl:when test="position()=4 and $pricingDetail = 'detailed' or not($pricingDetail) or $pricingDetail = ''">
                                                                <fo:table-cell padding-top="4px">
                                                                    <fo:block text-align="right">
                                                                        <fo:inline color="#4e4e4f" font-size="8pt" font-family="Helvetica">
                                                                        <xsl:value-of select="$eachCell"/>
                                                                        </fo:inline>
                                                                    </fo:block>
                                                                </fo:table-cell>
                                                            </xsl:when>
                                                            <xsl:when test="position()=last()">
                                                                <fo:table-cell padding-top="4px" padding-right="7px">
                                                                    <fo:block text-align="right">
                                                                        <fo:inline color="#4e4e4f" font-size="8pt" font-family="Helvetica">                                                                        
                                                                            <xsl:value-of select="$eachCell"/>                                                                      
                                                                        </fo:inline>
                                                                    </fo:block>
                                                                </fo:table-cell>
                                                            </xsl:when>
                                                            <xsl:otherwise>
                                                                <fo:table-cell padding-top="4px" padding-right="7px">
                                                                    <fo:block text-align="right">
                                                                        <fo:inline color="#4e4e4f" font-size="8pt" font-family="Helvetica">
                                                                        <xsl:value-of select="$eachCell"/>
                                                                        </fo:inline>
                                                                    </fo:block>
                                                                </fo:table-cell>
                                                            </xsl:otherwise>
                                                        </xsl:choose>
                                                    </xsl:for-each>
                                                </xsl:when>
                                                <xsl:when test="starts-with($eachRow, 'LINER-MAND-ACC_')">
                                                    <fo:table-cell padding-top="5px" number-columns-spanned="5"> 
                                                        <fo:block text-align="left">
                                                            <fo:inline color="#4e4e4f" font-size="8pt" 
                                                            font-family="Helvetica">
                                                            <xsl:value-of select="substring-after($eachRow, 'LINER-MAND-ACC_')"/>
                                                            </fo:inline>
                                                        </fo:block>
                                                    </fo:table-cell>
                                                </xsl:when>
                                                <xsl:when test="starts-with($eachRow, 'LINER-OPT-ACC_')">
                                                    <xsl:for-each xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exslt="http://exslt.org/strings" select="exslt:split($eachRow, '#$#')">
                                                        <xsl:variable name="eachCell" select="."/>
                                                        <xsl:choose>
                                                            <xsl:when test="position()=1">
                                                                <fo:table-cell padding-top="5px" number-columns-spanned="4"> 
                                                                    <fo:block text-align="left">
                                                                        <fo:inline color="#4e4e4f" font-size="8pt" 
                                                                        font-family="Helvetica">
                                                                        <xsl:value-of select="substring-after($eachCell, 'LINER-OPT-ACC_')"/>
                                                                        </fo:inline>
                                                                    </fo:block>
                                                                </fo:table-cell>
                                                            </xsl:when>
                                                            <xsl:when test="position()=last()">
                                                                <fo:table-cell padding-top="4px" padding-right="7px">
                                                                    <fo:block text-align="right">
                                                                        <fo:inline color="#4e4e4f" font-size="8pt" font-family="Helvetica">
                                                                            <xsl:if test="$pricingDetail = 'detailed' or not($pricingDetail) or $pricingDetail = ''">
                                                                                <xsl:value-of select="$eachCell"/>
                                                                            </xsl:if>
                                                                        </fo:inline>
                                                                    </fo:block>
                                                                </fo:table-cell>
                                                            </xsl:when>
                                                            <xsl:otherwise>
                                                                <fo:table-cell padding-top="4px" padding-right="7px">
                                                                    <fo:block text-align="right">
                                                                        <fo:inline color="#4e4e4f" font-size="8pt" font-family="Helvetica">
                                                                        <xsl:value-of select="$eachCell"/>
                                                                        </fo:inline>
                                                                    </fo:block>
                                                                </fo:table-cell>
                                                            </xsl:otherwise>
                                                        </xsl:choose>
                                                    </xsl:for-each>
                                                </xsl:when>
                                            </xsl:choose>                                        
                                        </xsl:when>
                                        <xsl:when test="starts-with($eachRow, 'MISC_')">
                                            <!-- Miscellaneous Items -->
                                            <xsl:for-each xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exslt="http://exslt.org/strings" select="exslt:split($eachRow, '#$#')">
                                                <xsl:variable name="eachCell" select="."/>
                                                <xsl:choose>
                                                    <xsl:when test="position()=1">
                                                        <fo:table-cell padding-top="4px" number-columns-spanned="3">
															<fo:block text-align="left">
																<fo:inline color="#4e4e4f" font-size="8pt" font-family="Helvetica">
                                                                    <xsl:value-of select="substring-after($eachCell, 'MISC_')"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
                                                    </xsl:when>
                                                    <xsl:when test="position()=last()">
														<fo:table-cell padding-top="4px" padding-right="7px">
															<fo:block text-align="right">
																<fo:inline color="#4e4e4f" font-size="8pt" font-family="Helvetica">
																<xsl:value-of select="$eachCell"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</xsl:when>
													<xsl:otherwise>
														<fo:table-cell padding-top="4px">
															<fo:block text-align="right">
																<fo:inline color="#4e4e4f" font-size="8pt" font-family="Helvetica">
																<xsl:value-of select="$eachCell"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:for-each>
                                        </xsl:when>
										<!-- <xsl:when test="position()=last()-1"> -->
                                        <xsl:when test="position()=last()-1 and starts-with($eachRow, 'Accessories:')">
                                            <!-- Accessories Included for General Roof and Walls -->
											<xsl:for-each xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exslt="http://exslt.org/strings" select="exslt:split($eachRow, '#$#')">
												<xsl:variable name="eachCell" select="."/>
												<xsl:choose>
													<xsl:when test="position()=1">
														<!-- <fo:table-cell padding-top="4px" number-columns-spanned="4" border-bottom="1px solid #4e4e4f"> -->
                                                        <fo:table-cell padding-top="4px" number-columns-spanned="4">
															<fo:block text-align="left">
																<fo:inline color="#4e4e4f" font-style="italic" font-size="8pt" font-family="Helvetica">
																<xsl:value-of select="$eachCell"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell> 
													</xsl:when>
                                                    <xsl:when test="position()=last()">
                                                        <fo:table-cell padding-top="4px" padding-right="7px">
                                                            <fo:block text-align="right">
                                                                <fo:inline color="#4e4e4f" font-size="8pt" font-family="Helvetica">
                                                                    <xsl:if test="$pricingDetail = 'detailed' or not($pricingDetail) or $pricingDetail = ''">
                                                                        <xsl:value-of select="$eachCell"/>
                                                                    </xsl:if>
                                                                </fo:inline>
                                                            </fo:block>
                                                        </fo:table-cell>
                                                    </xsl:when>
													<xsl:otherwise>
														<!-- <fo:table-cell padding-top="4px" padding-right="7px" border-bottom="1px solid #4e4e4f"> -->
                                                        <fo:table-cell padding-top="4px" padding-right="7px">
															<fo:block text-align="right">
																<fo:inline color="#4e4e4f" font-size="8pt" font-family="Helvetica">
																<xsl:value-of select="$eachCell"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell> 
													</xsl:otherwise>
												</xsl:choose>
											</xsl:for-each>							
										</xsl:when>
										<xsl:when test="position()=last()">                                        
                                            <!-- Building Price -->
											<!-- <fo:table-cell padding-top="4px" padding-bottom="6px" padding-right="7px" number-columns-spanned="5">  -->
                                            <xsl:for-each xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exslt="http://exslt.org/strings" select="exslt:split($eachRow, '#$#')">
                                                <xsl:variable name="eachCell" select="."/>
                                                <xsl:choose>
                                                    <xsl:when test="position()=1">
                                                        <fo:table-cell padding-top="4px" padding-bottom="6px" padding-right="7px" number-columns-spanned="3" border-top="1px solid #4e4e4f"> 
                                                            <fo:block text-align="left">
                                                                <fo:inline color="#4e4e4f" font-weight="bold" font-size="8pt" font-family="Helvetica" >
                                                                    <xsl:value-of select="$eachCell"/>
                                                                </fo:inline>
                                                            </fo:block>
                                                        </fo:table-cell>
                                                    </xsl:when>
                                                    <xsl:when test="position()=2">
                                                        <fo:table-cell padding-top="4px" padding-bottom="6px" padding-right="0px" number-columns-spanned="1" border-top="1px solid #4e4e4f"> 
                                                            <fo:block text-align="right">
                                                                <fo:inline color="#4e4e4f" font-weight="bold" font-size="8pt" font-family="Helvetica" >
                                                                    <xsl:value-of select="$eachCell"/>
                                                                </fo:inline>
                                                            </fo:block>
                                                        </fo:table-cell>
                                                    </xsl:when>													
                                                    <xsl:otherwise>
                                                        <fo:table-cell padding-top="4px" padding-bottom="6px" padding-right="7px" number-columns-spanned="1" border-top="1px solid #4e4e4f"> 
                                                            <fo:block text-align="right">
                                                                <fo:inline color="#4e4e4f" font-weight="bold" font-size="8pt" font-family="Helvetica" >
                                                                    <xsl:value-of select="$eachCell"/>
                                                                </fo:inline>
                                                            </fo:block>
                                                        </fo:table-cell>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:for-each>
										</xsl:when>
										<xsl:otherwise>
                                            <!-- General Roofs and Walls (Not Liner, Not Misc) -->
											<xsl:for-each xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exslt="http://exslt.org/strings" select="exslt:split($eachRow, '#$#')">
												<xsl:variable name="eachCell" select="."/>
												<xsl:choose>
													<xsl:when test="position()=1">
													    <fo:table-cell padding-top="4px">
															<fo:block text-align="left">
																<fo:inline color="#4e4e4f" font-size="8pt" font-family="Helvetica" font-weight="bold">
																<xsl:value-of select="$eachCell"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</xsl:when>
                                                    <xsl:when test="position()=2">
													    <fo:table-cell padding-top="4px">
															<fo:block text-align="left">
																<fo:inline color="#4e4e4f" font-size="8pt" font-family="Helvetica">
																<xsl:value-of select="$eachCell"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</xsl:when>
													<xsl:when test="position()=3">
													    <fo:table-cell padding-top="4px">
															<fo:block text-align="center">
																<fo:inline color="#4e4e4f" font-size="8pt" font-family="Helvetica">
																<xsl:value-of select="$eachCell"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</xsl:when>
													<xsl:when test="position()=last()">
														<fo:table-cell padding-top="4px" padding-right="7px">
															<fo:block text-align="right">
																<fo:inline color="#4e4e4f" font-size="8pt" font-family="Helvetica">
                                                                    <xsl:if test="$pricingDetail = 'detailed' or not($pricingDetail) or $pricingDetail = ''">
                                                                        <xsl:value-of select="$eachCell"/>
                                                                    </xsl:if>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</xsl:when>
													<xsl:otherwise>
														<fo:table-cell padding-top="4px">
															<fo:block text-align="right">
																<fo:inline color="#4e4e4f" font-size="8pt" font-family="Helvetica">
																<xsl:value-of select="$eachCell"/>
																</fo:inline>
															</fo:block>
														</fo:table-cell>
													</xsl:otherwise>
												</xsl:choose>                                    
											</xsl:for-each>
										</xsl:otherwise>
									</xsl:choose>                    
								</fo:table-row>
							</xsl:for-each>
						</xsl:for-each>
					</fo:table-body>
				</fo:table>
				</fo:table-cell>
			</fo:table-row>
		</fo:table-body>
</fo:table>