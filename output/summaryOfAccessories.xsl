<xsl:variable name="mainDoc" select="/transaction/data_xml/document[@document_var_name='transaction']"/>
<xsl:variable name="subDoc" select="/transaction/data_xml/document"/>
<fo:table table-layout="fixed" float="center">
    <fo:table-column column-width="58%"/>
    <fo:table-column column-width="8%"/>
    <fo:table-column column-width="34%"/>
    <fo:table-body>
        <xsl:variable name="dataStr" select="$mainDoc/accessoryItemBreakdown_t"/>
        <xsl:for-each xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exslt="http://exslt.org/strings" select="exslt:split($dataStr, '#@#')">           
			<xsl:variable name="eachRow" select="."/>
			<fo:table-row>
				<xsl:for-each xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exslt="http://exslt.org/strings" select="exslt:split($eachRow, '#$#')">
				<xsl:variable name="eachCell" select="."/>
					<xsl:choose>
						<xsl:when test="position()=1">
							<fo:table-cell padding-top="1px">
								<fo:block text-align="left">
									<fo:inline color="#4e4e4f" font-size="12pt" font-family="Helvetica">
									<xsl:value-of select="$eachCell"/>
									</fo:inline>
								</fo:block>
							</fo:table-cell>
						</xsl:when>
						<xsl:when test="position()=2">
							<fo:table-cell padding-top="1px">
								<fo:block text-align="right">
									<fo:inline color="#4e4e4f" font-size="12pt" font-family="Helvetica">
									<xsl:value-of select="$eachCell"/>
									</fo:inline>
								</fo:block>
							</fo:table-cell>
						</xsl:when>
						<xsl:otherwise>
							<fo:table-cell padding-top="1px" padding-left="4px">
								<fo:block text-align="left">
									<fo:inline color="#4e4e4f" font-size="12pt" font-family="Helvetica">
									<xsl:value-of select="$eachCell"/>
									</fo:inline>
								</fo:block>
							</fo:table-cell>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>				
			</fo:table-row>
		</xsl:for-each>				
    </fo:table-body>
</fo:table>