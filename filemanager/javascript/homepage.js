/**
 * @param dependencies {Array} name of modules this code depends on. Can exclude ".js"
 * @param callback {Function} function containing this module's functionality.
 * @version Fri Feb 25 18:44:56 2011
 */
 
(function(require, requirejs, define) {
require(["return_to_quote_button", "jquery_cookie"],function(rtq) { 
    //Put all functions for homepage here
    //this function runs when the page loads
    require.ready(function() {
        rtq.add_button_to_homepage(); 
    });

    //Get cpq site name
    var siteHost = window.location.host;
    var site = siteHost.substring(0, siteHost.indexOf("."));
    var baseImgPath = "/bmfsweb/" + site + "/image/images/";

    //Set login page service partners logo    
    $("#login-form img").attr("src", baseImgPath + "servicePartnersLogo2020.png");

    //Set login page quote machine logo
    $("#login-form-head .product-name")[0].innerHTML = "";
    $("#login-form-head .product-name").css({
        "background":"url(" + baseImgPath + "quoteMachine-logo.png) no-repeat center", 
        "background-size": "85%",
        "margin-top": "10px",        
    });
});

require(["qs_homepage"]);
})(bmref.require, bmref.requirejs, bmref.define);