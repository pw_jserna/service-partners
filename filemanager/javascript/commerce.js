


/**
 * @param dependencies {Array} name of modules this code depends on. Can exclude ".js"
 * @param callback {Function} function containing this module's functionality.
 * @version Fri Feb 25 18:44:56 2011
 */
(function(require, requirejs, define) {
	require(["bootstrap"], function(rtq) {
	  /*
	   * Put all functions for homepage here
	   */
	
	  //this function runs when the page loads
	  require.ready(function() {		  
		  if ($("#isPartnerUser_t0")[0].checked) {
			if (typeof $("#customer-ship-to-menu")[0] != "undefined"){				
				selectedShipTo = $("#customer-ship-to-menu")[0].value;		
				$("#_customer_id").val(selectedShipTo);

				// Click customer import if company name not already populated				
				companyName = $("#readonly_1__customer_t_company_name")[0].innerHTML;
				if ((companyName == "" || companyName == "&nbsp;") && selectedShipTo != "") {
					document.getElementById("cust.id_import").click();
				}
			}
									
			//Remove mandatory class for BU
			if ($("#attr_wrapper_1_businessUnit_t").has("span.readonly-wrapper").length > 0){			
				buLabel = $("#attr_wrapper_1_businessUnit_t label");
				buLabel.css("border-left", "none");
				buLabel.css("padding-left", "0px");				
			};
		  }

		  //Set current sales person
		  _setCurrentSalesPerson();

		  //update based off of freight charge
		  $("#freight_t").on("change", _modifyFreightLine);

		  //Set current selected sales person detail on sales person change
		  $("#sales-person-menu").on("change", _setCurrentSalesPersonOnChange);

		  // Hide ability to remove freight surcharge line
		  var fsDocNum = $("#fSDocNum_t").val();
		  $(".line-item[documentnumber=" + fsDocNum + "] .actions .line-item-delete").css("display", "none");  
	  });
	  $("#customer-ship-to-menu").on("change", _setCurrentCustomerShipToId);
	});	
	
	var _setCurrentCustomerShipToId = function(e){
		$('#_customer_id').val(e.target.value);		
		$('#currentSelectedCustomerShipToBU_t').val(e.target[0].attributes.businessunit.value);		
	}

	var _modifyFreightLine = function(e){
		//let val = e.target ? e.target.value || "" : "";
		//if(val>0){
			$('#modify_freight_line').click();
		/*}
		else if(val==0){
			$('#remove_freight_line').click();
		}*/
	}

	var _setCurrentSalesPerson = function() {		
		var selectedSalesPerson = $("#sales-person-menu").children("option:selected").val();		
		$('#currentSelectedSalesPerson_t').val(selectedSalesPerson);
	}

	var _setCurrentSalesPersonOnChange = function(e) {				
		$('#currentSelectedSalesPerson_t').val(e.target.value);		
		if (e.target.value != "Select sales person"){			
			$('#currentSelectedSalesPersonStatus_t').val($("#sales-person-menu").children("option:selected")[0].attributes.status.value);
		}		
	}

})(bmref.require, bmref.requirejs, bmref.define);
