
/**
 * @param dependencies {Array} name of modules this code depends on. Can exclude ".js"
 * @param callback {Function} function containing this module's functionality.
 * @version Fri Feb 25 18:44:56 2011
 */

 
(function(require, requirejs, define) {
    require(["CarrierSearch", "dataTables.sm_mod"], function() {
  
      require.ready(function() {
        (function ($) {
                $(function () {
                	
                    transId = $('input[name="id"]').val();
                    if(!$('input[name="id"]').length) {
                        transId = $('input[name="bs_id"]').val();
                    }
                    linkUrl = "https://" + _BM_HOST_COMPANY + ".bigmachines.com/commerce/buyside/document.jsp?formaction=performAction&document_id=36244074&action_id=36244076&version_id=36282630&id=" + transId;   
                    $('#externalConfigDiv').prepend('<p align="center"><a href="' + linkUrl + '">Return to Quote</a></p>');
                    
                    
                    if($("input[name='_step_varname']").length && $("input[name='_step_varname']").val() == "signed") {
                                $('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', 'https://' + _BM_HOST_COMPANY + '.bigmachines.com/bmfsweb/' + _BM_HOST_COMPANY + '/image/css/jquery.dataTables.css') );
                                $('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', 'https://' + _BM_HOST_COMPANY + '.bigmachines.com/bmfsweb/' + _BM_HOST_COMPANY + '/image/css/bootstrap.min.css') );
                        CarrierSearch.init();        
                    }
                    

                });
        })(jQuery); 
      });
    });
})(bmref.require, bmref.requirejs, bmref.define);
/*

(function ($) {
    $(function () {
        transId = $('input[name="id"]').val();
        if(!$('input[name="id"]').length) {
            transId = $('input[name="bs_id"]').val();
        }
        linkUrl = "https://" + _BM_HOST_COMPANY + ".bigmachines.com/commerce/buyside/document.jsp?formaction=performAction&document_id=36244074&action_id=36244076&version_id=36282630&id=" + transId;   
        $('#externalConfigDiv').prepend('<p align="center"><a href="' + linkUrl + '">Return to Quote</a></p>');
        
        
        if($("input[name='_step_varname']").length) {
                    $('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', 'https://' + _BM_HOST_COMPANY + '.bigmachines.com/bmfsweb/' + _BM_HOST_COMPANY + '/image/css/jquery.dataTables.css') );
                    $('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', 'https://' + _BM_HOST_COMPANY + '.bigmachines.com/bmfsweb/' + _BM_HOST_COMPANY + '/image/css/bootstrap.min.css') );
            CarrierSearch.init();        
        }

    });
})(jQuery);
*/ 