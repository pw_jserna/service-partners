# Get account details
Calls CPQ internal rest service to retrieve accounts based on provided query params.


## Syntax
```
getAccountDetails(params);
```

## Returns
Type|Description
-|-
```Json```|

## Parameters
Name|Type|Description
-|-|-
```params```|```Json```|

## Attributes
Name|Description
-|-
```_supplier_company_name```|

## Functions
Type|Function Signature
-|-
```String```|[Dictionary getEnvironmentVariableValues(String siteName, String[] variables)](Dictionary getEnvironmentVariableValues)
```String```|[parseRestErrorResponse(String Dictionary errorResponse)](parseRestErrorResponse)

## Source
```Java
// Get CPQ API details
variables = string[]{"cpqApiUsername", "cpqApiPassword", "cpqRestEndpointV8"};
apiUserDetailsDict = util.getEnvironmentVariableValues(_supplier_company_name, variables);
apiUsername = get(apiUserDetailsDict,"cpqApiUsername");
apiPass = get(apiUserDetailsDict,"cpqApiPassword");
baseRestEndpoint = get(apiUserDetailsDict,"cpqRestEndpointV8");   

// Format query params
queryParamsStr = jsontostr(params);
formattedQueryParamsstr = replace(queryParamsStr," ", "%20");
formattedQueryParamsstr = replace(formattedQueryParamsstr ,"&", "%26");

// Accounts rest service endpoint
getAccountDetailsEndpoint= baseRestEndpoint + "/accounts?q=" + formattedQueryParamsstr;

// Set up request headers
requestHeaders = dict("string");
encodecredential = encodebase64(apiUsername + ":" + apiPass); // for Authentication
authstring="Basic " + encodecredential;
put(requestHeaders, "Authorization", authstring);
put(requestHeaders, "Content-Type", "application/json");

// Send request
response = urldata(getAccountDetailsEndpoint, "GET", requestHeaders);

// Parse response
responseCode = get(response, "Status-Code");
if (responseCode <> "200") {
    // Handle error responses from the service call
    errorMessage = util.parseRestErrorResponse(response);   
    messageText = "An error occurred while trying retrieve accounts: ";
    messageText = messageText + errorMessage;
    throwerror(messageText, false);
    return json();
}

accountsJson = json(get(response, "Message-Body"));

return accountsJson;
```

## Revision History
 Rev. Date |Developer            |Notes / Comments
 ----------|---------------------|-------------------------------------------------------------------------
 2020-02-06|Jserna               |Initial version
