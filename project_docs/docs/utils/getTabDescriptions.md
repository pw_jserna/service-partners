# Get tab descriptions



## Syntax
```
getTabDescriptions(params);
```

## Returns
Type|Description
-|-
```Json```|

## Parameters
Name|Type|Description
-|-|-
```params```|```Json```|



## Source
```Java
descJson = json();
tabType = jsonget(params, "tabType", "string", "");
tabOption = jsonget(params, "tabOption", "string", "");

orderLineTabDesc = "";
outputDocTabDesc = "";
cutSheetTabDesc = "";

descriptions = bmql("SELECT * FROM TabDescription WHERE TabType = $tabType AND (TabOption = $tabOption OR TabOption IS NULL)");
for desc in descriptions {
    orderLineTabDesc = get(desc, "OrderLineDesc");
    outputDocTabDesc = get(desc, "OutputSummaryDesc");
    cutSheetTabDesc = get(desc, "OutputCutSheetDesc");

    jsonput(descJson, "OrderLineTabDesc", orderLineTabDesc);
    jsonput(descJson, "OutputSummaryTabDesc", outputDocTabDesc);
    jsonput(descJson, "OutputCutSheetTabDesc", upper(cutSheetTabDesc));
}

return descJson;
```

## Revision History
 Rev. Date |Developer            |Notes / Comments
 ----------|---------------------|-------------------------------------------------------------------------
 2020-01-30|Jserna               |Initial version
 2020-03-03|jonathan.serna@piercewashington.com|
