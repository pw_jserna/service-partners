# External customer quote access
Determines whether a quote should be forwarded to an external customer.


## Syntax
```
externalCustomerQuoteAccess(params);
```

## Returns
Type|Description
-|-
```String```|

## Parameters
Name|Type|Description
-|-|-
```params```|```Json```|



## Source
```Java
retStr = "";

/** Forwarding logic for exeternal customers **/
CONST_EXT_SALES_GROUP = "External Sales User";

// Get param values
isPartnerUser = jsonget(params, "isPartnerUser", "boolean", false);
creatorCompany = jsonget(params, "creatorCompany", "string", "");
shipToId = jsonget(params, "shipToId", "string", "");
transactionCompanyName = jsonget(params, "transactionCompanyName", "string", "");
currentUserCompanyName = jsonget(params, "currentUserCompanyName", "string", "");

quoteCompanyName = upper(transactionCompanyName);
userCompanyName = "";
// If quote created by customer then we already have a valid company name
if (isPartnerUser AND currentUserCompanyName == creatorCompany) {
    userCompanyName = creatorCompany;
} else {
    // We need to get valid company login from customer map
    customerMapRecs = bmql("SELECT * FROM CustomerMap WHERE CompanyName = $quoteCompanyName");
    companyLogin = "";
    for rec in customerMapRecs {
        companyLogin = get(rec, "CompanyLogin");
    }
    userCompanyName = companyLogin;
}

// Get list of users within selected company name and determine quote access rights
userAccessList = bmql("SELECT * FROM CustomerAccess WHERE CompanyLogin = $userCompanyName OR OtherCompanyIdAccess IS NOT NULL");
for rec in userAccessList {
	customerIdAcesss = get(rec, "CustomerIdAccess");
    customerIdAccessArr = split(customerIdAcesss, ";");
    otherCompanyIdAccess = get(rec, "OtherCompanyIdAccess");
    otherCompanyIdAccessArr = split(otherCompanyIdAccess, ";");
    quoteAccessLevel = upper(get(rec, "QuoteAccessLevel"));
	userLogin = get(rec, "UserLogin");     
    companyLogin = get(rec, "CompanyLogin");
    accessStr = userLogin + "~" + companyLogin + "~" + CONST_EXT_SALES_GROUP + "|";   
    customerIdAccessAllowed = findinarray(customerIdAccessArr, shipToId);
    otherCompanyIdAccessAllowed = findinarray(otherCompanyIdAccessArr, shipToId);  

    // Provide access based on user's defined company id access
    if (companyLogin == userCompanyName) {
        if (quoteAccessLevel == "*" AND (customerIdAcesss == "*" OR customerIdAccessAllowed <> -1)){
        // User has full quote access and has all customer id access or access to selected customer id 
		retStr = retStr + accessStr;
        } elif (userCompanyName == creatorCompany AND quoteAccessLevel == "COMPANY" AND (customerIdAcesss == "*" OR customerIdAccessAllowed <> -1)) {
            // User has access company created quotes with either all customer id access or access to selected customer id 
            retStr = retStr + accessStr;
        } elif (userCompanyName <> creatorCompany AND quoteAccessLevel =="SPCREATED" AND (customerIdAcesss == "*" OR customerIdAccessAllowed <> -1)) {
            // User has access SP created quotes with either all customer id access or access to selected customer id 
            retStr = retStr + accessStr;
        }
    }	

    // Provide access based on user's defined other company id access
    if (quoteAccessLevel == "*" AND otherCompanyIdAccessAllowed <> -1) {
        retStr = retStr + accessStr;
    } elif (userCompanyName == creatorCompany AND quoteAccessLevel == "COMPANY" AND otherCompanyIdAccessAllowed <> -1) {
        retStr = retStr + accessStr;
    } elif (userCompanyName <> creatorCompany AND quoteAccessLevel =="SPCREATED" AND otherCompanyIdAccessAllowed <> -1) {
        retStr = retStr + accessStr;
    }
}

return retStr;
```

## Revision History
 Rev. Date |Developer            |Notes / Comments
 ----------|---------------------|-------------------------------------------------------------------------
 2019-12-13|JSerna               |Initial version
 2020-02-05|JSerna               |Provide access to other company ids if user has the access
