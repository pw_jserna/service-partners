# List Part Details
return a JSON data for the list of input part numbers by looking up the parts db


## Syntax
```
listPartDetails(partsList);
```

## Returns
Type|Description
-|-
```Json```|

## Parameters
Name|Type|Description
-|-|-
```partsList```|```String[]```|



## Source
```Java
/*Ramesh - Jan 28, 2019 - Created */

retObj = json();
querySelectFields = String[];
append(querySelectFields, "part_number");
append(querySelectFields, "price");  
append(querySelectFields, "description");  
append(querySelectFields, "custom_field8");  
append(querySelectFields, "custom_field9");  
append(querySelectFields, "custom_field10");  
append(querySelectFields, "custom_field11");  
append(querySelectFields, "custom_field15");  
querySelectClause = join(querySelectFields, ", ");

rs = bmql("select $querySelectClause from _parts where part_number in $partsList");

for rec in rs {
    partNum = get(rec, "part_number"); 
    price = get(rec, "price");
    description = get(rec, "description");
    quantityUOM = get(rec,"custom_field15");
    pricingUOM = get(rec,"custom_field11");
    rValue = get(rec, "custom_field8");
    thickness = get(rec, "custom_field9");
    width = get(rec, "custom_field10");

    innerObj = jsonget(retObj, partNum, "json", json());
	jsonput(innerObj, "Price", price);
    jsonput(innerObj, "Description", description);
    jsonput(innerObj, "QuantityUOM", quantityUOM);
    jsonput(innerObj, "PricingUOM", pricingUOM);
    jsonput(innerObj, "RValue", rValue);
    jsonput(innerObj, "Thickness", thickness);
    jsonput(innerObj, "Width", width);
	jsonput(retObj, partNum, innerObj);
}

return retObj;
```

## Revision History
 Rev. Date |Developer            |Notes / Comments
 ----------|---------------------|-------------------------------------------------------------------------
 2020-01-10|Rlamsal              |Initial version
 2020-01-10|Jserna               |Added description to innerObj
 2020-01-20|Jserna               |Additional part fields returned
