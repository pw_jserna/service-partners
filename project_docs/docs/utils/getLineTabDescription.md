# Get line tab description
Gets the appropriate tab description for a provided insulation type, tab type, and tab option.


## Syntax
```
getLineTabDescription(params);
```

## Returns
Type|Description
-|-
```Json```|

## Parameters
Name|Type|Description
-|-|-
```params```|```Json```|



## Source
```Java
// Get param values
tab = jsonget(params, "tab", "string", "");
tabType = jsonget(params, "tabType", "string", "");
insulationType = jsonget(params, "insulationType", "string", "");
tabOption = jsonget(params, "tabOption", "string", "");

// Initialize tab desc vars
outputSummaryTabDesc = "";
outputCutSheetLineDesc = "";
orderLineTabDesc = "";

outputSummaryTabDesc = " [" + tab + "\" Tab]";
outputCutSheetLineDesc = "";
if ((insulationType == "Standard MBI System" OR insulationType == "Sag and Bag System") AND tabType == "Tape Tab") {
	outputSummaryTabDesc = " [" + tab + "\" ";
	if (tabOption == "1-6\" Single Tape Tab (SI)") {
	    outputSummaryTabDesc = outputSummaryTabDesc + "Tape Tab (SI)]";
        orderLineTabDesc = tab + " SI";
        outputCutSheetLineDesc = tab + "\" SI";
	} elif (tabOption == "2-3\" Single Tape Tab (SO)") {
	    outputSummaryTabDesc = outputSummaryTabDesc + "Tape Tab (SO)]";
        orderLineTabDesc = tab + " SO";
	} elif (tabOption == "2-3\" Double Tape Tabs (DO)") {
	    outputSummaryTabDesc = outputSummaryTabDesc + "Tape Tabs (DO)]";
        orderLineTabDesc = tab + " DO";
	} elif (tabOption == "2-3\" Double Tape Tabs (DI)") {
	    outputSummaryTabDesc = outputSummaryTabDesc + "Tape Tabs (DI)]";
        orderLineTabDesc = tab + " DI";
	} else {
	    outputSummaryTabDesc = outputSummaryTabDesc + "Tab]";
	}
}

return json();
```

## Revision History
 Rev. Date |Developer            |Notes / Comments
 ----------|---------------------|-------------------------------------------------------------------------
 2020-01-06|Jserna               |Initial version
