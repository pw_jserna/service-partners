# Get tab description
Gets the appropriate tab description for a provided insulation type, tab type, and tab option.
 



## Syntax
```
getTabDescription(params);
```

## Returns
Type|Description
-|-
```String```|

## Parameters
Name|Type|Description
-|-|-
```params```|```Json```|



## Source
```Java
// Get param values
tab = jsonget(params, "tab", "string", "");
tabType = jsonget(params, "tabType", "string", "");
insulationType = jsonget(params, "insulationType", "string", "");
tabOption = jsonget(params, "tabOption", "string", "");

retStr = " [" + tab + "\" Tab]";

if ((insulationType == "Standard MBI System" OR insulationType == "Sag and Bag System") AND tabType == "Tape Tab") {
	tab = " [" + tab + "\" ";
	if (tabOption == "1-6\" Single Tape Tab (SI)") {
	    tab = tab + "Tape Tab (SI)]";
	} elif (tabOption == "2-3\" Single Tape Tab (SO)") {
	    tab = tab + "Tape Tab (SO)]";
	} elif (tabOption == "2-3\" Double Tape Tabs (DO)") {
	    tab = tab + "Tape Tabs (DO)]";
	} elif (tabOption == "2-3\" Double Tape Tabs (DI)") {
	    tab = tab + "Tape Tabs (DI)]";
	} else {
	    tab = tab + "Tab]";
	}	
	retStr = tab;
}

return retStr;
```

## Revision History
 Rev. Date |Developer            |Notes / Comments
 ----------|---------------------|-------------------------------------------------------------------------
 2019-12-09|jonathan.serna@piercewashington.com|test12334LOAD
