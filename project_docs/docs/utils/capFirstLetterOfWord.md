# Cap First Letter Of Word
Capitalize the first letter and lower rest of the letters for each word in the input string


## Syntax
```
capFirstLetterOfWord(inputString);
```

## Returns
Type|Description
-|-
```String```|

## Parameters
Name|Type|Description
-|-|-
```inputString```|```String```|



## Source
```Java
/*Rlamsal - Created July 11, 2019 */
ret = "";
procArr = split(inputString, " ");
for item in procArr {
	if(len(item) >=  1) {
		firstLetter = substring(item, 0,1);				
		restLetters = substring(item, 1);		
		if(ret == "") {
			ret = upper(firstLetter) + lower(restLetters);
		}
		else {
			ret = ret + " " + upper(firstLetter) + lower(restLetters);
		}		
	}
}
return ret;
```

## Revision History
 Rev. Date |Developer            |Notes / Comments
 ----------|---------------------|-------------------------------------------------------------------------
 2020-12-14|JSerna               |Chars skipped if arr item len is 1. Fix to check >= 1
