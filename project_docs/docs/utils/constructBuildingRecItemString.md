# Construct Building Rec Item String



## Syntax
```
constructBuildingRecItemString(params);
```

## Returns
Type|Description
-|-
```String```|

## Parameters
Name|Type|Description
-|-|-
```params```|```Json```|



## Source
```Java
ret = "";
item = jsonget(params, "item", "string", "");
itemObj = jsonget(params, "itemObj", "json", json());
businessUnit = jsonget(params, "businessUnit", "string", "");
buildingLabel = jsonget(params, "buildingLabel", "string", "");
roofType = jsonget(params, "roofType", "string", "");
partsJObj = jsonget(params, "parts", "json", json());
jObj = jsonget(params, "buildingConfigJson", "json", json());
i = jsonget(params, "index", "integer", 0);
partitionWallIdx = jsonget(params, "partitionWallIdx", "integer", 1);

jsonput(itemObj, "Section", item); 
modelVar = "";
isSideWall = find(lower(item),"sidewall");
isEndWall = find(lower(item),"endwall");
isCombinedWalls = find(lower(item),"combinedwalls");
isCombinedPartitionWalls = find(lower(item),"combinedpartitionwalls");

if(item == "Roof") {
    modelVar = "roof";
    jsonput(itemObj, "rfType", roofType);
}
elif(isSideWall <> -1) {
    modelVar = "sideWall";
}
elif(isEndWall <> -1) {
    modelVar = "endWall";
}
elif(item == "PartitionWall"){
    modelVar = "partitionWall";     
}
elif(isCombinedWalls <> -1 OR isCombinedPartitionWalls <> -1) {
    modelVar = "combinedWalls";
    if (isCombinedPartitionWalls <> -1) {
        jsonput(itemObj, "Label", "ADDITIONAL WALLS");    
    } else {
        jsonput(itemObj, "Label", "WALLS");        
    }    
}
if(modelVar == "") {
    return "";
}
label = jsonget(itemObj, "Label", "string", "");
if(label == "" and modelVar == "combinedWalls") {
    if (isCombinedPartitionWalls <> -1){
        label = "Additional Walls";
    } else {
        label = "Walls";
    }    
}
thickness = jsonget(itemObj, "Thickness", "string", "");
thicknessTop = jsonget(itemObj, "ThicknessTop", "string", "");
thicknessBottom = jsonget(itemObj, "ThicknessBottom", "string", "");
facing = jsonget(itemObj, "FacingMaterial", "string", "");
rValue = jsonget(itemObj, "RValue", "string", "");
rValueTop = jsonget(itemObj, "RValueTop", "string", "");
rValueBottom = jsonget(itemObj, "RValueBottom", "string", "");
rValueCombined = jsonget(itemObj, "CombinedRValue", "string", "");
tabType = jsonget(itemObj, "TabType", "string", ""); // added sept 20
tabOption = jsonget(itemObj, "TabOption", "string", "");
installationMethod = jsonget(itemObj, "InstallationMethod", "string", "");
linerDesc = rValue;

if(installationMethod == "Liner System" and (modelVar == "sideWall" or modelVar == "endWall" or modelVar == "partitionWall")) {
    return "";
}	

j = 0;
ret = ret + "insulationSystems:fiberglassInsulation:" + modelVar + "~1" + "~" + jsontostr(itemObj) + "~0.0~" + string(i) + "|^|";
if(installationMethod == "Liner System") {
    if(rValueCombined <> "") {
        linerDesc = rValueCombined;
    }

    linerObj = json();
    
    if(modelVar == "combinedWalls") {
        linerObj = json();
        linerListArr = jsonarray();
        if (isCombinedPartitionWalls <> -1) {
            linerObj = jsonget(itemObj, "Liner", "json", json());
        } else {
            linerListArr = jsonget(itemObj, "Liner", "jsonarray", jsonarray("[]"));
        }
        
        if (jsonarraysize(linerListArr) > 0) {
            linerObj = jsonarrayget(linerListArr, 0, "json");
        }                	
    }
    else {
        linerObj = jsonget(itemObj, "Liner", "json", json());
    }
    linerDesc = linerDesc + " " + jsonget(linerObj, "type", "string", "");
    linerOption = jsonget(linerObj, "option", "string", "");
    if(linerOption <> "") {
        linerDesc = linerDesc + " " + linerOption;
    }

    jsonput(linerObj, "installationMethod", installationMethod);
    jsonput(linerObj, "BuildingLabel", buildingLabel);
    jsonput(linerObj, "section", item);
    jsonput(linerObj, "sectionLabel", label);
    jsonput(linerObj, "partType", "Liner");
    jsonput(linerObj, "linerDesc", linerDesc);
    jsonput(linerObj, "tabType", tabType); //new 
    jsonput(linerObj, "tab", tabOption);

    linerPart = jsonget(linerObj, "partNumber", "string", "");
    if(linerPart <> "") {
        linerPart = businessUnit + "-" + linerPart;
            if(jsonpathcheck(partsJObj, "$." + linerPart) == false) {
        linerPart = "insulationSystems:fiberglassInsulation:newPart";
    }
        j = j + 1;
        linerQty = jsonget(linerObj, "area", "float", 0.0);
        intQty = integer(round(linerQty, 0));
        ret = ret + linerPart + "~" + string(intQty) + "~" + jsontostr(linerObj) + "~~" + string(i) + "." + string(j) + "|^|";
    }
}
rollList = jsonget(itemObj, "rolls", "jsonarray", jsonarray());

jListCount = range(jsonarraysize(rollList));

for count in jListCount {
    j = j + 1;
    rollObj = jsonarrayget(rollList, count, "json");
    qty = jsonget(rollObj, "surfaceArea", "float", 0.0);
    integerQty = integer(round(qty, 0));        
    partNum = businessUnit + "-" + jsonget(rollObj, "partNumber");
    jsonput(rollObj, "facing", facing);
    jsonput(rollObj, "thickness", thickness);
    jsonput(rollObj, "thicknessTop", thicknessTop);
    jsonput(rollObj, "thicknessBottom", thicknessBottom);
    jsonput(rollObj, "rvalue", rValue);
    jsonput(rollObj, "rvalueTop", rValueTop);
    jsonput(rollObj, "rvalueBottom", rValueBottom);
    jsonput(rollObj, "section", item);
    jsonput(rollObj, "tabType", tabType);
    jsonput(rollObj, "tab", tabOption);
    jsonput(rollObj, "BuildingLabel", buildingLabel);
    jsonput(rollObj, "installationMethod", installationMethod);
    jsonput(rollObj, "sectionLabel", label);
    jsonput(rollObj, "partType", "Insulation");
    jsonput(rollObj, "linerDesc", linerDesc);    
    
    price = "0.0";
    comment = jsontostr(rollObj);
    //append(partsList, partNum);
    if(jsonpathcheck(partsJObj, "$." + partNum) == false) {
        partNum = "insulationSystems:fiberglassInsulation:newPart";
    }
    ret = ret + partNum + "~" + string(integerQty) + "~" + comment + "~" + price + "~" + string(i) + "." + string(j) + "|^|";
}

accessList = jsonget(itemobj, "Accessories", "jsonarray", jsonarray());
accessCount = range(jsonarraysize(accessList));
accessType = "Walls-Accessories";
if(item == "Roof") {
    accessType = "Roof-Accessories";
}
if (isCombinedPartitionWalls <> -1) {
    accessType = "Partition-Walls-Accessories";
}
for count in accessCount {
    accessObj = jsonarrayget(accessList, count, "json");
    jsonput(accessObj, "section", accessType);
    jsonput(accessObj, "BuildingLabel", buildingLabel);
    jsonput(accessObj, "partType", accessType);
    jsonput(accessObj, "installationMethod", installationMethod);
    accessPart = jsonget(accessObj, "partNumber", "string", "");
    if(accessPart <> "") {
        j = j + 1;
        fullAccessPart = businessUnit + "-" + accessPart;
        accQty = jsonget(accessObj, "quantity", "integer", 1);
        if(jsonpathcheck(partsJObj, "$." + fullAccessPart) == false) {
            fullAccessPart = "insulationSystems:fiberglassInsulation:newPart";
        }
        ret = ret + fullAccessPart + "~" + string(accQty) + "~" + jsontostr(accessObj) + "~~" + string(i) + "." + string(j) + "|^|";
        
    }   
}

return ret;
```

## Revision History
 Rev. Date |Developer            |Notes / Comments
 ----------|---------------------|-------------------------------------------------------------------------
 2019-12-13|JSerna               |Initial version
 2019-12-17|JSerna               |Combined partition wall handling
 2020-01-08|JSerna               |Use partition wall label now that it is being passed in CDS payload
