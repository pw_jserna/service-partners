# Float to formatted currency



## Syntax
```
floatToFormattedCurrency(params);
```

## Returns
Type|Description
-|-
```String```|

## Parameters
Name|Type|Description
-|-|-
```params```|```Json```|



## Source
```Java
trimLeadingZero = jsonget(params, "trimLeadingZero", "boolean", true);
currency = jsonget(params, "currencyCode", "string", "USD");
currencySymbol = jsonget(params, "currencySymbol", "string", "$");
floatValue = jsonget(params, "floatValue", "float", 0.00);
retStr = string(floatValue);

floatValueStr = string(floatValue);
leadingZeroIdx = find(floatValueStr, "0");
if (floatValue <> 0){
    if (trimLeadingZero) {                
        if (leadingZeroIdx == 0) {
            retStr = currencySymbol + substring(floatValueStr, leadingZeroIdx + 1);
        } else {
            retStr = currencySymbol + floatValueStr;
        }
    } else {
            retStr = currencySymbol + floatValueStr;
    }	
} else {
	retStr = formatascurrency(atof(floatValueStr),currency);
}

numOfDecimalVals = len(substring(retStr,find(retStr, ".")+1));
//Handle one decimal place
if (numOfDecimalVals == 1) {
    retStr = retStr + "00";
}

//Handle two decimal places
if (numOfDecimalVals == 2) {
    retStr = retStr + "0";
}

return retStr;
```

## Revision History
 Rev. Date |Developer            |Notes / Comments
 ----------|---------------------|-------------------------------------------------------------------------
 2020-01-15|Jserna               |Initial version
 2020-01-20|Jserna               |Handle one/two decimal place(S)
