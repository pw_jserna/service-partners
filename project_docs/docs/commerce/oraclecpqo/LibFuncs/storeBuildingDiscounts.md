# Store building discounts
Stores building discounts applied by the user.


## Syntax
```
storeBuildingDiscounts(params);
```

## Returns
Type|Description
-|-
```Json```|

## Parameters
Name|Type|Description
-|-|-
```params```|```Json```|

## Attributes
Name|Description
-|-
```_document_number```|
```_part_display_number```|
```customDiscountAmount_l```|
```customDiscountValue_l```|
```customDiscountType_l```|
```_system_selected_document_number```|


## Source
```Java
buildingDiscountsJson = json();

for line in transactionLine {
	if (line._parent_doc_number == _system_selected_document_number) {        
        partDiscountJson = json();
        discountType = line.customDiscountType_l; 
        discountValue = line.customDiscountValue_l;
        accessoryQtyOverride = line.accessoryQtyOverride_l;
        partDiscountJsonPopulated = false;

        // Set discountValue to discount amount attr if type is Amount Off        
        if ( discountType == "Amount Off") {
            discountValue = line.customDiscountAmount_l;
        }

        // Store custom accessory qty if entered
        if (accessoryQtyOverride <> 0) {
            jsonput(partDiscountJson, "accessoryQtyOverride", accessoryQtyOverride);
            partDiscountJsonPopulated = true;
        }

        // Store discount values if populated
        if (discountType <> "" AND discountValue <> 0) {
            jsonput(partDiscountJson, "discountType", line.customDiscountType_l);
            jsonput(partDiscountJson, "discountValue", discountValue); 
            partDiscountJsonPopulated = true;           
        }        

        // Store partDiscountJson in buildingDiscountsJson if not empty
        if (partDiscountJsonPopulated) {
            jsonput(buildingDiscountsJson, line._part_display_number, partDiscountJson);
        }
    }
}

return buildingDiscountsJson;
```

## Revision History
 Rev. Date |Developer            |Notes / Comments
 ----------|---------------------|-------------------------------------------------------------------------
 2020-01-23|Jserna               |Initial version
