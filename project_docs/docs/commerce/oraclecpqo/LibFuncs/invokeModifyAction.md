# Invoke modify action



## Syntax
```
invokeModifyAction(params);
```

## Returns
Type|Description
-|-
```String```|

## Parameters
Name|Type|Description
-|-|-
```params```|```Json```|

## Attributes
Name|Description
-|-
```bs_id```|
```_system_supplier_company_name```|

## Functions
Type|Function Signature
-|-
```String```|[Dictionary getEnvironmentVariableValues(String siteName, String[] variables)](Dictionary getEnvironmentVariableValues)
```String```|[parseRestErrorResponse(String Dictionary errorResponse)](parseRestErrorResponse)

## Source
```Java
CONST_COMMERCE_PROCESS_VARNAME = "Oraclecpqo";
CONST_COMMERCE_HEADER_DOCUMENT_VARNAME = "Transaction";

action = jsonget(params, "actionVariableName", "string", "");

// Get CPQ API details
variables = string[]{"cpqApiUsername", "cpqApiPassword", "cpqRestEndpointV8"};
apiUserDetailsDict = util.getEnvironmentVariableValues(_system_supplier_company_name, variables);
apiUsername = get(apiUserDetailsDict,"cpqApiUsername");
apiPass = get(apiUserDetailsDict,"cpqApiPassword");
baseRestEndpoint = get(apiUserDetailsDict, "cpqRestEndpointV8");

// Set up request headers
requestHeaders = dict("string");
encodecredential = encodebase64(apiUsername + ":" + apiPass); // for Authentication
authstring="Basic " + encodecredential;
put(requestHeaders, "Authorization", authstring);
put(requestHeaders, "Content-Type", "application/json");

// Action endpoint
actionEndpoint = baseRestEndpoint + "/commerceDocuments" + CONST_COMMERCE_PROCESS_VARNAME + CONST_COMMERCE_HEADER_DOCUMENT_VARNAME + "/" + bs_id + "/actions/" + action;

// Set up request body;
requestBody = json();

// Execute action request
response = urldata(actionEndpoint, "POST", requestHeaders, jsontostr(requestBody));

responseCode = get(response, "Status-Code");
if (responseCode <> "200") {
    // Handle error responses from the service call
    errorMessage = util.parseRestErrorResponse(response);   
    messageText = "An error occurred while trying to excute fetch price: ";
    messageText = messageText + errorMessage;
    return "";
}

responseBody = get(response, "Message-Body");

return responseBody;
```

## Revision History
 Rev. Date |Developer            |Notes / Comments
 ----------|---------------------|-------------------------------------------------------------------------
 2020-01-06|jonathan.serna@piercewashington.com|
