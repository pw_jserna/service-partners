# Building Accessory String Construct
Returns the code delimited string to form the Accessory Table under the Quote Summary table of the Quote Summary output Document.


## Syntax
```
buildingAccessoryStringConstruct();
```

## Returns
Type|Description
-|-
```String```|


## Attributes
Name|Description
-|-
```currency_t```|
```section_l```|
```requestedQuantity_l```|
```_part_custom_field15```|
```isLinerPart_l```|
```netPrice_l```|

## Functions
Type|Function Signature
-|-
```String```|[floatToFormattedCurrency(Json params)](floatToFormattedCurrency)

## Source
```Java
retStr = "";
accessoriesQuantityTotalDict = dict("integer");
accessoriesAmtDict = dict("float");
accessoriesUOMDict = dict("string");

for buildingLine in transactionLine {	
    if(startswith(buildingLine.section_l, "AccessoryPart")) { 
        //For Section=Accessories.
        dashPos = find (buildingLine.section_l, "-");
        accessoryKey = substring(buildingLine.section_l, dashPos+1);
        if(startswith(upper(accessoryKey), "DOUBLE FACE TAPE") OR startswith(upper(accessoryKey), "DOUBLE FACED TAPE")) {
            accessoryKey = "Double Faced Tape";
        }
        //print "accessoryKey: " + "\"" + accessoryKey + "\"";
        // Quantity:
        quantity = 0;
        if(containskey(accessoriesQuantityTotalDict, accessoryKey)) {
            quantity = get(accessoriesQuantityTotalDict, accessoryKey);
        }
        quantity = quantity + buildingLine.requestedQuantity_l;
        put(accessoriesQuantityTotalDict, accessoryKey, quantity);
        
        // Quantity UOM:
        if(NOT(containskey(accessoriesAmtDict, accessoryKey))) {
            uom = "";
            if(buildingLine._part_custom_field15 <> "" AND NOT(isnull(buildingLine._part_custom_field15))) {
                uom = buildingLine._part_custom_field15;
            }
            put(accessoriesUOMDict, accessoryKey, uom);
        }
        
        // Unit price
        put(accessoriesAmtDict,accessoryKey,buildingLine.netPrice_l);
    // new sept 23 2019
    } elif(buildingLine.isLinerPart_l AND (startswith(upper(buildingLine.section_l), "ROOF-ACCESSORIES") OR startswith(upper(buildingLine.section_l), "WALLS-ACCESSORIES") OR startswith(upper(buildingLine.section_l), "PARTITION-WALLS-ACCESSORIES"))) {
        //For Section= Liner Accessories.
        dashPos = find(buildingLine.section_l, "-");
        accessoryKey = substring(buildingLine.section_l, dashPos+1);
        if(startswith(upper(accessoryKey), "ACCESSORIES-")) {
            accessoryKey = substring(accessoryKey, len("ACCESSORIES-"));
        }
        if(startswith(upper(buildingLine.section_l), "PARTITION-WALLS-ACCESSORIES")) {
            accessoryKey = substring(buildingLine.section_l, len("PARTITION-WALLS-ACCESSORIES-"));
        }
        if(startswith(upper(accessoryKey), "DOUBLE FACE TAPE") OR startswith(upper(accessoryKey), "DOUBLE FACED TAPE")) {
            accessoryKey = "Double Faced Tape";
        }
        // Quantity:
        quantity = 0;
        if(containskey(accessoriesQuantityTotalDict, accessoryKey)) {
            quantity = get(accessoriesQuantityTotalDict, accessoryKey);
        }
        quantity = quantity + buildingLine.requestedQuantity_l;
        put(accessoriesQuantityTotalDict, accessoryKey, quantity);
        
        // Quantity UOM:
        if(NOT(containskey(accessoriesUOMDict, accessoryKey))) {
            uom = "";
            if(buildingLine._part_custom_field15 <> "" AND NOT(isnull(buildingLine._part_custom_field15))) {
                uom = buildingLine._part_custom_field15;
            }
            put(accessoriesUOMDict, accessoryKey, uom);
        }
        
        // Unit price
        put(accessoriesAmtDict,accessoryKey,buildingLine.netPrice_l);
    }
}
    
//print "accessoriesQuantityTotalDict: "; print accessoriesQuantityTotalDict;   
    
rowDelim = "#@#";
colDelim = "#$#";

accKeys = keys(accessoriesQuantityTotalDict);
for eachAcc in accKeys {
    accessories = eachAcc;   
    // Set accessory name and quantity in return str 
    retStr = retStr + accessories + colDelim + string(get(accessoriesQuantityTotalDict,eachAcc));
    
    // Set unit price in return str
    unitPrice = get(accessoriesAmtDict,eachAcc);          
    retStr = retStr + colDelim + " @ " + formatascurrency(unitPrice, currency_t);

    // Set UOM in return str if not blank
    if(get(accessoriesUOMDict, eachAcc) <> "") {
        retStr = retStr + " / " + get(accessoriesUOMDict, eachAcc);
    }
    retStr = retStr + colDelim + rowDelim;
}

return retStr;
```

## Revision History
 Rev. Date |Developer            |Notes / Comments
 ----------|---------------------|-------------------------------------------------------------------------
 2019-11-14|JSerna               |Initial version
 2020-01-14|JSerna               |Handle partition wall accessories and add unit price
 2020-01-17|Jserna               |Adding "@" before unit price
 2020-01-28|Jserna               |SP-118: Display unit price to 2 decimals
