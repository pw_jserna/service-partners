# Set customer account defaults



## Syntax
```
setCustomerAccountDefaults(params);
```

## Returns
Type|Description
-|-
```String```|

## Parameters
Name|Type|Description
-|-|-
```params```|```Json```|

## Attributes
Name|Description
-|-
```_transaction_customer_t_company_name```|
```isPartnerUser_t```|
```businessUnit_t```|
```_system_user_bill_company```|
```_system_user_login```|
```_system_company_name```|

## Functions
Type|Function Signature
-|-
```Json```|[getAccountDetails(Json params)](getAccountDetails)

## Source
```Java
CONST_DEFAULT_BU = "1901";
retStr = "";
action = jsonget(params, "action", "string", "LINE_DEFAULT");

inputParams = json();
customerAccountsJson = json();
companyName = "";
if (isPartnerUser_t) {
    jsonput(inputParams, "companyName", _system_user_bill_company);
} else {
    jsonput(inputParams, "companyName",_transaction_customer_t_company_name);
}

// 1. Get account details based on the user bill company 
accountDetailsJson = util.getAccountDetails(inputParams);
accountIds = jsonpathgetmultiple(accountDetailsJson, "$..customerId");

// 2. Get customer business unit details
customerBusinessUnitDetails = json();
querySelectFields = String[];
append(querySelectFields, "CustomerBusinessUnit.CustomerId");
append(querySelectFields, "CustomerBusinessUnit.BUId");  
append(querySelectFields, "CustomerBusinessUnit.CompanyName");  
append(querySelectFields, "BusinessUnit.InsideSalesDL");  
querySelectClause = join(querySelectFields, ", ");

// Query JOIN clause
queryJoinClause = "CustomerBusinessUnit INNER JOIN BusinessUnit ON ";
queryJoinClause = queryJoinClause + "(CustomerBusinessUnit.BUId = BusinessUnit.BUId)";
custBURecs = bmql("SELECT $querySelectClause FROM $queryJoinClause");
for rec in custBURecs {
    custIdJson = json();
    jsonput(custIdJson, "businessUnit", get(rec, "BUId"));
    jsonput(custIdJson, "insideSalesDL", get(rec, "InsideSalesDL")); 
    jsonput(customerBusinessUnitDetails, get(rec, "CustomerId"), custIdJson);    
}

// 3. Check user account id access
accessRecs = bmql("SELECT * FROM CustomerAccess WHERE UserLogin = $_system_user_login AND CompanyLogin = $_system_company_name");
customerAccessArr = string[];
otherCompanyAccessIdArr = string[];
for rec in accessRecs {
    customerIdAccess = get(rec,"CustomerIdAccess");
    otherCompanyIdAccess = get(rec, "OtherCompanyIdAccess");
    if (customerIdAccess <> "*") {
        customerAccessArr = split(customerIdAccess, ";");
    }    
    otherCompanyAccessIdArr = split(otherCompanyIdAccess, ";");
}

// 4. Set customer ship to menu options
customerShipToJson = json();
defaultBusinessUnit = "";
if (NOT isempty(customerAccessArr)){
    // Populate menu with specific ship to ids customer has access to    
    customerAccessRng = range(sizeofarray(customerAccessArr));
    for idx in customerAccessRng {      
        // Skip iteration if empty id in tilde delimited list for e.g. 904572~
        if (customerAccessArr[idx] == "") {
            continue;
        }
        businessUnit = jsonpathgetsingle(customerBusinessUnitDetails, "$." +  customerAccessArr[idx] + ".businessUnit", "string", "");        
        // Only put into json if it is a valid id
        if (jsonpathcheck(accountDetailsJson, "$.items.[?(@.customerId == '" + customerAccessArr[idx] + "')]")) {
            jsonput(customerShipToJson, customerAccessArr[idx], businessUnit);
        }        
    }
} else {
    // User has all access therefore populate menu with all ids (sold to and related ship tos)
    accountIdsRng = range(jsonarraysize(accountIds));
    for idx in accountIdsRng {
        businessUnit = jsonpathgetsingle(customerBusinessUnitDetails, "$." + jsonarrayget(accountIds, idx) + ".businessUnit", "string", "");        
        jsonput(customerShipToJson, jsonarrayget(accountIds, idx), businessUnit);        
    }
}

// 5. Include other company account ids user has access to
otherCompanyAccessIdRng= range(sizeofarray(otherCompanyAccessIdArr));
for idx in otherCompanyAccessIdRng {      
    // Skip iteration if empty id in tilde delimited list for e.g. 904572~
    if (otherCompanyAccessIdArr[idx] == "") {
        continue;
    }
    businessUnit = jsonpathgetsingle(customerBusinessUnitDetails, "$." +  otherCompanyAccessIdArr[idx] + ".businessUnit", "string", "");        
    jsonput(customerShipToJson, otherCompanyAccessIdArr[idx], businessUnit);   
}

customerShipToKeys = jsonkeys(customerShipToJson);
// Set business unit & insideSalesDL based on first menu option
defaultBusinessUnit = jsonpathgetsingle(customerShipToJson, "$." + customerShipToKeys[0], "string", "");
insideSalesDL = jsonpathgetsingle(customerBusinessUnitDetails, "$." + customerShipToKeys[0] + ".insideSalesDL", "string", "");

// 4. Get inside sales dl for each business unit
buDLsJson = json();
dlRecs = bmql("SELECT BUId, InsideSalesDL FROM BusinessUnit");
for rec in dlRecs {
	jsonput(buDLsJson, get(rec, "BUId"), get(rec, "InsideSalesDL"));
}

// Set business unit to 1901 if no BU defined for the selected ship to
if (defaultBusinessUnit == "" OR NOT isPartnerUser_t){
    if (businessUnit_t == "") {
        defaultBusinessUnit = "1151";    
    } else {
        defaultBusinessUnit = businessUnit_t;
    }	
}

// Set inside sales dl based on default bu if a DL is not defined for the company bu
if (insideSalesDL == ""){
	insideSalesDL = jsonget(buDLsJson, CONST_DEFAULT_BU, "string", "");	
}

if (action == "LINE_DEFAULT") {
    retStr = retStr + "1~customerShipToJson_t~" + jsontostr(customerShipToJson) + "|";
    retStr = retStr + "1~businessUnit_t~" + defaultBusinessUnit + "|";
    retStr = retStr + "1~currentSelectedCustomerShipToBU_t~" + defaultBusinessUnit + "|";
    retStr = retStr + "1~businessUnitInsideSalesDLJson_t~" + jsontostr(buDLsJson) + "|";
    retStr = retStr + "1~insideSalesDL_t~" + insideSalesDL  + "|";
}
if (action == "OPEN_TXN" AND isPartnerUser_t) {
    retStr = jsontostr(customerShipToJson);
}

return retStr;
```

## Revision History
 Rev. Date |Developer            |Notes / Comments
 ----------|---------------------|-------------------------------------------------------------------------
 2019-12-13|JSerna               |Initial Version
 2019-30-01|JSerna               |SP-98 Logic added to handle other company account id access
