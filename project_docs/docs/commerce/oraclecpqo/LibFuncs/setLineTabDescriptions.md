# Set Line Tab Descriptions



## Syntax
```
setLineTabDescriptions(params);
```

## Returns
Type|Description
-|-
```String```|

## Parameters
Name|Type|Description
-|-|-
```params```|```Json```|

## Attributes
Name|Description
-|-
```tabTypeTBD_t```|
```orderLineTabDescription_l```|
```outputSummaryTabDescription_l```|
```outputCutSheetTabDescription_l```|
```_line_item_comment```|
```_document_number```|
```_model_variable_name```|
```_parent_doc_number```|
```_part_number```|

## Functions
Type|Function Signature
-|-
```Json```|[getTabDescriptions(Json params)](getTabDescriptions)
```String```|[getLineType(String modelVarName, String parentDocNumber, String partNumber)](getLineType)

## Source
```Java
retStr = "";
tabTypeTBD = false;

// Loop through new lines being added
for line in transactionLine { 
    lineParentDocNumber = line._parent_doc_number;
    lineModelVarName = line._model_variable_name;
    linePartNumber = line._part_number;
    lineType = util.getLineType(lineModelVarName, lineParentDocNumber, linePartNumber); // CONFIG_ROOT, CONFIG_PART, CONFIG_MODEL, PART
    
    if (line._line_item_comment == "" OR lineType <> "CONFIG_PART") {
        continue;
    }
    
    configJson = json(line._line_item_comment);    
    tab = jsonget(configJson, "tab", "string", "");
    tabType = jsonget(configJson, "tabType", "string", "");
    tabOption = jsonget(configJson, "tabOption", "string", "");
  
    jsonput(params, "tabType", tabType);
    jsonput(params, "tabOption", tab);    
    tabDescJson = util.getTabDescriptions(params);

    orderLineTabDesc = jsonget(tabDescJson, "OrderLineTabDesc", "string", "");
    outputSummaryTabDesc = jsonget(tabDescJson, "OutputSummaryTabDesc", "string", "");
    outputCutSheetTabDesc = jsonget(tabDescJson, "OutputCutSheetTabDesc", "string", "");

    retStr = retStr + line._document_number + "~orderLineTabDescription_l~" + orderLineTabDesc + "|";
    retStr = retStr + line._document_number + "~outputSummaryTabDescription_l~" + outputSummaryTabDesc + "|";
    retStr = retStr + line._document_number + "~outputCutSheetTabDescription_l~" + outputCutSheetTabDesc + "|";
    if (tabType == "TBD") {
        tabTypeTBD = true;
    }
}
retStr = retStr + "1~tabTypeTBD_t~" + string(tabTypeTBD) + "|";

return retStr;
```

## Revision History
 Rev. Date |Developer            |Notes / Comments
 ----------|---------------------|-------------------------------------------------------------------------
 2020-03-03|jonathan.serna@piercewashington.com|
