# Order Lines HTML
returns tabular display of Order Lines as html string


## Syntax
```
orderLinesHTML();
```

## Returns
Type|Description
-|-
```String```|


## Attributes
Name|Description
-|-
```currency_t```|
```isPartnerUser_t```|
```integrationPartNumber_l```|
```requestedQuantity_l```|
```_part_custom_field15```|
```_document_number```|
```numRolls_l```|
```rollLength_l```|
```tab_l```|
```_part_desc```|
```rollPlacementId_l```|
```netPrice_l```|


## Source
```Java
ret = "";
DECIMAL_PLACES = 2;

retContent = "<div id=\"ord-parts\">";
itemHeadInternal = "<th width=\"15%\">Part Number</th><th width=\"25%\">Description</th><th width=\"10%\">Qty</th><th width=\"10%\">UOM</th><th width=\"10%\">Unit Price</th><th width=\"10%\">Num. Rolls</th><th width=\"10%\">Roll Length</th><th width=\"10%\">Tab</th>";
itemHeadExternal = "<th width=\"15%\">Part Number</th><th width=\"35%\">Description</th><th width=\"10%\">Qty</th><th width=\"10%\">UOM</th><th width=\"10%\">Num. Rolls</th><th width=\"10%\">Roll Length</th><th width=\"10%\">Tab</th>";
itemColumnHeaders = itemHeadInternal;
if (isPartnerUser_t){
	itemColumnHeaders = itemHeadExternal;
}

hasLines = false;
idx = 0;

totalQty = 0;
totalExtendedPrice = 0.00;
retContent = retContent + "<table id=\"ord-part\"><thead><tr>" + itemColumnHeaders + "</tr></thead><tbody>";
unitPrice = 0.00;
for line in transactionLine {	
	if(line.integrationPartNumber_l <> "" ){        
		hasLines = true;
		formattedQuantity = string(line.requestedQuantity_l);				
		if(idx % 2 == 0){
			retContent = retContent + "<tr class=\"even-row\">";
		}
		else {
			retContent = retContent + "<tr class=\"odd-row\">";
		}	
		
		// Set unit price depending on pricing UOM			
		if (line._part_custom_field11 == "MS") {		
			unitPrice = line.netPrice_l/1000;        
		} else {
			unitPrice = round(line.netPrice_l, 2);
		}
		
		// Set totals / formatted qty depending on whether line is credit line
		if (NOT line.isCreditLine_l) {					
        	totalExtendedPrice = totalExtendedPrice + (line.requestedQuantity_l * unitPrice);
        	totalQty = totalQty + line.requestedQuantity_l;
		} else {
			formattedQuantity = "-" + string(line.requestedQuantity_l);
		}	
        
		retContent = retContent	+ "<td>" + line.integrationPartNumber_l + "</td>"
						+ "<td>" + line._part_desc + "</td>"		
						+ "<td>" + formattedQuantity + "</td>"
						+ "<td>" + line._part_custom_field15 + "</td>";
		if (isPartnerUser_t <> true){
			retContent = retContent + "<td>" + formatascurrency(unitPrice, currency_t) + "</td>";
		}
		retContent = retContent + "<td>" + line.numRolls_l + "</td>"
								+ "<td>" + line.rollLength_l + "</td>"
								+ "<td>" + line.tab_l + "</td>"
								+ "</tr>";
		idx = idx + 1;
    }
}

tableFooter = "<tfoot><tr>"  
                +   "<td colspan='2'>TOTAL: </td>"
                +   "<td colspan='1'>" + string(totalQty) + "</td>"
                +   "<td colspan='1'></td>";                
if (isPartnerUser_t <> true){
	tableFooter = tableFooter + "<td colspan='1'>" + formatascurrency(totalExtendedPrice, currency_t) + "</td>";
}
tableFooter = tableFooter + "<td colspan='1'></td>"
                		  +  "<td colspan='1'></td>"
                		  +  "<td colspan='1'></td>"
                		  +  "</tr></tfoot>";

retContent = retContent + tableFooter + "</tbody></table></div>";

if(hasLines){
	ret = retContent;
}
return ret;
```

## Revision History
 Rev. Date |Developer            |Notes / Comments
 ----------|---------------------|-------------------------------------------------------------------------
 2019-11-13|Jserna               |Added table footer with totals
 2020-01-07|Jserna			   |Displaying negative qty for credit lines
