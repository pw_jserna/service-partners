# Format Attribute Values For PDF Output
Format Attribute Values as required for the quote output document. Used by the Generate Proposal. and Send Actions.


## Syntax
```
formatAttributeValuesForPDFOutput();
```

## Returns
Type|Description
-|-
```String```|


## Attributes
Name|Description
-|-
```_transaction_customer_t_company_name```|
```custContactName_t```|
```custContactPhone_t```|
```jobCity_t```|
```jobContactName_t```|
```jobContactPhone_t```|
```jobName_t```|
```jobStreet_t```|
```owner_t```|
```transactionName_t```|

## Functions
Type|Function Signature
-|-
```String```|[capFirstLetterOfWord(String inputString)](capFirstLetterOfWord)

## Source
```Java
/* Rlamsal - July 12, 2019 - Created */
ret = "";
quoteName = util.capFirstLetterOfWord(transactionName_t);
company_name = util.capFirstLetterOfWord(_transaction_customer_t_company_name);
jobName = util.capFirstLetterOfWord(jobName_t);
jobStreet = util.capFirstLetterOfWord(jobStreet_t);
jobCity = util.capFirstLetterOfWord(jobCity_t);
jobContactName = util.capFirstLetterOfWord(jobContactName_t);
jobContactPhone = util.capFirstLetterOfWord(jobContactPhone_t);
custContactName = util.capFirstLetterOfWord(custContactName_t);
custContactPhone = util.capFirstLetterOfWord(custContactPhone_t);
owner = util.capFirstLetterOfWord(owner_t);

// Get special notation title 
variables = string[]{"specialNotationTitle"};
envVarsDict = util.getEnvironmentVariableValues(_system_supplier_company_name, variables);
specialNotationTitle = get(envVarsDict,"specialNotationTitle");    

for line in transactionLine {
    lineDocNum = line._document_number;
    linePartNumber = line._part_number;
    lineDisplayNumber = line._part_display_number;
    lineParentDocNumber = line._parent_doc_number;
    lineModelVarName = line._model_variable_name;
    lineType = util.getLineType(lineModelVarName, lineParentDocNumber, linePartNumber); 

    if(lineType == "CONFIG_ROOT") { // building node
        // Set modelPlacementDiagram flag
        if (line.configPacementDiagram_l <> ""){
            ret = ret + lineDocNum + "~modelPlacementDiagram_l~true|";
        }
    }
}

ret = ret + "1~transactionName_t~" + quoteName + "|";
ret = ret + "1~_transaction_customer_t_company_name~" + company_name + "|";
ret = ret + "1~jobName_t~" + jobName + "|";
ret = ret + "1~jobStreet_t~" + jobStreet + "|";
ret = ret + "1~jobCity_t~" + jobCity + "|";
ret = ret + "1~jobContactName_t~" + jobContactName + "|";
ret = ret + "1~jobContactPhone_t~" + jobContactPhone + "|";
ret = ret + "1~custContactName_t~" + custContactName + "|";
ret = ret + "1~custContactPhone_t~" + custContactPhone + "|";
ret = ret + "1~owner_t~" + owner + "|";
ret = ret + "1~outputSpecialNotationTitle_t~" + specialNotationTitle + "|";

return ret;
```

## Revision History
Rev. Date |Developer            |Notes / Comments
----------|---------------------|-------------------------------------------------------------------------
2019-11-13|JSerna               |Setting modelPlacmentDiagram_l based on placement digram
