# Building Item Breakdown String Construct
Returns the code delimited string to form the Quote Summary table of the Quote Summary output Document


## Syntax
```
buildingItemBreakdownStringConstruct(inputDocNum);
```

## Returns
Type|Description
-|-
```String```|

## Parameters
Name|Type|Description
-|-|-
```inputDocNum```|```String```|

## Attributes
Name|Description
-|-
```currency_t```|
```includeDoorPricing_t```|
```_document_number```|
```_parent_doc_number```|
```_part_custom_field8```|
```_part_custom_field9```|
```_part_custom_field11```|
```_part_custom_field15```|
```listPrice_l```|
```netAmount_l```|
```netPrice_l```|
```requestedQuantity_l```|
```integrationPartNumber_l```|
```section_l```|
```tab_l```|
```buildingLabel_l```|
```_model_name```|
```_part_desc```|
```accessoryType_l```|
```facing_l```|
```insulationType_l```|
```_part_display_number```|
```isLinerPart_l```|
```linerDesc_l```|
```_model_variable_name```|
```tabType_l```|
```tabOption_l```|
```partType_l```|
```isCreditLine_l```|
```door_l```|
```_line_item_spare_rule_var_name```|
```_part_custom_field21```|
```lineNumber_l```|
```lineCustomSequenceNumber_l```|

## Functions
Type|Function Signature
-|-
```String```|[floatToFormattedCurrency(Json params)](floatToFormattedCurrency)
```String```|[getTabDescription(Json params)](getTabDescription)
```String```|[numberFormat(Float number, Integer places, String thousandSeparator, String decimalSeparator)](numberFormat)
```String[]```|[jsonStringArrayToArray(JsonArray jList)](jsonStringArrayToArray)

## Source
```Java
/**
    * @name Building Item Breakdown String Construct
    * @api buildingItemBreakdownStringConstruct
    * @summary Returns the code delimited string to form the Quote Summary table of the Quote Summary output Document
    * @param inputDocNum String param inputDocNum String
    * @attribute currency_t 
    * @attribute includeDoorPricing_t 
    * @attribute _document_number 
    * @attribute _parent_doc_number 
    * @attribute _part_custom_field8 
    * @attribute _part_custom_field9 
    * @attribute _part_custom_field11 
    * @attribute _part_custom_field15 
    * @attribute listPrice_l 
    * @attribute netAmount_l 
    * @attribute netPrice_l 
    * @attribute requestedQuantity_l 
    * @attribute integrationPartNumber_l 
    * @attribute section_l 
    * @attribute tab_l 
    * @attribute buildingLabel_l 
    * @attribute _model_name 
    * @attribute _part_desc 
    * @attribute accessoryType_l 
    * @attribute facing_l 
    * @attribute insulationType_l 
    * @attribute _part_display_number 
    * @attribute isLinerPart_l 
    * @attribute linerDesc_l 
    * @attribute _model_variable_name 
    * @attribute tabType_l 
    * @attribute tabOption_l 
    * @attribute partType_l 
    * @attribute isCreditLine_l 
    * @attribute door_l 
    * @attribute _line_item_spare_rule_var_name 
    * @attribute _part_custom_field21 
    * @function String floatToFormattedCurrency(Json params)
    * @function String getTabDescription(Json params)
    * @function String numberFormat(Float number, Integer places, String thousandSeparator, String decimalSeparator)
    * @function String[] jsonStringArrayToArray(JsonArray jList)
    * @return String 
    * @package 
    * @revision
    * Rev. Date |Developer            |Notes / Comments
    * ----------|---------------------|-------------------------------------------------------------------------
    * 2019-11-14|Jserna               |Updates per Phase II workshop
    * 2019-11-26|Jserna               |SP-46 - Tab type condition not evaluating to true
    * 2019-11-26|Jserna               |SP-46 Adding Tab type logic for Walls per Greg's feedback
    * 2019-11-26|Jserna               |Extracted tab desc logic to util func. Added logic to handle partition walls
    * 2019-12-17|Jserna               |Partition wall handling
    * 2020-01-09|Jserna               |SP-76 - Change to partition wall handling to address additional walls
    * 2020-02-28|Jserna               |Liner qty issue change
    * 2020-03-03|Jserna               |SP-129 - Display correct description for roof liners with same top/bottom rval
    * 2020-03-12|shansen              |Added partition wall totals per SP-135
    * 2020-03-16|shansen              |Changed to ignore freight and Freight surcharge
    * 2020-05-05|shansen              |Splitting out misc into misc and credit
    * 2020-05-21|shansen              |No longer blanking out insulationTypeStr when no rolls are present, as N/A could've been selected for one/both layers
    * 2020-05-21|shansen              |Changed to always append to the wall insulation desc, rolls/layers can be duplicates but should still be presented
    * 2020-06-12|shansen              |Including new door logic
    * 2021-01-13|jonathan.serna@piercewashington.com|
    */
    retStr = "";

    FORMATTED_QTY_PLACES = 0;
    FORMATTED_QTY_THOU_SEP = ",";
    CURRENCY_SYMBOL = "$";

    rowDelim = "#@#";
    colDelim = "#$#";
    builidingDelim = "@!@";

    miscQuantityTotalDict = dict("integer");
    miscQuantityUOMDict = dict("string");
    miscAmtTotalDict = dict("float");
    miscListTotalDict = dict("float");
    miscDescriptionDict = dict("string");

    creditQuantityTotalDict = dict("integer");
    creditQuantityUOMDict = dict("string");
    creditAmtTotalDict = dict("float");
    creditListTotalDict = dict("float");
    creditDescriptionDict = dict("string");

    // Set up inputs for float formatting func
    floatFormatParams = json();
    jsonput(floatFormatParams, "trimLeadingZero", true);
    jsonput(floatFormatParams, "currencyCode", currency_t);

    // Store building line number for sorting
    buildingLineNumbers = float[];
    miscLineNumbers = float[];
    creditLineNumbers = float[];
    creditPartNumbers = json();
    retJson = json("{\"buildings\": {}, \"misc\":{}, \"credit\": {}}");        
    miscPartsArr = jsonarray();

    for buildingLine in transactionLine {
        freightLine = (buildingLine._part_display_number=="F" OR buildingLine._part_display_number=="FS");
        // Loop only for Buildings and Miscellaneous Parts
        //if((buildingLine._parent_doc_number == "" OR isnull(buildingLine._parent_doc_number)) AND upper(buildingLine._part_desc) <> "FREIGHT" AND (buildingLine.integrationPartNumber_l=="" OR isnull(buildingLine.integrationPartNumber_l)) AND buildingLine._model_variable_name=="building") {
        lineSequenceNumber =  buildingLine.lineCustomSequenceNumber_l;
        print "LINE SEQUENCE NUMBER: " + string(lineSequenceNumber);
        if((buildingLine._parent_doc_number == "" OR isnull(buildingLine._parent_doc_number)) AND NOT freightLine AND buildingLine._model_variable_name=="building") {
            parentDocNum = buildingLine._document_number;
            buildingLabel = buildingLine.buildingLabel_l;
            buildingTotalPrice = 0.0;
            buildingTotalListPrice = 0.0;
            buildingTotalQty = 0;        
            
            roofBottomQuantityTotalDict = dict("integer");
            roofBottomQuantityUOMDict = dict("string");
            roofBottomAmtTotalDict = dict("float");
            roofBottomListTotalDict = dict("float");
            roofBottomInsulationTypeDict = dict("string");
            roofBottomFacingDict = dict("string");
            roofBottomTabTypeDict = dict("string"); // new sept 23 2019
            roofBottomTabOptionDict = dict("string"); // new sept 23 2019
            
            roofTopQuantityTotalDict = dict("integer");
            roofTopQuantityUOMDict = dict("string");
            roofTopAmtTotalDict = dict("float");
            roofTopListTotalDict = dict("float");
            roofTopInsulationTypeDict = dict("string");
            roofTopFacingDict = dict("string");
            
            linerRoofInsulationTypeStr = "";
            linerRoofQuantityTotal = 0;
            linerRoofNAQuantityTotal = 0; //in case N/A is selected as the liner, in which case the above won't receive a quantity
            linerRoofPriceTotal = 0.0;
            linerRoofListPriceTotal = 0.0;
            linerRoofDescArr = string[];
            linerRoofTopDescRVal = "";
            linerRoofBottomDescRVal = "";
            linerRoofNA = false;
            
            wallQuantityTotalDict = dict("integer");
            wallQuantityUOMDict = dict("string");
            wallAmtTotalDict = dict("float");
            wallListTotalDict = dict("float");
            wallInsulationTypeDict = dict("string");
            wallFacingDict = dict("string");
            wallTabTypeDict = dict("string"); // new nov 26 2019
            wallTabOptionDict = dict("string"); // new nov 26 2019

            // Initilise partition wall dicts
            partitionWallQuantityTotalDict = dict("integer");
            partitionWallQuantityUOMDict = dict("string");
            partitionWallAmtTotalDict = dict("float");
            partitionWallListTotalDict = dict("float");
            partitionWallInsulationTypeDict = dict("string");
            partitionWallFacingDict = dict("string");
            partitionWallTabTypeDict = dict("string"); 
            partitionWallTabOptionDict = dict("string");
            
            // Partition wall liners
            linerPartitionWallInsulationTypeStr = "";
            linerPartitionWallQuantityTotal = 0;
            linerPartitionWallNAQuantityTotal = 0;
            linerPartitionWallPriceTotal = 0.0;
            linerPartitionWallListPriceTotal = 0.0;
            linerPartitionWallDescArr = string[];
            linerPartitionWallNA = false;

            linerWallInsulationTypeStr = ""; 
            linerWallQuantityTotal = 0;
            linerWallNAQuantityTotal = 0;
            linerWallPriceTotal = 0.0;
            linerWallListPriceTotal = 0.0;
            linerWallDescArr = string[];
            linerWallNA = false;
            
            //Doors object, key = parent doc, value = json
            doorsJObj = json();
            doorPriceTotal = 0.0;

            accessories = "";
            accessoriesQuantityTotalDict = dict("integer");
            accessoriesUOMDict = dict("string");
            accessoriesPriceTotal = 0.0;
            
            linerOptionalAccessories = "Additional Accessories: ";
            linerMandatoryAccessories = "Accessories included in system price:  Banding, Fasteners, Tape, and Adhesive";
            
            linerRoofAccessoryStr = "";
            linerRoofAccessoriesQuantityTotalDict = dict("integer");
            linerRoofAccessoriesUOMDict = dict("string");
            linerRoofAccessoriesPriceTotal = 0.0;
            
            linerWallAccessoryStr = "";
            linerWallAccessoriesQuantityTotalDict = dict("integer");
            linerWallAccessoriesUOMDict = dict("string");
            linerWallAccessoriesPriceTotal = 0.0;

            // Partition wall liner accessories
            linerPartitionWallAccessoryStr = "";
            linerPartitionWallAccessoriesQuantityTotalDict = dict("integer");
            linerPartitionWallAccessoriesUOMDict = dict("string");
            linerPartitionWallAccessoriesPriceTotal = 0.0;
            
            combinedWallsBool = false;
            
            for childLine in transactionLine {
                // Only for child items of the Building            
                if(childLine._parent_doc_number == parentDocNum) {

                    listPrice = childLine.listPrice_l;
                    extListPrice = listPrice * childLine.requestedQuantity_l;
                    if (childLine._part_custom_field11 == "MS") {
                        extListPrice = extListPrice / 1000;
                    }
                    // For the Roof, Walls, Doors, and Accessories
                    if(childLine.integrationPartNumber_l<>"" AND NOT(isnull(childLine.integrationPartNumber_l))) { 
                        

                        childTypeKey = "";
                        if(childLine._part_custom_field8 <> "") {                        
                            // If it is roll, then Thickness + R-Value + Tab
                            childTypeKey = childLine._part_custom_field9 + "_" + childLine._part_custom_field8 + "_" + childLine.tab_l;
                        }
                        // } else {
                            // // If it is liner, then Liner Description.
                            // childTypeKey = childLine.linerDesc_l;
                        // }
                        
                        if(NOT childLine.isLinerPart_l AND NOT childLine.door_l) {
                            if(startswith(upper(childLine.section_l), "ROOF")) {
                                if(childLine.section_l == "Roof-Top Layer") {
                                    // For Section = Roof-Top Layer
                                    // Quantity:
                                    quantity = 0;
                                    if(containskey(roofTopQuantityTotalDict, childTypeKey)) {
                                        quantity = get(roofTopQuantityTotalDict, childTypeKey);
                                    }
                                    quantity = quantity + childLine.requestedQuantity_l;
                                    put(roofTopQuantityTotalDict, childTypeKey, quantity);
                                    
                                    // Quantity UOM:
                                    if(NOT(containskey(roofTopQuantityUOMDict, childTypeKey))) {
                                        put(roofTopQuantityUOMDict, childTypeKey, childLine._part_custom_field15);
                                    }
                                    
                                    // Total Net Amount:
                                    amount = 0.0;
                                    if(containskey(roofTopAmtTotalDict, childTypeKey)) {
                                        amount = get(roofTopAmtTotalDict, childTypeKey);
                                    }
                                    amount = amount + childLine.netAmount_l;
                                    put(roofTopAmtTotalDict, childTypeKey, amount);

                                    // Total List Amount:
                                    amount = 0.0;
                                    if(containskey(roofTopListTotalDict, childTypeKey)) {
                                        amount = get(roofTopListTotalDict, childTypeKey);
                                    }
                                    amount = amount + extListPrice;
                                    put(roofTopListTotalDict, childTypeKey, amount);
                                    
                                    // Insulation Type
                                    if(NOT(containskey(roofTopInsulationTypeDict, childTypeKey))) {
                                        put(roofTopInsulationTypeDict, childTypeKey, childLine.insulationType_l);
                                    }
                                    
                                    // Facing
                                    if(NOT(containskey(roofTopFacingDict, childTypeKey))) {
                                        put(roofTopFacingDict, childTypeKey, childLine.facing_l);
                                    }
                                    
                                } //elif(childLine.section_l == "Roof-Bottom Layer" OR childLine.section_l == "Roof") { 
                                else {
                                    //For Section = Roof-Bottom Layer OR Roof OR Roof Liner
                                    // Quantity:
                                    quantity = 0;
                                    if(containskey(roofBottomQuantityTotalDict, childTypeKey)) {
                                        quantity = get(roofBottomQuantityTotalDict, childTypeKey);
                                    }
                                    quantity = quantity + childLine.requestedQuantity_l;
                                    put(roofBottomQuantityTotalDict, childTypeKey, quantity);
                                    
                                    // Quantity UOM:
                                    if(NOT(containskey(roofBottomQuantityUOMDict, childTypeKey))) {
                                        put(roofBottomQuantityUOMDict, childTypeKey, childLine._part_custom_field15);
                                    }
                                    
                                    // Total Net Amount:
                                    amount = 0.0;
                                    if(containskey(roofBottomAmtTotalDict, childTypeKey)) {
                                        amount = get(roofBottomAmtTotalDict, childTypeKey);
                                    }
                                    amount = amount + childLine.netAmount_l;
                                    put(roofBottomAmtTotalDict, childTypeKey, amount);

                                    // Total List Amount:
                                    amount = 0.0;
                                    if(containskey(roofBottomListTotalDict, childTypeKey)) {
                                        amount = get(roofBottomListTotalDict, childTypeKey);
                                    }
                                    amount = amount + extListPrice;
                                    put(roofBottomListTotalDict, childTypeKey, amount);
                                    
                                    // Insulation Type
                                    if(NOT(containskey(roofBottomInsulationTypeDict, childTypeKey))) {
                                        put(roofBottomInsulationTypeDict, childTypeKey, childLine.insulationType_l);
                                    }
                                    
                                    // Facing
                                    if(NOT(containskey(roofBottomFacingDict, childTypeKey))) {
                                        put(roofBottomFacingDict, childTypeKey, childLine.facing_l);
                                    }

                                    // new sept 23 2019
                                    // Tab Type
                                    if(NOT(containskey(roofBottomTabTypeDict, childTypeKey))) {
                                        put(roofBottomTabTypeDict, childTypeKey, childLine.tabType_l);
                                    }

                                    // Tab Option
                                    if(NOT(containskey(roofBottomTabOptionDict, childTypeKey))) {
                                        put(roofBottomTabOptionDict, childTypeKey, childLine.tabOption_l);
                                    }

                                }
                            } elif(startswith(upper(childLine.section_l), "ACCESSORYPART")) { 
                                //For Section=Accessories.
                                dashPos = find(childLine.section_l, "-");
                                accessoryKey = substring(childLine.section_l, dashPos+1);
                                if(startswith(upper(accessoryKey), "DOUBLE FACE TAPE") OR startswith(upper(accessoryKey), "DOUBLE FACED TAPE")) {
                                    accessoryKey = "Double Faced Tape";
                                }
                                // Quantity:
                                quantity = 0;
                                if(containskey(accessoriesQuantityTotalDict, accessoryKey)) {
                                    quantity = get(accessoriesQuantityTotalDict, accessoryKey);
                                }
                                quantity = quantity + childLine.requestedQuantity_l;
                                put(accessoriesQuantityTotalDict, accessoryKey, quantity);
                                
                                // Quantity UOM:
                                if(NOT(containskey(accessoriesUOMDict, accessoryKey))) {
                                    uom = "";
                                    if(childLine._part_custom_field15 <> "" AND NOT(isnull(childLine._part_custom_field15))) {
                                        uom = childLine._part_custom_field15;
                                    }
                                    put(accessoriesUOMDict, accessoryKey, uom);
                                }
                                
                                accessoriesPriceTotal = accessoriesPriceTotal + childLine.netAmount_l;
                            } elif(startswith(upper(childLine.section_l), "SIDEWALL") OR startswith(upper(childLine.section_l), "ENDWALL") OR startswith(upper(childLine.section_l), "COMBINEDWALLS")) {
                                // For Section = S/W1, S/W2, E/W1, E/W2, CombinedWalls.
                                
                                if(startswith(upper(childLine.section_l), "COMBINEDWALLS")) {
                                    combinedWallsBool = true;
                                }
                                
                                // Quantity:
                                quantity = 0;
                                if(containskey(wallQuantityTotalDict, childTypeKey)) {
                                    quantity = get(wallQuantityTotalDict, childTypeKey);
                                }
                                quantity = quantity + childLine.requestedQuantity_l;
                                put(wallQuantityTotalDict, childTypeKey, quantity);
                                
                                // Quantity UOM:
                                if(NOT(containskey(wallQuantityUOMDict, childTypeKey))) {
                                    put(wallQuantityUOMDict, childTypeKey, childLine._part_custom_field15);
                                }
                                
                                // Total Net Amount
                                amount = 0.0;
                                if(containskey(wallAmtTotalDict, childTypeKey)) {
                                    amount = get(wallAmtTotalDict, childTypeKey);
                                }
                                amount = amount + childLine.netAmount_l;
                                put(wallAmtTotalDict, childTypeKey, amount);

                                // Total List Amount
                                amount = 0.0;
                                if(containskey(wallListTotalDict, childTypeKey)) {
                                    amount = get(wallListTotalDict, childTypeKey);
                                }
                                amount = amount + extListPrice;
                                put(wallListTotalDict, childTypeKey, amount);
                                
                                // Insulation Type
                                if(NOT(containskey(wallInsulationTypeDict, childTypeKey))) {
                                    put(wallInsulationTypeDict, childTypeKey, childLine.insulationType_l);
                                }
                                
                                // Facing
                                if(NOT(containskey(wallFacingDict, childTypeKey))) {
                                    put(wallFacingDict, childTypeKey, childLine.facing_l);
                                }

                                // new nov 26 2019
                                // Tab Type
                                if(NOT(containskey(wallTabTypeDict, childTypeKey))) {
                                    put(wallTabTypeDict, childTypeKey, childLine.tabType_l);
                                }

                                // Tab Option
                                if(NOT(containskey(wallTabOptionDict, childTypeKey))) {
                                    put(wallTabOptionDict, childTypeKey, childLine.tabOption_l);
                                }
                            } elif(startswith(upper(childLine.section_l), "PARTITIONWALL") OR startswith(upper(childLine.section_l), "COMBINEDPARTITIONWALLS")) {
                                    //For Section = Partition Wall
                                    // Quantity:
                                    quantity = 0;
                                    if(containskey(partitionWallQuantityTotalDict, childTypeKey)) {
                                        quantity = get(partitionWallQuantityTotalDict, childTypeKey);
                                    }
                                    quantity = quantity + childLine.requestedQuantity_l;
                                    put(partitionWallQuantityTotalDict, childTypeKey, quantity);
                                    
                                    // Quantity UOM:
                                    if(NOT(containskey(partitionWallQuantityUOMDict, childTypeKey))) {
                                        put(partitionWallQuantityUOMDict, childTypeKey, childLine._part_custom_field15);
                                    }
                                    
                                    // Total Net Amount:
                                    amount = 0.0;
                                    if(containskey(partitionWallAmtTotalDict, childTypeKey)) {
                                        amount = get(partitionWallAmtTotalDict, childTypeKey);
                                    }
                                    amount = amount + childLine.netAmount_l;
                                    put(partitionWallAmtTotalDict, childTypeKey, amount);

                                    // Total List Amount:
                                    amount = 0.0;
                                    if(containskey(partitionWallListTotalDict, childTypeKey)) {
                                        amount = get(partitionWallListTotalDict, childTypeKey);
                                    }
                                    amount = amount + extListPrice;
                                    put(partitionWallListTotalDict, childTypeKey, amount);
                                    
                                    // Insulation Type
                                    if(NOT(containskey(partitionWallInsulationTypeDict, childTypeKey))) {
                                        put(partitionWallInsulationTypeDict, childTypeKey, childLine.insulationType_l);
                                    }
                                    
                                    // Facing
                                    if(NOT(containskey(partitionWallFacingDict, childTypeKey))) {
                                        put(partitionWallFacingDict, childTypeKey, childLine.facing_l);
                                    }

                                    // new sept 23 2019
                                    // Tab Type
                                    if(NOT(containskey(partitionWallTabTypeDict, childTypeKey))) {
                                        put(partitionWallTabTypeDict, childTypeKey, childLine.tabType_l);
                                    }

                                    // Tab Option
                                    if(NOT(containskey(partitionWallTabOptionDict, childTypeKey))) {
                                        put(partitionWallTabOptionDict, childTypeKey, childLine.tabOption_l);
                                    }
                            }
                        } elif(childLine.isLinerPart_l) {
                            // Liner Systems                        
                            if(startswith(upper(childLine.section_l), "ROOF")) {                                                    
                                if(linerRoofInsulationTypeStr == "") {
                                    linerRoofInsulationTypeStr = childLine.linerDesc_l;
                                }                            
                                if(startswith(upper(childLine.section_l), "ROOF-ACCESSORIES")) {
                                    //For Section=ROOF-ACCESSORIES.
                                    section = "ROOF-ACCESSORIES-";
                                    accessoryKey = substring(childLine.section_l, len(section));
                                    
                                    // Quantity:
                                    quantity = 0;
                                    if(containskey(linerRoofAccessoriesQuantityTotalDict, accessoryKey)) {
                                        quantity = get(linerRoofAccessoriesQuantityTotalDict, accessoryKey);
                                    }
                                    quantity = quantity + childLine.requestedQuantity_l;
                                    put(linerRoofAccessoriesQuantityTotalDict, accessoryKey, quantity);
                                    
                                    // Quantity UOM:
                                    if(NOT(containskey(linerRoofAccessoriesUOMDict, accessoryKey))) {
                                        uom = "";
                                        if(childLine._part_custom_field15 <> "" AND NOT(isnull(childLine._part_custom_field15))) {
                                            uom = childLine._part_custom_field15;
                                        }
                                        put(linerRoofAccessoriesUOMDict, accessoryKey, uom);
                                    }
                                    
                                    linerRoofAccessoriesPriceTotal = linerRoofAccessoriesPriceTotal + childLine.netAmount_l;
                                } else {
                                    // For Section = Roof-Top Layer or Roof-Bottom Layer
                                    // Quantity:
                                    // linerRoofQuantityTotal = linerRoofQuantityTotal + childLine.requestedQuantity_l;
                                    if(linerRoofQuantityTotal == 0 AND childLine.partType_l == "Liner") {
                                        linerRoofQuantityTotal = childLine.requestedQuantity_l;
                                    }
                                    elif(childLine.partType_l <> "Liner"){
                                        linerRoofNAQuantityTotal = childLine.requestedQuantity_l + linerRoofNAQuantityTotal;
                                    }
                                    
                                    // Total Net Amount:
                                    linerRoofPriceTotal = linerRoofPriceTotal + childLine.netAmount_l;

                                    // Total List Amount:
                                    linerRoofListPriceTotal = linerRoofListPriceTotal + extListPrice;
                                    
                                    // Insulation Type String
                                    if(childLine._part_custom_field9 <> "" AND childLine._part_custom_field8 <> "") {
                                        descStr = childLine._part_custom_field9 + "\" " + "(" + childLine._part_custom_field8 + ")";
                                        if (startswith(upper(childLine.section_l), "ROOF-TOP") AND linerRoofTopDescRVal == "") {
                                            linerRoofTopDescRVal = descStr;
                                            append(linerRoofDescArr, descStr);
                                        }                                       
                                        if (startswith(upper(childLine.section_l), "ROOF-BOTTOM") AND linerRoofBottomDescRVal == "") {
                                            linerRoofBottomDescRVal = descStr;
                                            append(linerRoofDescArr, descStr);
                                        }
                                    }
                                    //linerRoofInsulationTypeStr = linerRoofInsulationTypeStr + childLine.rollWidth_l + "\" " + "(" + childLine._part_custom_field8 + ")" + " + ";
                                }
                            } elif(startswith(upper(childLine.section_l), "SIDEWALL") OR startswith(upper(childLine.section_l), "ENDWALL") OR startswith(upper(childLine.section_l), "COMBINEDWALLS") OR startswith(upper(childLine.section_l), "WALLS")) {
                                // For Section = S/W1, S/W2, E/W1, E/W2, CombinedWalls or any of its liner.
                                if(linerWallInsulationTypeStr == "") {
                                    linerWallInsulationTypeStr = childLine.linerDesc_l;
                                }
                                if(startswith(upper(childLine.section_l), "WALLS-ACCESSORIES")) { 
                                    //For Section=WALLS-ACCESSORIES.
                                    section = "WALLS-ACCESSORIES-";
                                    accessoryKey = substring(childLine.section_l, len(section));
                                    
                                    // Quantity:
                                    quantity = 0;
                                    if(containskey(linerWallAccessoriesQuantityTotalDict, accessoryKey)) {
                                        quantity = get(linerWallAccessoriesQuantityTotalDict, accessoryKey);
                                    }
                                    quantity = quantity + childLine.requestedQuantity_l;
                                    put(linerWallAccessoriesQuantityTotalDict, accessoryKey, quantity);
                                    
                                    // Quantity UOM:
                                    if(NOT(containskey(linerWallAccessoriesUOMDict, accessoryKey))) {
                                        uom = "";
                                        if(childLine._part_custom_field15 <> "" AND NOT(isnull(childLine._part_custom_field15))) {
                                            uom = childLine._part_custom_field15;
                                        }
                                        put(linerWallAccessoriesUOMDict, accessoryKey, uom);
                                    }
                                    
                                    linerWallAccessoriesPriceTotal = linerWallAccessoriesPriceTotal + childLine.netAmount_l;
                                } else {
                                    // For Section = Walls / Side Walls / End Walls / Combined Walls
                                    // Quantity:
                                    // linerWallQuantityTotal = linerWallQuantityTotal + childLine.requestedQuantity_l;
                                    if(linerWallQuantityTotal == 0 AND childLine.partType_l == "Liner") {
                                        linerWallQuantityTotal = childLine.requestedQuantity_l;
                                    }
                                    elif(childLine.partType_l <> "Liner"){
                                        linerWallNAQuantityTotal = childLine.requestedQuantity_l + linerWallNAQuantityTotal;
                                    }
                                    
                                    // Total Net Amount:
                                    linerWallPriceTotal = linerWallPriceTotal + childLine.netAmount_l;

                                    // Total List Amount:
                                    linerWallListPriceTotal = linerWallListPriceTotal + extListPrice;
                                    
                                    // Insulation Type String
                                    if(childLine._part_custom_field9 <> "" AND childLine._part_custom_field8 <> "") {
                                        descStr = childLine._part_custom_field9 + "\" " + "(" + childLine._part_custom_field8 + ")";
                                        //if(findinarray(linerWallDescArr,descStr) == -1) {
                                            append(linerWallDescArr, descStr);
                                        //}
                                    }
                                    //linerWallInsulationTypeStr = linerWallInsulationTypeStr + childLine.rollWidth_l + "\" " + "(" + childLine._part_custom_field8 + ")" + " + ";                             
                                }
                            } elif (startswith(upper(childLine.section_l), "PARTITIONWALL") OR startswith(upper(childLine.section_l), "COMBINEDPARTITIONWALLS") OR startswith(upper(childLine.section_l), "PARTITION") ) {                           
                                if(linerPartitionWallInsulationTypeStr == "") {                                
                                    linerPartitionWallInsulationTypeStr = childLine.linerDesc_l;
                                }
                                if(startswith(upper(childLine.section_l), "PARTITION-WALLS-ACCESSORIES")) { 
                                    //For Section=WALLS-ACCESSORIES.
                                    section = "PARTITION-WALLS-ACCESSORIES-";
                                    accessoryKey = substring(childLine.section_l, len(section));
                                    
                                    // Quantity:
                                    quantity = 0;
                                    if(containskey(linerPartitionWallAccessoriesQuantityTotalDict, accessoryKey)) {
                                        quantity = get(linerPartitionWallAccessoriesQuantityTotalDict, accessoryKey);
                                    }
                                    quantity = quantity + childLine.requestedQuantity_l;
                                    put(linerPartitionWallAccessoriesQuantityTotalDict, accessoryKey, quantity);
                                    
                                    // Quantity UOM:
                                    if(NOT(containskey(linerPartitionWallAccessoriesUOMDict, accessoryKey))) {
                                        uom = "";
                                        if(childLine._part_custom_field15 <> "" AND NOT(isnull(childLine._part_custom_field15))) {
                                            uom = childLine._part_custom_field15;
                                        }
                                        put(linerPartitionWallAccessoriesUOMDict, accessoryKey, uom);
                                    }
                                    
                                    linerPartitionWallAccessoriesPriceTotal = linerPartitionWallAccessoriesPriceTotal + childLine.netAmount_l;
                                } else {
                                    //linerPartitionWallQuantityTotal = linerPartitionWallQuantityTotal + childLine.requestedQuantity_l;
                                    if(linerPartitionWallQuantityTotal == 0 AND childLine.partType_l == "Liner") {
                                        linerPartitionWallQuantityTotal = childLine.requestedQuantity_l;
                                    }
                                    elif(childLine.partType_l <> "Liner"){
                                        linerPartitionWallNAQuantityTotal = childLine.requestedQuantity_l + linerPartitionWallNAQuantityTotal;
                                    }                            
                                    // Total Net Amount:
                                    linerPartitionWallPriceTotal = linerPartitionWallPriceTotal + childLine.netAmount_l;

                                    // Total List Amount:
                                    linerPartitionWallListPriceTotal = linerPartitionWallListPriceTotal + extListPrice;
                                    
                                    // Insulation Type String                        
                                    if(childLine._part_custom_field9 <> "" AND childLine._part_custom_field8 <> "") {
                                        descStr = childLine._part_custom_field9 + "\" " + "(" + childLine._part_custom_field8 + ")";
                                        //if(findinarray(linerPartitionWallDescArr,descStr) == -1) {
                                            append(linerPartitionWallDescArr, descStr);
                                        //}
                                    }
                                }    
                            }
                        }
                        elif(childLine.door_l){
                            //doors
                            decIdx = find(childLine._line_item_spare_rule_var_name, ".");
                            doorKey = substring(childLine._line_item_spare_rule_var_name, 0 , decIdx);
                            doorSubKey = substring(childLine._line_item_spare_rule_var_name, decIdx+1);
                            doorJObj = jsonget(doorsJObj, doorKey, "json", json());
                            doorIncludes = jsonget(doorJObj, "includes", "json", json());

                            componentDesc= childLine._part_custom_field21;
                            if(componentDesc==""){
                                componentDesc= childLine._part_desc;
                            }
                            componentDesc= "(" + string(childLine.requestedQuantity_l) + ") " + componentDesc;
                            //componentID = 
                            jsonput(doorIncludes, doorSubKey, componentDesc);
                            doorListTotal = jsonget(doorJObj, "list", "float", 0.0);
                            doorNetTotal = jsonget(doorJObj, "net", "float", 0.0);

                            doorListTotal = doorListTotal + extListPrice;
                            doorNetTotal = doorNetTotal + childLine.netAmount_l;
                            doorPriceTotal = doorPriceTotal + childLine.netAmount_l;
                            jsonput(doorJObj, "includes", doorIncludes);
                            jsonput(doorJObj, "list", doorListTotal);
                            jsonput(doorJObj, "net", doorNetTotal);
                            jsonput(doorsJObj, doorKey, doorJObj);
                        }
                    }
                    elif(childLine._model_name=="Door"){
                        doorKey = childLine._line_item_spare_rule_var_name;
                        doorJObj = jsonget(doorsJObj, doorKey, "json", json());
                        //get door description and quantity
                        //jsonput(doorJObj, "quantity", childLine.requestedQuantity_l);
                        jsonput(doorJObj, "desc", childLine._part_desc);
                        jsonput(doorsJObj, doorKey, doorJObj);
                    }
                } 
            }

            // For Roof Liners
            if(sizeofarray(linerRoofDescArr) > 0) {
                linerRoofInsulationTypeStr = linerRoofInsulationTypeStr + " " + join(linerRoofDescArr, " + ");
            } /*else {
                // No Roof Liners. - don't blank this out, there could be 0 rolls but still have a valid roof
                linerRoofInsulationTypeStr = "";
            }*/
            
            // For Wall Liners
            if(sizeofarray(linerWallDescArr) > 0) {
                linerWallInsulationTypeStr = linerWallInsulationTypeStr + " " + join(linerWallDescArr, " + ");
            } /*else {
                // No Wall Liners.
                linerWallInsulationTypeStr = "";
            }*/

            //For Partition Wall Liners
            if(sizeofarray(linerPartitionWallDescArr) > 0) {
                linerPartitionWallInsulationTypeStr = linerPartitionWallInsulationTypeStr + " " + join(linerPartitionWallDescArr, " + ");
            } /*else {
                // No Wall Liners.
                linerPartitionWallInsulationTypeStr = "";
            }*/
                    
            // Totalling the amounts and quantities to form the Building Totals
            // Roof bottom amt total
            roofBottomAmts = keys(roofBottomAmtTotalDict);        
            for each1 in roofBottomAmts {
                buildingTotalPrice = buildingTotalPrice + get(roofBottomAmtTotalDict, each1);            
            }

            // Roof bottom qty total
            roofBottomQties = keys(roofBottomQuantityTotalDict);        
            for each1 in roofBottomQties {
                buildingTotalQty = buildingTotalQty + get(roofBottomQuantityTotalDict, each1);
            }
            
            // Roof top amt total
            roofTopAmts = keys(roofTopAmtTotalDict);
            for each2 in roofTopAmts {
                buildingTotalPrice = buildingTotalPrice + get(roofTopAmtTotalDict, each2);            
            }

            // Roof top qty total
            roofTopQties = keys(roofTopQuantityTotalDict);
            for each2 in roofTopQties {
                buildingTotalQty = buildingTotalQty + get(roofTopQuantityTotalDict, each2);
            }
            
            // Wall amt total
            wallAmts = keys(wallAmtTotalDict);
            for each3 in wallAmts {
                buildingTotalPrice = buildingTotalPrice + get(wallAmtTotalDict, each3);            
            }

            // Wall qty total
            wallQties = keys(wallQuantityTotalDict);
            for each3 in wallQties {
                buildingTotalQty = buildingTotalQty + get(wallQuantityTotalDict, each3);
            }

            // Partition Wall amt total
            partitionWallAmts = keys(partitionWallAmtTotalDict);
            for each3 in partitionWallAmts {
                buildingTotalPrice = buildingTotalPrice + get(partitionWallAmtTotalDict, each3);            
            }

            // Partition wall qty total
            partitionWallQties = keys(partitionWallQuantityTotalDict);
            for each3 in partitionWallQties {
                buildingTotalQty = buildingTotalQty + get(partitionWallQuantityTotalDict, each3);
            }

            //liner quantity fix and N/A setting
            if(linerRoofQuantityTotal==0){
                linerRoofNA = true;
                linerRoofQuantityTotal = linerRoofNAQuantityTotal;
            }

            if(linerWallQuantityTotal==0){
                linerWallNA = true;
                linerWallQuantityTotal = linerWallNAQuantityTotal;
            }

            if(linerPartitionWallQuantityTotal==0){
                linerPartitionWallNA = true;
                linerPartitionWallQuantityTotal= linerPartitionWallNAQuantityTotal;
            }

            //doors
            if(includeDoorPricing_t){
                buildingTotalPrice = buildingTotalPrice + doorPriceTotal;
            }

            buildingTotalPrice = buildingTotalPrice + accessoriesPriceTotal;
            
            buildingTotalPrice = buildingTotalPrice + linerRoofPriceTotal + linerWallPriceTotal + linerRoofAccessoriesPriceTotal + linerWallAccessoriesPriceTotal + linerPartitionWallPriceTotal + linerPartitionWallAccessoriesPriceTotal;
            buildingTotalQty   = buildingTotalQty +  linerRoofQuantityTotal + linerWallQuantityTotal;
            
            /*print "buildingDesc: " + buildingDesc; print "";
            print "roofQuantityTotalDict: "; print roofQuantityTotalDict; print "";
            print "roofQuantityUOMDict: "; print roofQuantityUOMDict; print "";
            print "roofUnitPriceDict: "; print roofUnitPriceDict; print "";
            print "roofUnitPriceUOMDict: "; print roofUnitPriceUOMDict; print "";
            print "roofAmtTotalDict: "; print roofAmtTotalDict; print "";
            print "wallQuantityTotalDict: "; print wallQuantityTotalDict; print "";
            print "wallQuantityUOMDict: "; print wallQuantityUOMDict; print "";
            print "wallUnitPriceDict: "; print wallUnitPriceDict; print "";
            print "wallUnitPriceUOMDict: "; print wallUnitPriceUOMDict; print "";
            print "wallAmtTotalDict: "; print wallAmtTotalDict; print "";
            print "accessories: " + accessories; print "";
            print "buildingTotalPrice: " + string(buildingTotalPrice); print "";*/
            
            // Forming the return string for each building       
            retStr = retStr + upper(buildingLabel) + rowDelim;
            roofBottomKeys = keys(roofBottomQuantityTotalDict);
            roofTopKeys = keys(roofTopQuantityTotalDict);
            for eachBottomRoof in roofBottomKeys {
                splitRoofDet = split(eachBottomRoof, "_");
                splitRoofDetArrSize = sizeofarray(splitRoofDet);
                tempStr = "ROOF BOTTOM";
                if((sizeofarray(roofTopKeys) == 0) OR (splitRoofDetArrSize == 1)) {
                    tempStr = "ROOF";
                }
                retStr = retStr + tempStr + colDelim; // Col 1
                if(splitRoofDetArrSize > 1) { 
                    // If it is a Roll.
                    retStr = retStr + splitRoofDet[0] + "\"" + " [" + splitRoofDet[1] + "] " +  get(roofBottomInsulationTypeDict, eachBottomRoof) + colDelim; // Col 2: Insulation & Systems
                    tab = splitRoofDet[2];
                    params = json();

                    if(tab <> "") {
                        // new sept 23 2019
                        insulationType = get(roofBottomInsulationTypeDict, eachBottomRoof);
                        tabType = get(roofBottomTabTypeDict, eachBottomRoof);
                        tabOption = get(roofBottomTabOptionDict, eachBottomRoof);
                        
                        jsonput(params, "tab", tab);
                        jsonput(params, "tabType", tabType);
                        jsonput(params, "tabOption", tabOption);
                        jsonput(params, "insulationType", insulationType);
                        tab = util.getTabDescription(params);                    
                    }
                    // retStr = retStr + get(roofBottomFacingDict, eachBottomRoof) + " [" + tab + " Tab]" + colDelim; // Col 3: Facing
                    retStr = retStr + get(roofBottomFacingDict, eachBottomRoof) + tab + colDelim; // Col 3: Facing
                } else {
                    // If it is a liner.
                    retStr = retStr + splitRoofDet[0] + colDelim; // Col 2: Part Number
                    retStr = retStr + " " + colDelim; // Col 3: Facing will be empty
                }
                // Calculate unit price
                roofBottomQtyTotal = get(roofBottomQuantityTotalDict, eachBottomRoof);
                roofBottomAmtTotal  = get(roofBottomAmtTotalDict, eachBottomRoof);
                unitPrice = round(roofBottomAmtTotal/roofBottomQtyTotal,3);   

                // Format values  
                jsonput(floatFormatParams,"floatValue", unitPrice);             
                formattedRoofBottomQty = util.numberFormat(roofBottomQtyTotal, FORMATTED_QTY_PLACES, FORMATTED_QTY_THOU_SEP, ".");
                formattedUnitPrice = util.floatToFormattedCurrency(floatFormatParams);

                // Set return str
                retStr = retStr + formattedRoofBottomQty + " " + get(roofBottomQuantityUOMDict, eachBottomRoof) + colDelim; // Col 4: Quantity
                retStr = retStr + formattedUnitPrice + colDelim; // Col 5: Net unit price
                retStr = retStr + formatascurrency(roofBottomAmtTotal, currency_t) + colDelim + rowDelim; // Col 6: Net Ext Total
            }
            
            for eachTopRoof in roofTopKeys {
                splitRoofDet = split(eachTopRoof, "_");
                tempStr = "ROOF TOP";
                if(sizeofarray(roofBottomKeys) == 0) {
                    tempStr = "ROOF";
                }
                retStr = retStr + tempStr + colDelim; // Col 1
                retStr = retStr + splitRoofDet[0] + "\"" + " [" + splitRoofDet[1] + "] " +  get(roofTopInsulationTypeDict, eachTopRoof) + colDelim; // Col 2: Insulation & Systems
                tab = splitRoofDet[2];
                /*if(tab == "") {
                    tab = "Long";
                } else {
                    tab = tab + "\"";
                }*/
                if(tab <> "") {
                    tab = " [" + tab + "\" Tab]";
                }
                // retStr = retStr + get(roofTopFacingDict, eachTopRoof) + " [" + tab + " Tab]" + colDelim; // Col 3: Facing
                // new sept 23 2019
                insulationType = get(roofTopInsulationTypeDict, eachTopRoof);
                if (insulationType == "Long Tab Banded System" OR insulationType == "Sag and Bag System") {
                    retStr = retStr + "UNFACED" + colDelim; // Col 3: Facing
                } else {
                    retStr = retStr + get(roofTopFacingDict, eachTopRoof) + tab + colDelim; // Col 3: Facing
                }
                // Calculate unitPrice
                roofTopAmtTotal = get(roofTopAmtTotalDict, eachTopRoof);
                roofTopQtyTotal = get(roofTopQuantityTotalDict, eachTopRoof);
                unitPrice = round(roofTopAmtTotal/roofTopQtyTotal,3);

                // Format values  
                jsonput(floatFormatParams,"floatValue", unitPrice);
                formattedRoofTopQuantity = util.numberFormat(roofTopQtyTotal, FORMATTED_QTY_PLACES, FORMATTED_QTY_THOU_SEP, ".");
                formattedUnitPrice = util.floatToFormattedCurrency(floatFormatParams);

                // Set return str            
                retStr = retStr + formattedRoofTopQuantity + " " + get(roofTopQuantityUOMDict, eachTopRoof) + colDelim; // Col 4: Quantity
                retStr = retStr + formattedUnitPrice + colDelim; // Col 5: Net unit price
                retStr = retStr + formatascurrency(roofTopAmtTotal, currency_t) + colDelim + rowDelim; // Col 5: Net total price
            }
            
            // Liner Roof
            if(linerRoofInsulationTypeStr <> "") {
                retStr = retStr + "LINER_ROOF" + colDelim + linerRoofInsulationTypeStr + colDelim;
                
                //if it's N/A, add "unfaced". SP-163
                if(linerRoofNA){
                    retStr = retStr + "UNFACED" + colDelim;
                }
                else{
                    retStr = retStr + colDelim;
                }

                // Calculate unit price
                unitPrice = 0.0;
                if(linerRoofQuantityTotal<>0){
                    unitPrice = round(linerRoofPriceTotal/linerRoofQuantityTotal,3);
                }

                // Format values  
                jsonput(floatFormatParams,"floatValue", unitPrice);
                formattedLinerRoofQtyTotal = util.numberFormat(linerRoofQuantityTotal, FORMATTED_QTY_PLACES, FORMATTED_QTY_THOU_SEP, ".");
                formattedUnitPrice = util.floatToFormattedCurrency(floatFormatParams);    

                retStr = retStr + formattedLinerRoofQtyTotal + " SF" + colDelim;
                retStr = retStr + formattedUnitPrice + colDelim;
                retStr = retStr + formatascurrency(linerRoofPriceTotal, currency_t) + colDelim;
                retStr = retStr + rowDelim;
                //SP-163
                if(NOT linerRoofNA){
                    retStr = retStr + "LINER-MAND-ACC_" + linerMandatoryAccessories + rowDelim;
                }
                roofLinerAccessoryKey = keys(linerRoofAccessoriesQuantityTotalDict);
                if(sizeofarray(roofLinerAccessoryKey) > 0) {
                    linerRoofAccessoryStr = linerOptionalAccessories;
                    for eachAcc in roofLinerAccessoryKey {
                        linerRoofAccessoryStr = linerRoofAccessoryStr + "(" + string(get(linerRoofAccessoriesQuantityTotalDict,eachAcc)) + ") " + get(linerRoofAccessoriesUOMDict,eachAcc) + " " + eachAcc + ", ";
                    }
                    if(endswith(linerRoofAccessoryStr, ", ")) {
                        linerRoofAccessoryStr = substring(linerRoofAccessoryStr, 0, -2);
                    }
                    retStr = retStr + "LINER-OPT-ACC_" + linerRoofAccessoryStr + colDelim + colDelim + formatascurrency(linerRoofAccessoriesPriceTotal, currency_t) + colDelim;
                    retStr = retStr + rowDelim;
                }
                retStr = retStr + "LINER-MAND-ACC_" + rowDelim; // For an empty line after Roof Liner.
            }
            
            wallKeys = keys(wallQuantityTotalDict);
            layerCount = 1;
            for eachWall in wallKeys {
                splitWallDet = split(eachWall, "_");
                insulationType = "";
                if(containskey(wallInsulationTypeDict, eachWall)){
                    insulationType = get(wallInsulationTypeDict, eachWall);
                }
                tempStr = "WALLS";
                if(combinedWallsBool) { 
                    tempStr = "COMBINED WALLS"; 
                    if(find(insulationType, "Filled Cavity System")<>-1){
                        tempStr = "WALLS LAYER " + string(layerCount);
                        layerCount = layerCount + 1;
                    }
                }
                
                retStr = retStr + tempStr + colDelim; // Col 1
                print splitWallDet;
                if(sizeofarray(splitWallDet) > 1) {
                    // If it is a Roll.
                    retStr = retStr + splitWallDet[0] + "\"" + " [" + splitWallDet[1] + "] " +  insulationType + colDelim; // Col 2: Insulation & Systems
                    tab = splitWallDet[2];
                    params = json();
                    if(tab <> "") {
                        // new sept 23 2019
                        tabType = get(wallTabTypeDict, eachWall);
                        tabOption = get(wallTabOptionDict, eachWall);
                        
                        jsonput(params, "tab", tab);
                        jsonput(params, "tabType", tabType);
                        jsonput(params, "tabOption", tabOption);
                        jsonput(params, "insulationType", insulationType);
                        tab = util.getTabDescription(params);
                    }
                    /*if(tab <> "") {
                        tab = " [" + tab + "\" Tab]";
                    }*/
                    // retStr = retStr + get(wallFacingDict, eachWall) + " [" + tab + " Tab]" + colDelim; // Col 3: Facing
                    retStr = retStr + get(wallFacingDict, eachWall) + tab + colDelim; // Col 3: Facing
                } else {
                    // If it is a Liner.
                    retStr = retStr + splitWallDet[0] + colDelim; // Col 2: Integration Part Number
                    retStr = retStr + " " + colDelim; // Col 3: Facing will be blank.
                }
                // Calculate unit price
                wallQtyTotal = get(wallQuantityTotalDict, eachWall);
                wallAmtTotal = get(wallAmtTotalDict, eachWall);
                unitPrice = round(wallAmtTotal/wallQtyTotal,3);

                // Format values  
                jsonput(floatFormatParams,"floatValue", unitPrice);  
                formattedWallQty = util.numberFormat(wallQtyTotal, FORMATTED_QTY_PLACES, FORMATTED_QTY_THOU_SEP, ".");            
                formattedUnitPrice = util.floatToFormattedCurrency(floatFormatParams);  

                // Set return str         
                retStr = retStr + formattedWallQty + " " + get(wallQuantityUOMDict, eachWall) + colDelim; // Col 4: Quantity
                retStr = retStr + formattedUnitPrice + colDelim; // Col 5: Net unit price
                retStr = retStr + formatascurrency(wallAmtTotal, currency_t) + colDelim + rowDelim; // Col 6: Net total price
            }

            // Partition walls
            partitionWallKeys = keys(partitionWallQuantityTotalDict);
            for eachWall in partitionWallKeys {
                splitPartitionWallDet = split(eachWall, "_");
                categoryLabel = "ADDITIONAL WALLS";            
                retStr = retStr + categoryLabel + colDelim; // Col 1
                if(sizeofarray(splitPartitionWallDet) > 1) {
                    // If it is a Roll.
                    retStr = retStr + splitPartitionWallDet[0] + "\"" + " [" + splitPartitionWallDet[1] + "] " +  get(partitionWallInsulationTypeDict, eachWall) + colDelim; // Col 2: Insulation & Systems
                    tab = splitPartitionWallDet[2];  
                    params = json();              
                    if(tab <> "") {
                        // new sept 23 2019
                        insulationType = get(partitionWallInsulationTypeDict, eachWall);
                        tabType = get(partitionWallTabTypeDict, eachWall);
                        tabOption = get(partitionWallTabOptionDict, eachWall);
                        
                        jsonput(params, "tab", tab);
                        jsonput(params, "tabType", tabType);
                        jsonput(params, "tabOption", tabOption);
                        jsonput(params, "insulationType", insulationType);
                        tab = util.getTabDescription(params);
                    }                 
                    retStr = retStr + get(partitionWallFacingDict, eachWall) + tab + colDelim; // Col 3: Facing
                } else {
                    // If it is a Liner.
                    retStr = retStr + splitPartitionWallDet[0] + colDelim; // Col 2: Integration Part Number
                    retStr = retStr + " " + colDelim; // Col 3: Facing will be blank.
                }
                // Calculate unit price
                partitionWallQtyTotal = get(partitionWallQuantityTotalDict, eachWall);
                partitionWallAmtTotal = get(partitionWallAmtTotalDict, eachWall);
                unitPrice = round(partitionWallAmtTotal/partitionWallQtyTotal,3);

                // Format values  
                jsonput(floatFormatParams,"floatValue", unitPrice);
                formattedWallQty = util.numberFormat(partitionWallQtyTotal, FORMATTED_QTY_PLACES, FORMATTED_QTY_THOU_SEP, ".");
                formattedUnitPrice = util.floatToFormattedCurrency(floatFormatParams);

                // Set unit price
                retStr = retStr + formattedWallQty + " " + get(partitionWallQuantityUOMDict, eachWall) + colDelim; // Col 4: Quantity
                retStr = retStr + formattedUnitPrice + colDelim; // Col 5: Net unit price
                retStr = retStr + formatascurrency(partitionWallAmtTotal, currency_t) + colDelim + rowDelim; // Col 6: Net total price
            }
            
            // Liner Wall
            if(linerWallInsulationTypeStr <> "") {
                retStr = retStr + "LINER_WALLS" + colDelim + linerWallInsulationTypeStr + colDelim;

                //if it's N/A, add "unfaced". SP-163
                if(linerWallNA){
                    retStr = retStr + "UNFACED" + colDelim;
                }
                else{
                    retStr = retStr + colDelim;
                }

                //Calculate unit price
                unitPrice =0.0;
                if(linerWallQuantityTotal>0){
                    unitPrice = round(linerWallPriceTotal/linerWallQuantityTotal,3);
                }

                // Format values  
                jsonput(floatFormatParams,"floatValue", unitPrice);
                formattedLinerWallQtyTotal = util.numberFormat(linerWallQuantityTotal, FORMATTED_QTY_PLACES, FORMATTED_QTY_THOU_SEP, ".");
                formattedUnitPrice = util.floatToFormattedCurrency(floatFormatParams); 

                retStr = retStr + formattedLinerWallQtyTotal + " SF" + colDelim;
                retStr = retStr + formattedUnitPrice + colDelim;
                retStr = retStr + formatascurrency(linerWallPriceTotal, currency_t) + colDelim;
                retStr = retStr + rowDelim;
                //SP-163
                if(NOT linerWallNA){
                    retStr = retStr + "LINER-MAND-ACC_" + linerMandatoryAccessories + rowDelim;
                }
                wallLinerAccessoryKey = keys(linerWallAccessoriesQuantityTotalDict);
                if(sizeofarray(wallLinerAccessoryKey) > 0) {
                    linerWallAccessoryStr = linerOptionalAccessories;
                    for eachAcc in wallLinerAccessoryKey {
                        linerWallAccessoryStr = linerWallAccessoryStr + "(" + string(get(linerWallAccessoriesQuantityTotalDict,eachAcc)) + ") " + get(linerWallAccessoriesUOMDict,eachAcc) + " " + eachAcc + ", ";
                    }
                    if(endswith(linerWallAccessoryStr, ", ")) {
                        linerWallAccessoryStr = substring(linerWallAccessoryStr, 0, -2);
                    }
                    retStr = retStr + "LINER-OPT-ACC_" + linerWallAccessoryStr + colDelim + colDelim + formatascurrency(linerWallAccessoriesPriceTotal, currency_t) + colDelim;
                    retStr = retStr + rowDelim;
                }
            }

            // Liner Partition Wall
            if(linerPartitionWallInsulationTypeStr <> "") {            
                retStr = retStr + "LINER_ADDITIONAL WALLS" + colDelim + linerPartitionWallInsulationTypeStr + colDelim;
                //if it's N/A, add "unfaced". SP-163
                if(linerPartitionWallNA){
                    retStr = retStr + "UNFACED" + colDelim;
                }
                else{
                    retStr = retStr + colDelim;
                }
                //Calculate unit price
                unitPrice = round(linerPartitionWallPriceTotal / linerPartitionWallQuantityTotal,3);

                // Format values  
                jsonput(floatFormatParams,"floatValue", unitPrice);
                formattedLinerPartitionWallQtyTotal = util.numberFormat(linerPartitionWallQuantityTotal, FORMATTED_QTY_PLACES, FORMATTED_QTY_THOU_SEP, ".");
                formattedUnitPrice = util.floatToFormattedCurrency(floatFormatParams);

                retStr = retStr + formattedLinerPartitionWallQtyTotal + " SF" + colDelim;
                retStr = retStr + formattedUnitPrice + colDelim;
                retStr = retStr + formatascurrency(linerPartitionWallPriceTotal, currency_t) + colDelim;
                retStr = retStr + rowDelim;
                //SP-163
                if(NOT linerPartitionWallNA){
                    retStr = retStr + "LINER-MAND-ACC_" + linerMandatoryAccessories + rowDelim;
                }
                partitionWallLinerAccessoryKey = keys(linerPartitionWallAccessoriesQuantityTotalDict);
                if(sizeofarray(partitionWallLinerAccessoryKey) > 0) {
                    linerPartitionWallAccessoryStr = linerOptionalAccessories;
                    for eachAcc in partitionWallLinerAccessoryKey {
                        linerPartitionWallAccessoryStr = linerPartitionWallAccessoryStr + "(" + string(get(linerPartitionWallAccessoriesQuantityTotalDict,eachAcc)) + ") " + get(linerPartitionWallAccessoriesUOMDict,eachAcc) + " " + eachAcc + ", ";
                    }
                    if(endswith(linerPartitionWallAccessoryStr, ", ")) {
                        linerPartitionWallAccessoryStr = substring(linerPartitionWallAccessoryStr, 0, -2);
                    }
                    retStr = retStr + "LINER-OPT-ACC_" + linerPartitionWallAccessoryStr + colDelim + colDelim + formatascurrency(linerPartitionWallAccessoriesPriceTotal, currency_t) + colDelim;
                    retStr = retStr + rowDelim;
                }
            }
            
            accKeys = keys(accessoriesQuantityTotalDict);
            if(sizeofarray(accKeys) > 0) {
                accessories = "Accessories: ";        
                accessoryQtyTotal  = 0;
                for eachAcc in accKeys {
                    accessories = accessories + " ["+ string(get(accessoriesQuantityTotalDict, eachAcc)) + "] " + get(accessoriesUOMDict, eachAcc) + " " + eachAcc + ", ";
                    accessoryQtyTotal = accessoryQtyTotal + get(accessoriesQuantityTotalDict, eachAcc);
                }            
                if(endswith(accessories, ", ")) {
                    accessories = substring(accessories, 0, -2);
                    accessories = accessories + ".";
                }
                //unitPrice = accessoriesPriceTotal / accessoryQtyTotal;
                accessories = accessories + colDelim + colDelim + formatascurrency(accessoriesPriceTotal, currency_t) + colDelim;
                retStr = retStr + accessories + rowDelim;
            }
            //print doorsJObj;
            doorKeys = jsonkeys(doorsJObj);
            print doorKeys;
            //key = line #, value =json
            //secondary json keys = quantity, desc, includes (list), net, list
        multiDoor=false;
            for doorKey in doorKeys{
                doorJObj = jsonget(doorsJObj, doorKey, "json", json());
                desc = jsonget(doorJObj, "desc", "string", "");
                //qty = jsonget(doorJObj, "quantity", "string", "");
                //this is an obj with numeric keys so that we can sort them
                includes = jsonget(doorJObj, "includes", "json", json());
                net = jsonget(doorJObj, "net", "float", 0.0);
        
            doorInclusionKeys = jsonkeys(includes);
            includesList=string[];
            sort(doorInclusionKeys , "asc");
            for doorInclusionKey in doorInclusionKeys {
                includedComponent = jsonget(includes, doorinclusionKey, "string", "");
                if(includedComponent<>""){
                    append(includesList, includedComponent);
                }
            }
                //includesList = util.jsonStringArrayToArray(includes);
                formattedInclusions =  join(includesList, ", ");
                //formattedInclusions = " ("+qty+") " + join(includesList, ", ("+qty+") ");
            if(multiDoor){
                retStr = retStr + "DOORS-SECTION-MORE";
            }
            else{
                retStr = retStr + "DOORS-SECTION-START";
            }
                retStr = retStr + colDelim + desc + colDelim + colDelim + colDelim + colDelim  + colDelim + rowDelim;
                retStr = retStr + "DOORS-INC" + colDelim + formattedInclusions + colDelim + colDelim + formatascurrency(net, currency_t) + colDelim + rowDelim;
                multiDoor=true;
            }


            formattedBuildingTotalQty = util.numberFormat(buildingTotalQty, FORMATTED_QTY_PLACES, FORMATTED_QTY_THOU_SEP, ".");
            retStr = retStr + "TOTAL" + colDelim + formattedBuildingTotalQty + " SF" + colDelim + colDelim + formatascurrency(buildingTotalPrice, currency_t) + rowDelim;
            retStr = retStr + builidingDelim;        
            print lineSequenceNumber;
            //Store building line number
            jsonput(jsonpathgetsingle(retJson, "$.buildings", "json"), string(lineSequenceNumber),retStr);
            append(buildingLineNumbers, lineSequenceNumber);
            
            // JS: 13/01/21 We are now storing each building str in json along with customSeqNumber for sorting purposes.
            // Therefore reset retStr after each iteration        
            retStr = ""; 
        
        } // elif((buildingLine._parent_doc_number == "" OR isnull(buildingLine._parent_doc_number)) AND upper(buildingLine._part_desc) <> "FREIGHT" AND buildingLine._model_variable_name=="" AND buildingLine.integrationPartNumber_l<>"" AND NOT(isnull(buildingLine.integrationPartNumber_l))) {
        elif((buildingLine._parent_doc_number == "" OR isnull(buildingLine._parent_doc_number)) AND NOT freightLine AND buildingLine._model_variable_name=="") {
            // For the parts added through quick key to be grouped under Miscellaneous 
            childTypeKey = buildingLine.integrationPartNumber_l;            
            
            //split for miscellaneous or credit line

            if(buildingLine.isCreditLine_l){
                //credit lines
                quantity = 0;
                creditPartJson = json();
                if(containskey(creditQuantityTotalDict, childTypeKey)) {
                    quantity = get(creditQuantityTotalDict, childTypeKey);
                }
                quantity = quantity + buildingLine.requestedQuantity_l;
                put(creditQuantityTotalDict, childTypeKey, quantity);
                
                // Quantity UOM:
                if(NOT(containskey(creditQuantityUOMDict, childTypeKey))) {
                    put(creditQuantityUOMDict, childTypeKey, buildingLine._part_custom_field15);
                }
                
                // Total Net Amount
                amount = 0.0;
                if(containskey(creditAmtTotalDict, childTypeKey)) {
                    amount = get(creditAmtTotalDict, childTypeKey);
                }
                amount = amount + buildingLine.netAmount_l;
                put(creditAmtTotalDict, childTypeKey, amount);

                // Total List Amount
                amount = 0.0;
                if(containskey(creditListTotalDict, childTypeKey)) {
                    amount = get(creditListTotalDict, childTypeKey);
                }
                amount = amount + buildingLine.listPrice_l;
                put(creditListTotalDict, childTypeKey, amount);
                
                // Adhoc Part Description: Part Number + Part Description
                if(NOT(containskey(creditDescriptionDict, childTypeKey))) {
                    put(creditDescriptionDict, childTypeKey, "CREDIT_"+buildingLine._part_display_number+": "+buildingLine._part_desc);
                }
                
                append(creditLineNumbers, lineSequenceNumber);                
                jsonput(creditPartJson, "lineNumber", lineSequenceNumber);                
                jsonput(creditPartJson, "retStr", "");
                jsonput(jsonpathgetsingle(retJson,"$.credit","json"), childTypeKey, creditPartJson);
            }
            else{
                // For the parts added through quick key to be grouped under Miscellaneous 
                quantity = 0;
                miscPartJson = json();
                if(containskey(miscQuantityTotalDict, childTypeKey)) {
                    quantity = get(miscQuantityTotalDict, childTypeKey);
                }
                quantity = quantity + buildingLine.requestedQuantity_l;
                put(miscQuantityTotalDict, childTypeKey, quantity);
                
                // Quantity UOM:
                if(NOT(containskey(miscQuantityUOMDict, childTypeKey))) {
                    put(miscQuantityUOMDict, childTypeKey, buildingLine._part_custom_field15);
                }
                
                // Total Net Amount
                amount = 0.0;
                if(containskey(miscAmtTotalDict, childTypeKey)) {
                    amount = get(miscAmtTotalDict, childTypeKey);
                }
                amount = amount + buildingLine.netAmount_l;
                put(miscAmtTotalDict, childTypeKey, amount);

                // Total List Amount
                amount = 0.0;
                if(containskey(miscListTotalDict, childTypeKey)) {
                    amount = get(miscListTotalDict, childTypeKey);
                }
                amount = amount + buildingLine.listPrice_l;
                put(miscListTotalDict, childTypeKey, amount);
                
                // Adhoc Part Description: Part Number + Part Description
                if(NOT(containskey(miscDescriptionDict, childTypeKey))) {
                    put(miscDescriptionDict, childTypeKey, "MISC_"+buildingLine._part_display_number+": "+buildingLine._part_desc);
                }            
                jsonput(miscPartJson, "lineNumber", lineSequenceNumber);
                append(miscLineNumbers, lineSequenceNumber);
                jsonput(miscPartJson, "retStr", "");
                jsonput(jsonpathgetsingle(retJson,"$.misc","json"), childTypeKey, miscPartJson);                
            }
        } 
    }    

    // Forming the string for the Miscellaneous group.
    miscKeys = keys(miscQuantityTotalDict);    
    sortedMiscStr = "";    
    if(sizeofarray(miscKeys) > 0) {
        sortedMiscStr = sortedMiscStr + "MISCELLANEOUS" + rowDelim;
        for eachMisc in miscKeys {
            miscStr = "";

            // Calculate unit price
            miscQtyTotal = get(miscQuantityTotalDict,eachMisc);
            miscAmtTotal = get(miscAmtTotalDict,eachMisc);
            unitPrice = round(miscAmtTotal / miscQtyTotal,3);

            // Format values  
            jsonput(floatFormatParams,"floatValue", unitPrice);
            formattedMiscQty = util.numberFormat(miscQtyTotal, FORMATTED_QTY_PLACES, FORMATTED_QTY_THOU_SEP, ".");
            formattedUnitPrice = util.floatToFormattedCurrency(floatFormatParams); 

            // Set return str       
            miscStr = miscStr + get(miscDescriptionDict,eachMisc) + colDelim; // Description.
            miscStr = miscStr + formattedMiscQty + " " + get(miscQuantityUOMDict,eachMisc) + colDelim; // Quantity.
            miscStr = miscStr + formattedUnitPrice + colDelim; // Net unit price 
            miscStr = miscStr + formatascurrency(miscAmtTotal, currency_t) + colDelim + rowDelim; // Net total price.

            jsonpathset(retJson, "$.misc." + eachMisc + ".retStr", miscStr);    
        }                
        sort(miscLineNumbers, "asc");        
        rng = range(sizeofarray(miscLineNumbers));        
        for idx in rng {       
            miscRetStrs = jsonpathgetmultiple(retJson, "$.misc..[?(@.lineNumber==" + string(miscLineNumbers[idx]) + ")].retStr", false);            
            miscRetStrsRng = range(jsonarraysize(miscRetStrs));
            for miscStr in miscRetStrsRng {
                sortedMiscStr = sortedMiscStr + jsonarrayget(miscRetStrs,miscStr);
            }        
        }
        sortedMiscStr = sortedMiscStr + builidingDelim;
    }   

    // Forming the string for the Credit group.
    creditKeys = keys(creditQuantityTotalDict);
    creditString = "";
    sortedCreditStr = "";
    if(sizeofarray(creditKeys) > 0) {
        creditString = creditString + "CREDIT" + rowDelim;
        for eachCredit in creditKeys {
            // Calculate unit price
            creditQtyTotal = get(creditQuantityTotalDict,eachCredit);
            creditAmtTotal = get(creditAmtTotalDict,eachCredit);
            unitPrice = round(creditAmtTotal / creditQtyTotal,3);

            // Format values  
            jsonput(floatFormatParams,"floatValue", unitPrice);
            formattedCreditQty = util.numberFormat(creditQtyTotal, FORMATTED_QTY_PLACES, FORMATTED_QTY_THOU_SEP, ".");
            formattedUnitPrice = util.floatToFormattedCurrency(floatFormatParams); 

            // Set return str       
            creditString = creditString + get(creditDescriptionDict,eachCredit) + colDelim; // Description.
            creditString = creditString + formattedCreditQty + " " + get(creditQuantityUOMDict,eachCredit) + colDelim; // Quantity.
            creditString = creditString + formattedUnitPrice + colDelim; // Net unit price 
            creditString = creditString + formatascurrency(creditAmtTotal, currency_t) + colDelim + rowDelim; // Net total price.

            jsonpathset(retJson, "$.credit." + eachCredit + ".retStr", creditString);
        }
        sort(creditLineNumbers, "asc");        
        rng = range(sizeofarray(creditLineNumbers));        
        for idx in rng {       
            creditRetStrs = jsonpathgetmultiple(retJson, "$.credit..[?(@.lineNumber==" + string(creditLineNumbers[idx]) + ")].retStr", false);            
            creditRetStrsRng = range(jsonarraysize(creditRetStrs));
            for creditStr in creditRetStrsRng {
                sortedCreditStr = sortedCreditStr + jsonarrayget(creditRetStrs,creditStr);
            }        
        }
        sortedCreditStr = sortedCreditStr + builidingDelim;
    }

    // Sort building str based on line number asc
    sortedRetStr = "";
    sortedBuildingStr = "";
    sort(buildingLineNumbers, "asc");   
    print buildingLineNumbers;     
    rng = range(sizeofarray(buildingLineNumbers));   
    print retJson;     
    for idx in rng {       
        buildingRetStrs = jsonpathgetmultiple(retJson, "$.buildings." + string(buildingLineNumbers[idx]) + ".retStr", false);        
        buildingRetStrsRng = range(jsonarraysize(buildingRetStrs));
        for buildingStr in buildingRetStrsRng {
            sortedBuildingStr = sortedBuildingStr + jsonarrayget(buildingRetStrs,buildingStr);
        }        
    }

    // Construct final sorted ret str
    sortedRetStr = sortedBuildingStr + sortedMiscStr + sortedCreditStr;    
    return sortedRetStr;
```

## Revision History
 Rev. Date |Developer            |Notes / Comments
 ----------|---------------------|-------------------------------------------------------------------------
 2021-01-15|jonathan.serna@piercewashington.com|
