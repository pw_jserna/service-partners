# Set bottom line discounts



## Syntax
```
setBottomLineDiscounts();
```

## Returns
Type|Description
-|-
```String```|


## Attributes
Name|Description
-|-
```bottomLinePercentageDiscount_t```|
```lineDiscount_t```|
```lineDiscountType_t```|
```_document_number```|
```customDiscountType_l```|
```customDiscountValue_l```|
```_parent_doc_number```|
```_part_display_number```|
```_system_selected_document_number```|


## Source
```Java
retStr = "";
PRICE_OVERRIDE_DISC_TYPE = "Price Override";
selectedDocNumArr = split(_system_selected_document_number, "~");
customDiscountType = lineDiscountType_t;
customDiscountAmount = lineDiscount_t;
exemptPartNums = string[]{"FS"};


for line in transactionLine {
    docNum = line._document_number;
    parentDocNum = line._parent_doc_number;
    partExempt = (findinarray(exemptPartNums, line._part_display_number) <> -1);
    isModel = ((line._model_variable_name <> "" OR line._model_variable_name <> "newPart") AND line._part_number == "");

    // Ensure model lines are zero priced 
    if (isModel) {
        retStr = retStr + docNum + "~customDiscountType_l~" + PRICE_OVERRIDE_DISC_TYPE + "|";
        retStr = retStr + docNum + "~customDiscountValue_l~0.0|";
        continue;
    }

    if (((findinarray(selectedDocNumArr, parentDocNum) <> -1) OR selectedDocNumArr[0] == "") AND NOT partExempt) {
        retStr = retStr + docNum + "~customDiscountType_l~" + customDiscountType + "|";
        if (customDiscountType == "Amount Off") {
            retStr = retStr + docNum + "~customDiscountAmount_l~" + string(customDiscountAmount) + "|";
        }
        retStr = retStr + docNum + "~customDiscountValue_l~" + string(customDiscountAmount) + "|";        
    }    
}

return retStr;
```

## Revision History
 Rev. Date |Developer            |Notes / Comments
 ----------|---------------------|-------------------------------------------------------------------------
 2020-01-08|Jserna               |Initial version
 2020-01-23|Jserna               |SP-113 ability to set discount type and discount to all lines
 2020-12-16|JSerna               |Exempt parts
 2020-12-17|JSerna               |Exclude discounting
