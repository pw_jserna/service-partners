# Pre Formula Pricing
pricing Script sets base line level attribute values by iterating through the lines in the transaction. These values are used by the formulas to set other line and quote level attributes.


## Syntax
```
preFormulaPricing(action);
```

## Returns
Type|Description
-|-
```String```|

## Parameters
Name|Type|Description
-|-|-
```action```|```String```|

## Attributes
Name|Description
-|-
```businessUnit_t```|
```placementDiagramFile_t```|
```configPlacementDiagram_t```|
```shipToPartyID_t```|
```missingPrice_t```|
```isPartnerUser_t```|
```lineItemsAdded_t```|
```buildingDiscountsJSON_t```|
```_document_number```|
```_part_number```|
```listPrice_l```|
```_line_item_comment```|
```_line_item_spare_rule_var_name```|
```_config_attr_info```|
```_parent_doc_number```|
```_model_variable_name```|
```_price_list_price_each```|
```_part_display_number```|
```integrationPartNumber_l```|
```_model_name```|
```lineNumber_l```|
```_part_custom_field15```|
```requestedQuantity_l```|
```partDescScrubbed_l```|
```_part_desc```|
```configPacementDiagram_l```|
```isCreditLine_l```|
```_part_custom_field14```|
```lineComment_l```|
```customDiscountValue_l```|
```_part_custom_field21```|
```configurationDataJSON_l```|
```isLinerPart_l```|
```partType_l```|
```_part_custom_field9```|
```section_l```|
```_system_buyside_id```|
```_system_supplier_company_name```|

## Functions
Type|Function Signature
-|-
```Json```|[listPartDetails(String[] partsList)](listPartDetails)
```Json```|[setPricedValues(String priceXMLData, Json params)](setPricedValues)
```String```|[Dictionary executeCommerceSoapApi(String soapMessage, String companyName)](Dictionary executeCommerceSoapApi)
```String```|[Dictionary getEnvironmentVariableValues(String siteName, String[] variables)](Dictionary getEnvironmentVariableValues)
```String```|[getConfigurationAttributeValue(String configData, String attrName)](getConfigurationAttributeValue)
```String```|[getLineType(String modelVarName, String parentDocNumber, String partNumber)](getLineType)
```String```|[numberFormat(Float number, Integer places, String thousandSeparator, String decimalSeparator)](numberFormat)
```String```|[scrubXmlSoapInput(String input)](scrubXmlSoapInput)
```String```|[sendAPIRequest(String urlType, String method, String requestBody)](sendAPIRequest)
```String[]```|[jsonStringArrayToArray(JsonArray jList)](jsonStringArrayToArray)
```String[]```|[mergeStringArrays(String[] arr1, String[] arr2)](mergeStringArrays)
```String[]```|[prependBUForPartNumbers(String businessUnit, String[] displayPartsList)](prependBUForPartNumbers)

## Source
```Java
//************************************************************************************************************
//Model BU const
MODEL_BU = "MODEL";
DEBUG = false;
// Initialize variables
returnString = "";
warningMessage = "";
warnList = String[];
lineSequenceNumber = 0;
lineType = "";
lineDocJObj = json();
lastConfiguredModelDocNum = "";
pricingXml = "";
actionArr = split(lower(action), "~");
lineDefaultAction = findinarray(actionArr, "line_default");
refreshPartsAction = (findinarray(actionArr, "refresh_parts")<>-1);
saveAction = findinarray(actionArr, "save");
fetchPriceAction = (findinarray(actionArr, "fetch_price") <> -1); 
lineItemCount = 0;
buildingDiscounts = json();
if (buildingDiscountsJSON_t <> "") {
  buildingDiscounts = json(buildingDiscountsJSON_t);
}
FSDocNum = "";
FSCurrentVal = 0.0;
freightVal = 0.0;

// Get credit line types from env variables
variables = string[]{"creditLineTypes","branchAgnosticLineTypes"};
variablesDict = util.getEnvironmentVariableValues(_system_supplier_company_name, variables);
creditLineTypes = split(get(variablesDict, "creditLineTypes"), ";");
branchAgnosticLineTypes = split(get(variablesDict, "branchAgnosticLineTypes"), ";");
print "LINE DEFAULT EXECUTED: " + string(lineDefaultAction);

linerPricingJObj = json();
totalLinerSqFt = 0;
missingPrice = false;
// Loop through new lines being added
for line in transactionLine {
    lineNumber = line.lineNumber_l;
    configData = line.configurationDataJSON_l;
    isConfigPart = false;  
    isCreditLine = false;

    lineDocNum = line._document_number;
    linePartNumber = line._part_number;
    lineDisplayNumber = line._part_display_number;
    lineParentDocNumber = line._parent_doc_number;
    lineModelVarName = line._model_variable_name;
    lineComment = line._line_item_comment;
    lineIntegrationPartNumber = line.integrationPartNumber_l;
    lineModelName = line._model_name;
    lineActualUOM = line._part_custom_field15;
    lineQty = line.requestedQuantity_l;
    lineDesc = line._part_desc;    
    lineType = util.getLineType(lineModelVarName, lineParentDocNumber, linePartNumber); // CONFIG_ROOT, CONFIG_PART, CONFIG_MODEL, PART
    linePartType = line._part_custom_field14;
    lineAdditionalMBIComments = line.lineComment_l;
    lineItemCount = lineItemCount + 1;
    isLiner = line.isLinerPart_l;
    partType = line.partType_l;
    thickness = line._part_custom_field9;
    section = line.section_l;

    LineObj = json();
    jsonput(LineObj, "LineType", lineType);
    jsonput(LineObj, "DocumentNumber", lineDocNum);
    jsonput(LineObj, "ParentDocNumber", lineParentDocNumber);
    jsonput(LineObj, "ModelName", lineModelName);
    jsonput(LineObj, "ModelVarName", lineModelVarName);    
    jsonput(lineObj, "BranchAgnostic", "false");
    
    //check for FS Doc num
    if(line._part_display_number=="FS"){      
      FSDocNum = line._document_number; 
      FSCurrentVal = line.customDiscountValue_l;   
    }
    //check for freight value (in case of deletion this will update the total)
    if(line._part_display_number=="F"){
        freightVal = line.customDiscountValue_l;
    }
    //need to do this regardless of linedefault
    if (findinarray(creditLineTypes, linePartType) <> -1) {
        isCreditLine = true;          
    }  
    if (findinarray(branchAgnosticLineTypes, linePartType) <> -1) {
      //will consider the part valid if a bu-part# match OR 0000-part# match is found
        jsonput(lineObj, "BranchAgnostic", "true");         
    }  

    if(lineDefaultAction <> -1) {  // set up lines
          lineSeqStr = "";
          lineIntegrationPartNumber = lineDisplayNumber;  
          if(lineType == "CONFIG_ROOT" or lineType == "PART") {
             lineSequenceNumber = lineSequenceNumber + 1;             
             lineSeqStr = string(lineSequenceNumber);
             lineNumber = string(lineSequenceNumber);
             jsonput(LineObj, "SequenceNumber", lineSequenceNumber);
             if(lineType == "CONFIG_ROOT") { // building node
                // Set modelPlacementDiagram flag
                if (line.configPacementDiagram_l <> ""){
                  returnString = returnString + lineDocNum + "~modelPlacementDiagram_l~true|";
                }
                lineIntegrationPartNumber = "";
                configData = util.getConfigurationAttributeValue(line._config_attr_info, "buildingInsulationConfigJSON");
                buildingJson = json(configdata);
                buildingLabel = jsonget(buildingJson, "Label", "string", "Building");
                splitType = jsonpathgetsingle(buildingJson,"$.Roof.SplitRolls","string", ""); 
                lineModelName = buildingLabel;
                returnString = returnString + lineDocNum + "~_model_name~" + upper(buildingLabel) + "|"; 
                returnString = returnString + lineDocNum + "~_part_display_number~" + upper(buildingLabel) + "|";
                //returnString = returnString + lineDocNum + "~_part_number~" + upper(buildingLabel) + "|";
                returnString = returnString + lineDocNum + "~modelPartNumber_l~" + upper(buildingLabel) + "|";
                returnString = returnString + lineDocNum + "~configurationID_l~" + jsonget(buildingJson, "configID", "string", "") + "|";
                returnString = returnString + lineDocNum + "~buildingWidth_l~" + jsonget(buildingJson, "Width", "string", "") + "|";
                returnString = returnString + lineDocNum + "~buildingHeight_l~" + jsonget(buildingJson, "Height", "string", "") + "|";
                returnString = returnString + lineDocNum + "~buildingLength_l~" + jsonget(buildingJson, "Length", "string", "") + "|";
                returnString = returnString + lineDocNum + "~buildingRoofType_l~" + jsonget(buildingJson, "RoofType", "string", "") + "|";
                returnString = returnString + lineDocNum + "~buildingPitch_l~" + jsonget(buildingJson, "Pitch1", "string", "") + "|";
                returnString = returnString + lineDocNum + "~buildingLabel_l~" + jsonget(buildingJson, "Label", "string", "") + "|";
                returnString = returnString + lineDocNum + "~buildingSplitRoofRolls_l~" + splitType + "|";
                attachData = util.getConfigurationAttributeValue(line._config_attr_info, "placementDiagramContent");
                if(len(line.lineNumber_l) == 0) {  
                        lastConfiguredModelDocNum = lineDocNum;
                }
             }
             
          }
          else { // config_model or config_part 
             if(lineModelVarName == "accessories") {
                configData = "{}";
             }
             else {
                configData = lineComment;
                if(lineComment==""){
                  configData=jsontostr(json());
                }
             }
             configJobj = json(configData);

             parentObj = jsonget(lineDocJObj, lineParentDocNumber, "json");
             parentSeq = jsonget(parentObj, "SequenceNumber", "integer");             
             lineIndex = line._line_item_spare_rule_var_name;             
             lineIndex = replace(lineIndex, ".", "");            
             //lineSeqStr = string(parentSeq) + "." + lineIndex;             
             lineNumber = "  " + string(parentSeq) + "." + line._line_item_spare_rule_var_name;
             idx =find(lineNumber, ".");
             lineSeqStr = substring(lineNumber, idx + 1);              
             
             if(lineModelVarName <> "" and lineModelVarName <> "accessories") {
                modelType = jsonget(configJobj, "section", "string", "");
                modelName = "";
                modelDesc = "";                 
                if(lineModelVarName == "newPart") { // new part not existing in db
                    configPartNumber = jsonget(configJobj, "partNumber", "string", "NOT-FOUND");
                    // Get model part detail
                    //sven workaround - filled cavity error
                    modelDisplayNum = MODEL_BU + "-" + configPartNumber;
                    partList = string[]{modelDisplayNum};                    
                    modelPartDetail = util.listPartDetails(partList);
                    modelDesc = jsonpathgetsingle(modelPartDetail, "$." + modelDisplayNum + ".Description", "string", "");                    
                    modelQtyUOM = jsonpathgetsingle(modelPartDetail, "$." + modelDisplayNum + ".QuantityUOM", "string", "");  
                    modelPricingUOM = jsonpathgetsingle(modelPartDetail, "$." + modelDisplayNum + ".PricingUOM", "string", ""); 
                    modelRValue = jsonpathgetsingle(modelPartDetail, "$." + modelDisplayNum + ".RValue", "string", ""); 
                    modelThickness = jsonpathgetsingle(modelPartDetail, "$." + modelDisplayNum + ".Thickness", "string", ""); 
                    modelWidth = jsonpathgetsingle(modelPartDetail, "$." + modelDisplayNum + ".Width", "string", ""); 
                    if (modelPricingUOM == "Per Thousand SQ. Feet") {
                      modelPricingUOM = "MS";
                    }                   

                    // Construct line details based on properties in payload
                    if (modelDesc == "") {
                      if(modelType == "AccessoryPart") {
                        modelDesc = jsonget(configJobj, "type", "string", "");
                      }
                      elif(modelType == "Doors") {
                        modelDesc = jsonget(configJobj, "doorElement", "string", "");
                      }
                      else {
                        width = jsonget(configJobj, "rollWidth", "integer", 0);
                        widthInches = width * 12;
                        rollType = jsonget(configJobj, "type", "string", "");
                        rollRVal = jsonget(configJobj, "rvalue", "string", "");
                        thickness = jsonget(configJobj, "thickness", "string", "");
                        slit = jsonget(configJobj, "slit", "integer", 0);
                        if(slit > 0) {
                          widthInches = slit;
                        }
                        if(rollType == "Top Layer") {
                          thickness = jsonget(configJobj, "thicknessTop", "string", "");
                          rollRVal = jsonget(configJobj, "rvalueTop", "string", "");
                        }
                        elif(rollType == "Bottom Layer") {
                          thickness = jsonget(configJobj, "thicknessBottom", "string", "");
                          rollRVal = jsonget(configJobj, "rvalueBottom", "string", "");
                        }
                        if(rollRVal <> "") {
                          modelDesc = rollRVal + " " + thickness + "\"X" + string(widthInches) + "\" " + jsonget(configJobj, "facing") + " FACED";
                          returnString = returnString + lineDocNum + "~_part_custom_field8~" + rollRVal + "|";
                          returnString = returnString + lineDocNum + "~_part_custom_field9~" + thickness + "|"; // new sept 23 2019, prev: jsonget(configJobj, "thickness")
                          returnString = returnString + lineDocNum + "~_part_custom_field10~" + string(widthInches) + "|";                          
                          returnString = returnString + lineDocNum + "~_part_custom_field15~" + "SF" + "|";
                          returnString = returnString + lineDocNum + "~_part_custom_field11~" + "MS" + "|";
                        }
                        else {
                          modelDesc = rollType;
                        }
                        
                      }                                            
                    } else {
                      returnString = returnString + lineDocNum + "~_part_custom_field8~" + modelRValue + "|";
                      returnString = returnString + lineDocNum + "~_part_custom_field9~" + modelThickness + "|";
                      returnString = returnString + lineDocNum + "~_part_custom_field10~" + modelWidth + "|";
                      returnString = returnString + lineDocNum + "~_part_custom_field15~" + modelQtyUOM + "|";
                      returnString = returnString + lineDocNum + "~_part_custom_field11~" + modelPricingUOM + "|";                      
                    }                   

                    modelName = jsonget(json(configData), "partNumber", "string", "NOT FOUND");
                    lineIntegrationPartNumber = modelName;
                    linePartNumber = businessUnit_t + "-" + modelName;                                     
                }
                else { // models Roof , Walls
                    modelName = jsonget(configJobj, "Label", "string", "");
                    tabOpt = jsonget(configJobj, "TabOption", "string", "");
                    combinedRValue = jsonget(configJobj, "CombinedRValue", "string", "");
                    rVal = jsonget(configJobj, "RValue", "string", "");
                    doorType = jsonget(configJobj, "doorDesc", "string", "");
                    modelDesc = replace(jsonget(configJobj, "InstallationMethod", "string", ""), "System", "");
                    if(tabOpt <> "") {
                     modelDesc = modelDesc + ", " + tabOpt; 
                    }
                    if(combinedRValue <> "") {
                      modelDesc = modelDesc + ", Combined " + combinedRValue; 
                    }
                    elif(rVal <> "") {
                      modelDesc = modelDesc + ", " + rVal;
                    }
                    elif(doorType<>""){
                      returnString = returnString + lineDocNum + "~door_l~true|";
                      modelDesc = doorType;
                    }
                    returnString = returnString + lineDocNum + "~_part_display_number~" + upper(modelName) + "|";
                    
                }                

                lineModelName = modelName;
                lineDesc = modelDesc;
                returnString = returnString + lineDocNum + "~_model_name~" + upper(modelName) + "|";
                returnString = returnString + lineDocNum + "~_part_desc~" + modelDesc + "|";    

             }
             if(lineModelVarName == "accessories") {
              returnString = returnString + lineDocNum + "~_part_display_number~" + "Accessories" + "|";

             }
             
             if(lineType == "CONFIG_PART") {
                desc2 = "";
                partType = jsonget(configJobj, "partType", "string", "");
                numRolls = jsonget(configJobj, "numRolls", "string", "");
                rollLength = jsonget(configJobj, "rollLength", "string", ""); 
                rollWidth = jsonget(configJobj, "rollWidth", "string", ""); 

                section = jsonget(configJobj, "section", "string", "");
                sectionType = jsonget(configJobj, "type", "string", "");
                if(sectionType <> "") {
                  section = section + "-" + sectionType;
                }
                sectionLabel = jsonget(configJobj, "sectionLabel", "string", "");
                tab = jsonget(configJobj, "tab", "string", "");
                tabType = jsonget(configJobj, "tabType", "string", "");
                tabOption = jsonget(configJobj, "tab", "string", "");

                //SP-78
                cutType = jsonget(configJObj, "cutType", "string", "");
                desc2 = jsonget(configJobj, "description", "string", "");
                if (upper(cutType) == "CUT") {
                  descIdx = find(desc2,",");
                  lineAdditionalMBIComments = trim(substring(desc2, descIdx + 1));
                  desc2 = substring(desc2, 0, descIdx);                  
                } 
                
                door = (jsonget(configJobj, "doorElement", "string", "")<>"");
                if(door){
                  //doors use _part_custom_field21 and not description
                  desc2 = line._part_custom_field21;
                }
                buildLabel = jsonget(configJobj, "BuildingLabel", "string", "");
                placeId = jsonget(configJobj, "placementCode", "string", "");
                installationMethod = jsonget(configJobj, "installationMethod", "string", "");
                facing = jsonget(configJobj, "facing", "string", "");
                quotInd = find(tab, "\"");
                linerDesc = jsonget(configJobj, "linerDesc", "string", "");

                if(quotInd <> -1) {
                  tab = substring(tab, 0, quotInd);
                }
                isLiner = false;
          
                if(installationMethod == "Liner System") {
                  isLiner = true;

                }  
                returnString = returnString + lineDocNum + "~partType_l~" + partType + "|"; // added Spt 3, 2018
                returnString = returnString + lineDocNum + "~isLinerPart_l~" + string(isLiner) + "|"; // added Spt 3, 2018
                returnString = returnString + lineDocNum + "~linerDesc_l~" + linerDesc + "|"; // added Spt 3, 2018
                returnString = returnString + lineDocNum + "~description2_l~" + desc2 + "|";
                returnString = returnString + lineDocNum + "~section_l~" + section + "|";
                returnString = returnString + lineDocNum + "~sectionLabel_l~" + sectionLabel + "|";
                returnString = returnString + lineDocNum + "~tab_l~" + tab + "|";                //added new 
                returnString = returnString + lineDocNum + "~tabOption_l~" + tabOption + "|";
                returnString = returnString + lineDocNum + "~tabType_l~" + tabType + "|";
                returnString = returnString + lineDocNum + "~numRolls_l~" + numRolls + "|";
                returnString = returnString + lineDocNum + "~rollLength_l~" + rollLength + "|";
                returnString = returnString + lineDocNum + "~rollWidth_l~" + rollWidth + "|";
                returnString = returnString + lineDocNum + "~buildingLabel_l~" + buildLabel + "|";
                returnString = returnString + lineDocNum + "~isConfiguredPart_l~" + string(true) + "|";
                returnString = returnString + lineDocNum + "~insulationType_l~" + installationMethod + "|";
                returnString = returnString + lineDocNum + "~facing_l~" + facing + "|";
                returnString = returnString + lineDocNum + "~rollPlacementId_l~" + placeId + "|";
                returnString = returnString + lineDocNum + "~lineComment_l~" + lineAdditionalMBIComments + "|";
        returnString = returnString + lineDocNum + "~door_l~" + string(door) + "|";

             }
             else {
              lineIntegrationPartNumber = "";
             }
          }         

          returnString = returnString + lineDocNum + "~integrationPartNumber_l~" + lineIntegrationPartNumber + "|";
          returnString = returnString + lineDocNum + "~lineNumber_l~" + lineNumber + "|";
          returnString = returnString + lineDocNum + "~lineCustomSequenceNumber_l~" + lineSeqStr + "|";
          returnString = returnString + lineDocNum + "~configurationDataJSON_l~" + configData + "|";
          returnString = returnString + lineDocNum + "~partDescScrubbed_l~" + util.scrubXmlSoapInput(lineDesc) + "|";
          returnString = returnString + lineDocNum + "~partDescForOutputDoc_l~" + replace(lineDesc, " x ", "X") + "|";          
          
    }  // end line_default


    //liner pricing set up
    if(isLiner){
      lineKey = "";
      lineKeys = split(lineNumber, ".");
      if(sizeofarray(lineKeys)>2){
        //x.x.x format, meaning its insulation
        lineKey = lineKeys[0] + "." + lineKeys[1];
      }
      else{
        lineKey = lineNumber;
      }
      configJobj = json(configData);
      linerDetailJObj = jsonget(linerPricingJObj, lineKey, "json", json());
      if(partType=="Liner"){
        //add this entries docnum
        jsonput(linerDetailJObj, "mainDoc", line._document_number);
        jsonput(linerDetailJObj, "rValue", jsonget(configJObj, "rValueCombined", "string", ""));
        //just use partnum instead of type, color, and option
        jsonput(linerDetailJObj, "liner", line._part_display_number);
        totalLinerSqFt = totalLinerSqFt + line.requestedQuantity_l;
      }
      elif(partType =="Insulation"){
        layerKey = "";
        layerKeys = split(line.section_l, "-");
        if(sizeofarray(layerKeys)>1){
          layerKey = layerKeys[1];
        }
        jsonput(linerDetailJObj, layerKey, thickness);

        //insulation doc nums
        insDocs = jsonget(linerDetailJObj, "insDocs", "jsonarray", jsonarray());
        jsonarrayappend(insDocs, line._document_number);
        jsonput(linerDetailJObj, "insDocs", insDocs);
      }
      jsonput(linerPricingJObj, lineKey, linerDetailJObj);
    }
    //
    linerNonAccessory = (isLiner AND find(lower(partType), "accessories")==-1);


    //if(lower(action) == "line_default" OR lower(action) == "save") { // set up xml for pricing
    if(saveAction <> -1) { // set up xml for pricing  
       print "SETTING UP XML";
        //skip F and FS lines as these will be priced separtaely
        if(lineIntegrationPartNumber <> "" and len(lineActualUOM) > 0 AND NOT(lineDisplayNumber=="F" OR lineDisplayNumber=="FS" OR linerNonAccessory)) {     
            pricingXml = pricingXml + "<items>";
            pricingXml = pricingXml + "<actualUOM>" + lineActualUOM + "</actualUOM>";
            pricingXml = pricingXml + "<itemNumber>" + lineIntegrationPartNumber + "</itemNumber>"
                         + "<lineNumber>" + lineDocNum + "</lineNumber>"
                         + "<quantity>" + string(lineQty) + "</quantity>" 
                         + "</items>";
        }    
    }
    //always update regardless of line default
    returnString = returnString + lineDocNum + "~isCreditLine_l~" + string(isCreditLine) + "|";

    jsonput(LineObj, "IntegrationPartNumber", lineIntegrationPartNumber);
    jsonput(LineObj, "PartNumber", linePartNumber);
    jsonput(LineObj, "Label", lineModelName);
    jsonput(lineDocJObj, lineDocNum, LineObj);
} // end for loop - line iteration

// Set line items added flag
if (NOT lineItemsAdded_t AND lineItemCount > 0) {
  returnString = returnString + "1~lineItemsAdded_t~true|";
}

//get liner values and price
//figure out sq foot breakpoint 0-5k, 5001-10k, etc
linerPriceCol = "Price0to5k";
if(totalLinerSqFt>5000 AND totalLinerSqFt<10001){
  linerPriceCol = "Price5to10k";
}
elif(totalLinerSqFt>10000 AND totalLinerSqFt<20001){
  linerPriceCol = "Price10to20k";
}
elif(totalLinerSqFt>20000 AND totalLinerSqFt<40001){
  linerPriceCol = "Price20to40k";
}
elif(totalLinerSqFt>40000 AND totalLinerSqFt<50001){
  linerPriceCol = "Price40to50k";
}
elif(totalLinerSqFt>50000){
  linerPriceCol = "Price50kUp";
}
linerPriceColSelect =  linerPriceCol + ", Cost, CustomerID";

//go through each key, form a call individually.  Could be bulked together but may be marginal benefit performance-wise
linerObjKeys = jsonkeys(linerPricingJObj);
linerPriceStr="";
for linerObjKey in linerObjKeys{
    linerDetailJObj = jsonget(linerPricingJObj, linerObjKey, "json", json());
    thickness1=0.0;
    thickness2=0.0;
    linerPartNum = jsonget(linerDetailJObj, "liner", "string", "");
    rValue = jsonget(linerDetailJObj, "rValue", "string", "");
    
    //get top layer or layer 1
    thicknessStr = jsonget(linerDetailJObj, "Top Layer", "string", "");
    if(thicknessStr==""){
      thicknessStr = jsonget(linerDetailJObj, "layer1", "string", "");
    }
    if(thicknessStr<>"" AND isnumber(thicknessStr)){
      thickness1= atof(thicknessStr);
    }

    //get bottom layer or layer 2
    thicknessStr = jsonget(linerDetailJObj, "Bottom Layer", "string", "");
    if(thicknessStr==""){
      thicknessStr = jsonget(linerDetailJObj, "layer2", "string", "");
    }
    if(thicknessStr<>"" AND isnumber(thicknessStr)){
      thickness2= atof(thicknessStr);
    }
    custID = shipToPartyID_t; //don't do anything with this for now

    //make data table call with parameters to get the proper price
    rs = RecordSet();
    if(rValue==""){
      //we want the null version (fabric only) in this case
      rs=BMQL("SELECT $linerPriceColSelect FROM LinerPricingKeys INNER JOIN LinerPricing ON LinerPricingKeys.LinerPricingKey = LinerPricing.LinerPricingKey WHERE ((LinerPricing.Thickness1 = $thickness1 AND LinerPricing.Thickness2 = $thickness2) OR (LinerPricing.Thickness1 = $thickness2 AND LinerPricing.Thickness2 = $thickness1)) AND LinerPricingKeys.LinerPartNum = $linerPartNum AND LinerPricing.RVal IS NULL AND (LinerPricing.CustomerID = $custID OR LinerPricing.CustomerID IS NULL)");
    }
    else{
      rs=BMQL("SELECT $linerPriceColSelect FROM LinerPricingKeys INNER JOIN LinerPricing ON LinerPricingKeys.LinerPricingKey = LinerPricing.LinerPricingKey WHERE ((LinerPricing.Thickness1 = $thickness1 AND LinerPricing.Thickness2 = $thickness2) OR (LinerPricing.Thickness1 = $thickness2 AND LinerPricing.Thickness2 = $thickness1)) AND LinerPricingKeys.LinerPartNum = $linerPartNum AND LinerPricing.RVal = $rValue AND (LinerPricing.CustomerID = $custID OR LinerPricing.CustomerID IS NULL)");
    }

    if(DEBUG){
      print "---------------";
      print "CALL PARAMS";
      print linerDetailJObj;
      print linerPartNum;
      print rValue;
      print thickness1;
      print thickness2;
      print "RS:";
      print rs;
    }
    //process results
    linePrice = "0.0";
    lineCost = "0.0";  //not actually applied right now
    priceType = "list"; //will prefer customer if found
    priceMissing=true;
    for rec in rs{
      priceMissing=false;
      cust = get(rec, "CustomerID");

      if(cust<>""){
        priceType="cust";
        linePrice = get(rec, linerPriceCol);
        lineCost = get(rec, "Cost");
      }
      if(priceType=="list"){
        //only grab these if cust hasn't been found.  Either cust is found and then doesn't get overwritten OR this is set first and then cust will overwrite it
        linePrice = get(rec, linerPriceCol);
        lineCost = get(rec, "Cost");
      }
    }
    //ONLY IF price found should the line(s) be changed
    if(DEBUG){
      print "LINER PRICES MISSING: " + string(priceMissing);
      print "TYPE: " + priceType;
      print linePrice;
      print lineCost;
    }

    if(priceMissing OR linePrice=="0.0"){
      //set missing price flag
      missingPrice=true;
    }
    //get all the insulation docs to zero out
    insDocs = jsonget(linerDetailJObj, "insDocs", "jsonarray", jsonarray());
    insDocRange = range(jsonarraysize(insDocs));
    for insDocIdx in insDocRange{
      insDoc = jsonarrayget(insDocs, insDocIdx, "string");
      linerPriceStr = linerPriceStr + insDoc + "~listPrice_l~0.0|" + insDoc + "~priceAppliedType_l~" + priceType + "|" + insDoc + "~basePrice_l~0.0|" + insDoc + "~missingPrice_l~" + string(priceMissing) + "|" ;// + insDoc + "~_part_custom_field4~0.0|";
    }
    //get the liner itself and price
    linerDoc = jsonget(linerDetailJObj, "mainDoc", "string", "");
    linerPriceStr = linerPriceStr + linerDoc + "~listPrice_l~" + linePrice + "|" 
      + linerDoc + "~priceAppliedType_l~" + priceType + "|" 
      + linerDoc + "~basePrice_l~" + linePrice + "|"
      + linerDoc + "~missingPrice_l~" + string(priceMissing) + "|"; 
      //+ linerDoc + "~_part_custom_field4~" + lineCost +"|";
}
returnString = returnString + linerPriceStr;


//pricing Call
soapRequest = "";
response = "";
if(len(pricingXml) > 0) {
    print "LEN PRICING XML: " + string(len(pricingXml));
    envVars = String[]{"jdeUsername", "jdePassword"};
    envVarDict = util.getEnvironmentVariableValues(_system_supplier_company_name, envVars);
    templateFile = "$BASE_PATH$/Templates/pricing.xml";
    templateDict = dict("string");
    put(templateDict, "Username", get(envVarDict, "jdeUsername"));
    put(templateDict, "Password", get(envVarDict, "jdePassword"));
    put(templateDict, "RequestId", _system_buyside_id);
    put(templateDict, "ShipToId", shipToPartyID_t);
    put(templateDict, "BusinessUnit", businessUnit_t);
    put(templateDict, "PricingLines", pricingXml);
    soapRequest = applytemplate(templateFile, templateDict);
    
    soapRequest = replace(soapRequest, "&lt;", "<");
    soapRequest = replace(soapRequest, "&gt;", ">");
    //print soapRequest;
    response = util.sendAPIRequest("endPoint_Price", "POST", soapRequest);    
    if(len(response) > 0 and not startswith(response, "ERROR")) {
      params = json();      
      jsonput(params, "missingPrice", missingPrice_t);
      pricedValuesJson = util.setPricedValues(response, params);

      returnString = returnString + jsonget(pricedValuesJson, "returnString", "string", "");
      missingPrice = (missingPrice OR jsonget(pricedValuesJson,"missingPrice", "boolean", false));
      // Send notification to inside sales if missing price identified
    }
    //print response;
}
if (missingPrice) {
  if (isPartnerUser_t) {
    warningMessage = warningMessage + "\tThere are missing prices on your quote. Please click on 'Submit Proposal' to notify the Service Partners team and they will reach out to you shortly.\t";
  } else {
    warningMessage = warningMessage + "\tThere are missing prices on your quote.\t";
  }        
}

// end pricing call

//process lines
displayPartsJList = jsonpathgetmultiple(lineDocJObj, "$.*[?(@.LineType=='CONFIG_PART' || @.LineType=='PART')].IntegrationPartNumber");
displayPartsJListAgnostic = jsonpathgetmultiple(lineDocJObj, "$.*[?((@.LineType=='CONFIG_PART' || @.LineType=='PART') && @.BranchAgnostic=='true')].IntegrationPartNumber");

displayPartsList = util.jsonStringArrayToArray(displayPartsJList);
displayPartsListAgnostic = util.jsonStringArrayToArray(displayPartsJListAgnostic);

partsList = util.prependBUForPartNumbers(businessUnit_t, displayPartsList); // part Numbers have Business Unit code prepended in db
partsListAgnostic = util.prependBUForPartNumbers("0000", displayPartsListAgnostic); //these get 0000 prefixed to be more flexible
fullPartsList = util.mergeStringArrays(partsList, partsListAgnostic);


partsDetailObj  = util.listPartDetails(fullPartsList);


validParts = jsonkeys(partsDetailObj);

docList = jsonkeys(lineDocJObj);
configPartNotInDB = false;

//for refresh parts we simply want to return partExistsIN DB
if(refreshPartsAction){
    returnString = "";
}


for doc in docList { 
    docObj = jsonget(lineDocJObj, doc, "json");
    docType = jsonget(docObj, "LineType");
    branchAgnostic = (jsonget(docObj, "BranchAgnostic", "string", "false")=="true");
    docPartNumber = jsonget(docObj, "PartNumber");
    intPartNumber = jsonget(docObj, "IntegrationPartNumber");
    buPartNumber = businessUnit_t + "-" + intPartNumber;
    if(branchAgnostic){
      buPartNumber = "0000-" + intPartNumber;
    }
    
    mVarName = jsonget(docObj, "ModelVarName");
    parentDocNumber = jsonget(docObj, "ParentDocNumber");
    
    if(docType == "CONFIG_PART" OR docType == "PART") {
        if(findinarray(validParts , buPartNumber) <> -1 OR intPartNumber == "F") {
            returnString = returnString + doc + "~partExistsInDB_l~" + "true" + "|";
        }
        else {
            configPartNotInDB = true;
            returnString = returnString + doc + "~partExistsInDB_l~" + "false" + "|";
        }
    }
    if(NOT refreshPartsAction){
      if(mVarName == "newPart") {
            returnString = returnString + doc + "~_part_display_number~" + intPartNumber + "|";    
      //      returnString = returnString + doc + "~_part_number~" + intPartNumber + "|";
      //      returnString = returnString + doc + "~_model_name~" + intPartNumber + "|";
      }

      if (lineDefaultAction <> -1 AND parentDocNumber == lastConfiguredModelDocNum) {
         // Set previously applied discounts on reconfigure action 
            partDiscount = jsonget(buildingDiscounts, intPartNumber, "json", json());
            discountType = jsonget(partDiscount, "discountType", "string", "");//jsonpathgetsingle(buildingDiscounts, "$." + lineDisplayNumber + ".discountType", "string", "");
            discountValue = jsonget(partDiscount, "discountValue", "float", 0.0);//jsonpathgetsingle(buildingDiscounts, "$." + lineDisplayNumber + ".discountValue", "float", 0.0);
            accessoryQtyOverride = jsonget(partDiscount, "accessoryQtyOverride", "integer", 0);//jsonpathgetsingle(buildingDiscounts, "$." + lineDisplayNumber + ".accessoryQtyOverride", "integer", 0);
            
            // Set discount type
            if (discountType <> "") {          
              returnString = returnString + doc + "~customDiscountType_l~" + discountType + "|";            
            }

            // Set discount value
            if (discountValue <> 0) {            
              if (discountType == "Amount Off") {
                returnString = returnString + doc + "~customDiscountAmount_l~" + string(discountValue) + "|";                
              }                                                         
              returnString = returnString + doc + "~customDiscountValue_l~" + string(discountValue) + "|";            
            }

            // Set accessoryQtyOverride
            if (accessoryQtyOverride <> 0) {            
              returnString = returnString + doc + "~accessoryQtyOverride_l~" + string(accessoryQtyOverride) + "|";            
            }   
      }
    }
}

if(refreshPartsAction){
  return returnString;
}

//check to see if FS needs adjusting
if(FSDocNum<>""){
  PRICE_OVERRIDE_DISC_TYPE = "Price Override";    
  FSValStr = "0.0";  

  // Retreive freight surcharge 
  queryClause = string[]{"DEFAULT", shipToPartyID_t};
  freightSurchargeDict = bmql("SELECT * FROM FreightSurcharge WHERE CustomerID IN $queryClause");  

  for freightSurcharge in freightSurchargeDict {
    customer = get(freightSurcharge, "CustomerID");
    freightSurchargeVal = get(freightSurcharge, "FreightSurcharge");
    if (NOT isnull(customer)){
      FSValStr = freightSurchargeVal;
    }
  }

  // Check if FS value overriden by user.
  if (string(FSCurrentVal) <> FSValStr AND (NOT (fetchPriceAction) OR _system_user_name == "CpqApiUser")) {
    FSValStr = string(FSCurrentVal);
  }
  
  returnString = returnString + "1~fSDocNum_t~" + FSDocNum + "|" + FSDocNum + "~customDiscountValue_l~" + FSValStr + "|" + FSDocNum + "~customDiscountType_l~" + PRICE_OVERRIDE_DISC_TYPE + "|1~fuelSurcharge_t~" + FSValStr + "|";
}
returnString = returnString + "1~freight_t~" + string(freightVal) + "|";

returnString = returnString + "1" + "~lastConfiguredModelDocNum_t~" + lastConfiguredModelDocNum + "|";
if(configPartNotInDB) {
      warningMessage = warningMessage + "One or more configured Parts(in red) do not exist in Database. These will need to be created before the quote can be ordered.\t";
}
returnString = returnString + "1" + "~warningMessages_t~" + warningMessage  + "|"; 
returnString = returnString + "1" + "~jdeFetchPriceRequestPayload_t~" + soapRequest  + "|"; 
returnString = returnString + "1" + "~jdeFetchPriceResponsePayload_t~" + response  + "|"; 

return returnString;
```

## Revision History
 Rev. Date |Developer            |Notes / Comments
 ----------|---------------------|-------------------------------------------------------------------------
 07/21/17  |rlamsal              |Initial version
 2019-12-12|Jserna               |SP-59 Changed lineSeqStr determination
 2020-01-06|Jserna               |SP-12 Identify credit lines
 2020-01-09|Jserna               |SP-68 Populate lineComment with cut/slit detail
 2020-01-13|Jserna               |SP-52 Use MODEL BU Desc for items not set up in a BU
 2020-01-20|Jserna               |Set lines added flag
 2020-01-23|Jserna               |SP-74 Lock pricing on reconfigure
 2020-03-12|shansen              |SP-131 Updating Fuel surcharge
 2020-03-16|shansen              |Skipping F & FS lines for pricing
 2020-05-01|shansen              |changed default of configPartNumber to "NOT-FOUND" instead of "NOT FOUND" to prevent invalid jsonpath errors
 2020-05-05|shansen              |added collection of branch agnostic parts to check for existence in DB, such as credit lines (0000-CCMBI)
 2020-05-05|shansen              |conditioned to ONLY return partExistsInDB info for refresh parts action 
 2020-05-05|shansen              |conditioned to price liner accessories still
 2020-05-05|shansen              |missing liner prices bug fix, will now consider non-existent liner pricing as missing properly
 2020-12-17|Jserna               |setting discountType of freight surcharge line to price override
 2020-12-22|Jserna               |customer specific freight surcharges
