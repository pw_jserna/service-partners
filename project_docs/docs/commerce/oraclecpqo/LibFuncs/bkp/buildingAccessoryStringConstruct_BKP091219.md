# Building Accessory String Construct
Returns the code delimited string to form the Accessory Table under the Quote Summary table of the Quote Summary output Document.


## Syntax
```
buildingAccessoryStringConstruct();
```

## Returns
Type|Description
-|-
```String```|


## Attributes
Name|Description
-|-
```_part_custom_field15```|
```isLinerPart_l```|
```requestedQuantity_l```|
```section_l```|


## Source
```Java
retStr = "";
accessoriesQuantityTotalDict = dict("integer");
accessoriesUOMDict = dict("string");

for buildingLine in transactionLine {	
    if(startswith(buildingLine.section_l, "AccessoryPart")) { 
        //For Section=Accessories.
        dashPos = find (buildingLine.section_l, "-");
        accessoryKey = substring(buildingLine.section_l, dashPos+1);
        if(startswith(upper(accessoryKey), "DOUBLE FACE TAPE") OR startswith(upper(accessoryKey), "DOUBLE FACED TAPE")) {
            accessoryKey = "Double Faced Tape";
        }
        //print "accessoryKey: " + "\"" + accessoryKey + "\"";
        // Quantity:
        quantity = 0;
        if(containskey(accessoriesQuantityTotalDict, accessoryKey)) {
            quantity = get(accessoriesQuantityTotalDict, accessoryKey);
        }
        quantity = quantity + buildingLine.requestedQuantity_l;
        put(accessoriesQuantityTotalDict, accessoryKey, quantity);
        
        // Quantity UOM:
        if(NOT(containskey(accessoriesUOMDict, accessoryKey))) {
            uom = "";
            if(buildingLine._part_custom_field15 <> "" AND NOT(isnull(buildingLine._part_custom_field15))) {
                uom = buildingLine._part_custom_field15;
            }
            put(accessoriesUOMDict, accessoryKey, uom);
        }
    // new sept 23 2019
    } elif(buildingLine.isLinerPart_l AND (startswith(upper(buildingLine.section_l), "ROOF-ACCESSORIES") OR startswith(upper(buildingLine.section_l), "WALLS-ACCESSORIES"))) {
        //For Section= Liner Accessories.
        dashPos = find(buildingLine.section_l, "-");
        accessoryKey = substring(buildingLine.section_l, dashPos+1);
        if(startswith(upper(accessoryKey), "ACCESSORIES-")) {
            accessoryKey = substring(accessoryKey, len("ACCESSORIES-"));
        }
        if(startswith(upper(accessoryKey), "DOUBLE FACE TAPE") OR startswith(upper(accessoryKey), "DOUBLE FACED TAPE")) {
            accessoryKey = "Double Faced Tape";
        }
        // Quantity:
        quantity = 0;
        if(containskey(accessoriesQuantityTotalDict, accessoryKey)) {
            quantity = get(accessoriesQuantityTotalDict, accessoryKey);
        }
        quantity = quantity + buildingLine.requestedQuantity_l;
        put(accessoriesQuantityTotalDict, accessoryKey, quantity);
        
        // Quantity UOM:
        if(NOT(containskey(accessoriesUOMDict, accessoryKey))) {
            uom = "";
            if(buildingLine._part_custom_field15 <> "" AND NOT(isnull(buildingLine._part_custom_field15))) {
                uom = buildingLine._part_custom_field15;
            }
            put(accessoriesUOMDict, accessoryKey, uom);
        }
    }
}
    
//print "accessoriesQuantityTotalDict: "; print accessoriesQuantityTotalDict;   
    
rowDelim = "#@#";
colDelim = "#$#";

accKeys = keys(accessoriesQuantityTotalDict);
for eachAcc in accKeys {
    accessories = eachAcc;
    if(get(accessoriesUOMDict, eachAcc) <> "") {
        accessories = accessories + "/" + get(accessoriesUOMDict, eachAcc);
    }
    retStr = retStr + accessories + colDelim + string(get(accessoriesQuantityTotalDict,eachAcc)) + colDelim;
    retStr = retStr + rowDelim;
}

return retStr;
```

## Revision History
Rev. Date |Developer            |Notes / Comments
----------|---------------------|-------------------------------------------------------------------------
2019-11-14|jonathan.serna@piercewashington.com|
