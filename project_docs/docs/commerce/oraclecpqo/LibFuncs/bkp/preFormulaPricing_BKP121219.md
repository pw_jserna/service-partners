# Pre Formula Pricing
pricing Script sets base line level attribute values by iterating through the lines in the transaction. These values are used by the formulas to set other line and quote level attributes.


## Syntax
```
preFormulaPricing(action);
```

## Returns
Type|Description
-|-
```String```|

## Parameters
Name|Type|Description
-|-|-
```action```|```String```|

## Attributes
Name|Description
-|-
```businessUnit_t```|
```placementDiagramFile_t```|
```configPlacementDiagram_t```|
```shipToPartyID_t```|
```missingPrice_t```|
```isPartnerUser_t```|
```_document_number```|
```_part_number```|
```listPrice_l```|
```_line_item_comment```|
```_line_item_spare_rule_var_name```|
```_config_attr_info```|
```_parent_doc_number```|
```_model_variable_name```|
```_price_list_price_each```|
```_part_display_number```|
```integrationPartNumber_l```|
```_model_name```|
```lineNumber_l```|
```_part_custom_field15```|
```requestedQuantity_l```|
```partDescScrubbed_l```|
```_part_desc```|
```configPacementDiagram_l```|
```_system_buyside_id```|
```_system_supplier_company_name```|

## Functions
Type|Function Signature
-|-
```Json```|[listPartDetails(String[] partsList)](listPartDetails)
```Json```|[sendEmail(Json params)](sendEmail)
```Json```|[setPricedValues(String priceXMLData, Json params)](setPricedValues)
```String```|[Dictionary executeCommerceSoapApi(String soapMessage, String companyName)](Dictionary executeCommerceSoapApi)
```String```|[Dictionary getEnvironmentVariableValues(String siteName, String[] variables)](Dictionary getEnvironmentVariableValues)
```String```|[getConfigurationAttributeValue(String configData, String attrName)](getConfigurationAttributeValue)
```String```|[getLineType(String modelVarName, String parentDocNumber, String partNumber)](getLineType)
```String```|[scrubXmlSoapInput(String input)](scrubXmlSoapInput)
```String```|[sendAPIRequest(String urlType, String method, String requestBody)](sendAPIRequest)
```String[]```|[jsonStringArrayToArray(JsonArray jList)](jsonStringArrayToArray)
```String[]```|[prependBUForPartNumbers(String businessUnit, String[] displayPartsList)](prependBUForPartNumbers)

## Source
```Java
//************************************************************************************************************

// Initialize variables

returnString = "";

warningMessage = "";
warnList = String[];

//lineCount = 0;

//sectionDetailsJObj = json();
//lineDetailsJObj = json();
lineSequenceNumber = 0;
//newPartsList = String[];

lineType = "";

lineDocJObj = json();

lastConfiguredModelDocNum = "";

pricingXml = "";

actionArr = split(lower(action), "~");
lineDefaultAction = findinarray(actionArr, "line_default");
saveAction = findinarray(actionArr, "save");

// Loop through new lines being added
for line in transactionLine {
    lineNumber = "";
    configData = "";
    isConfigPart = false;
    

    lineDocNum = line._document_number;
    linePartNumber = line._part_number;
    lineDisplayNumber = line._part_display_number;
    lineParentDocNumber = line._parent_doc_number;
    lineModelVarName = line._model_variable_name;
    lineComment = line._line_item_comment;
    lineIntegrationPartNumber = line.integrationPartNumber_l;
    lineModelName = line._model_name;
    lineActualUOM = line._part_custom_field15;
    lineQty = line.requestedQuantity_l;
    lineDesc = line._part_desc;


    lineType = util.getLineType(lineModelVarName, lineParentDocNumber, linePartNumber); // CONFIG_ROOT, CONFIG_PART, CONFIG_MODEL, PART
    
    LineObj = json();
    jsonput(LineObj, "LineType", lineType);
    jsonput(LineObj, "DocumentNumber", lineDocNum);
    jsonput(LineObj, "ParentDocNumber", lineParentDocNumber);
    jsonput(LineObj, "ModelName", lineModelName);
    jsonput(LineObj, "ModelVarName", lineModelVarName);
    

    if(lineDefaultAction <> -1) {  // set up lines
          lineSeqStr = "";
          lineIntegrationPartNumber = lineDisplayNumber;
	 print "PART NUMBER: " + lineIntegrationPartNumber;
          if(lineType == "CONFIG_ROOT" or lineType == "PART") {
             lineSequenceNumber = lineSequenceNumber + 1;
             print "LINE SEQ NU " + string(lineSequenceNumber);
             lineSeqStr = string(lineSequenceNumber);
             lineNumber = string(lineSequenceNumber);
             jsonput(LineObj, "SequenceNumber", lineSequenceNumber);
             if(lineType == "CONFIG_ROOT") { // building node
                // Set modelPlacementDiagram flag
                if (line.configPacementDiagram_l <> ""){
                  returnString = returnString + lineDocNum + "~modelPlacementDiagram_l~true|";
                }
             	  lineIntegrationPartNumber = "";
                configData = util.getConfigurationAttributeValue(line._config_attr_info, "buildingInsulationConfigJSON");
                buildingJson = json(configdata);
                buildingLabel = jsonget(buildingJson, "Label", "string", "Building");
                splitType = jsonpathgetsingle(buildingJson,"$.Roof.SplitRolls","string", ""); 
                lineModelName = buildingLabel;
                returnString = returnString + lineDocNum + "~_model_name~" + upper(buildingLabel) + "|"; 
                returnString = returnString + lineDocNum + "~_part_display_number~" + upper(buildingLabel) + "|";
                //returnString = returnString + lineDocNum + "~_part_number~" + upper(buildingLabel) + "|";
                returnString = returnString + lineDocNum + "~modelPartNumber_l~" + upper(buildingLabel) + "|";
                returnString = returnString + lineDocNum + "~configurationID_l~" + jsonget(buildingJson, "configID", "string", "") + "|";
                returnString = returnString + lineDocNum + "~buildingWidth_l~" + jsonget(buildingJson, "Width", "string", "") + "|";
                returnString = returnString + lineDocNum + "~buildingHeight_l~" + jsonget(buildingJson, "Height", "string", "") + "|";
                returnString = returnString + lineDocNum + "~buildingLength_l~" + jsonget(buildingJson, "Length", "string", "") + "|";
                returnString = returnString + lineDocNum + "~buildingRoofType_l~" + jsonget(buildingJson, "RoofType", "string", "") + "|";
                returnString = returnString + lineDocNum + "~buildingPitch_l~" + jsonget(buildingJson, "Pitch1", "string", "") + "|";
                returnString = returnString + lineDocNum + "~buildingLabel_l~" + jsonget(buildingJson, "Label", "string", "") + "|";
                returnString = returnString + lineDocNum + "~buildingSplitRoofRolls_l~" + splitType + "|";
                attachData = util.getConfigurationAttributeValue(line._config_attr_info, "placementDiagramContent");
                if(len(line.lineNumber_l) == 0) {  
                        lastConfiguredModelDocNum = lineDocNum;
                }
             }
             
          }
          else { // config_model or config_part 
             if(lineModelVarName == "accessories") {
                configData = "{}";
             }
             else {
                configData = lineComment;
             }
             configJobj = json(configData);

             parentObj = jsonget(lineDocJObj, lineParentDocNumber, "json");
             parentSeq = jsonget(parentObj, "SequenceNumber", "integer");
             print "PARENT SEQ: " + string(parentSeq);
             lineIndex = line._line_item_spare_rule_var_name;
             lineIndex = replace(lineIndex, ".", "");
             print "LINE INDEX: " + lineIndex;
             lineSeqStr = string(parentSeq) + "." + lineIndex;
             print "LINE SEQ STR: " + lineSeqStr;
             lineNumber = "  " + string(parentSeq) + "." + line._line_item_spare_rule_var_name;
		print "FORMATTED LINE NUM: " + lineNumber;
             if(lineModelVarName <> "" and lineModelVarName <> "accessories") {
                modelType = jsonget(configJobj, "section", "string", "");
                modelName = "";
                modelDesc = ""; 
                if(lineModelVarName == "newPart") { // new part not existing in db
                    if(modelType == "AccessoryPart") {
                       modelDesc = jsonget(configJobj, "type", "string", "") ;
                    }
                    else {
                      width = jsonget(configJobj, "rollWidth", "integer", 0);
                      widthInches = width * 12;
                      rollType = jsonget(configJobj, "type", "string", "");
                      rollRVal = jsonget(configJobj, "rvalue", "string", "");
                      thickness = jsonget(configJobj, "thickness", "string", "");
                      slit = jsonget(configJobj, "slit", "integer", 0);
                      if(slit > 0) {
                      	widthInches = slit;
                      }
                      if(rollType == "Top Layer") {
                        thickness = jsonget(configJobj, "thicknessTop", "string", "");
                        rollRVal = jsonget(configJobj, "rvalueTop", "string", "");
                      }
                      elif(rollType == "Bottom Layer") {
                        thickness = jsonget(configJobj, "thicknessBottom", "string", "");
                        rollRVal = jsonget(configJobj, "rvalueBottom", "string", "");
                      }
                      if(rollRVal <> "") {
                      	  modelDesc = rollRVal + " " + thickness + "\"X" + string(widthInches) + "\" " + jsonget(configJobj, "facing") + " FACED";
                      	  returnString = returnString + lineDocNum + "~_part_custom_field8~" + rollRVal + "|";
                      	  returnString = returnString + lineDocNum + "~_part_custom_field9~" + thickness + "|"; // new sept 23 2019, prev: jsonget(configJobj, "thickness")
	                      returnString = returnString + lineDocNum + "~_part_custom_field10~" + string(widthInches) + "|";
	                      returnString = returnString + lineDocNum + "~_part_custom_field15~" + "SF" + "|";
	                      returnString = returnString + lineDocNum + "~_part_custom_field11~" + "MS" + "|";
                      }
                      else {
                      	modelDesc = rollType;
                      }
                      
                    }

                    modelName = jsonget(json(configData), "partNumber", "string", "NOT FOUND");
                    lineIntegrationPartNumber = modelName;
                    linePartNumber = businessUnit_t + "-" + modelName;
                    
                }
                else { // models Roof , Walls
                    modelName = jsonget(configJobj, "Label", "string", "");
                    tabOpt = jsonget(configJobj, "TabOption", "string", "");
                    combinedRValue = jsonget(configJobj, "CombinedRValue", "string", "");
                    rVal = jsonget(configJobj, "RValue", "string", "");
                    modelDesc = replace(jsonget(configJobj, "InstallationMethod", "string", ""), "System", "");

                    if(tabOpt <> "") {
                     modelDesc = modelDesc + ", " + tabOpt; 
                    }
                    if(combinedRValue <> "") {
                    	modelDesc = modelDesc + ", Combined " + combinedRValue; 
                    }
                    elif(rVal <> "") {
                    	modelDesc = modelDesc + ", " + rVal;
                    }
                    returnString = returnString + lineDocNum + "~_part_display_number~" + upper(modelName) + "|";
                    
                }
                lineModelName = modelName;
                lineDesc = modelDesc;
                returnString = returnString + lineDocNum + "~_model_name~" + upper(modelName) + "|";
                returnString = returnString + lineDocNum + "~_part_desc~" + modelDesc + "|";    
             }
             if(lineModelVarName == "accessories") {
             	returnString = returnString + lineDocNum + "~_part_display_number~" + "Accessories" + "|";

             }

             if(lineType == "CONFIG_PART") {
                desc2 = "";
                partType = jsonget(configJobj, "partType", "string", "");
                numRolls = jsonget(configJobj, "numRolls", "string", "");
                rollLength = jsonget(configJobj, "rollLength", "string", ""); 
                rollWidth = jsonget(configJobj, "rollWidth", "string", ""); 

                section = jsonget(configJobj, "section", "string", "");
                sectionType = jsonget(configJobj, "type", "string", "");
                if(sectionType <> "") {
                  section = section + "-" + sectionType;
                }
                sectionLabel = jsonget(configJobj, "sectionLabel", "string", "");
                tab = jsonget(configJobj, "tab", "string", "");
                tabType = jsonget(configJobj, "tabType", "string", "");
                tabOption = jsonget(configJobj, "tab", "string", "");


                desc2 = jsonget(configJobj, "description", "string", "");
                buildLabel = jsonget(configJobj, "BuildingLabel", "string", "");
                placeId = jsonget(configJobj, "placementCode", "string", "");
                installationMethod = jsonget(configJobj, "installationMethod", "string", "");
                facing = jsonget(configJobj, "facing", "string", "");
                quotInd = find(tab, "\"");
                linerDesc = jsonget(configJobj, "linerDesc", "string", "");

                if(quotInd <> -1) {
                  tab = substring(tab, 0, quotInd);
                }
                isLiner = false;
          
                if(installationMethod == "Liner System") {
                	isLiner = true;

                }  
                returnString = returnString + lineDocNum + "~partType_l~" + partType + "|"; // added Spt 3, 2018
                returnString = returnString + lineDocNum + "~isLinerPart_l~" + string(isLiner) + "|"; // added Spt 3, 2018
                returnString = returnString + lineDocNum + "~linerDesc_l~" + linerDesc + "|"; // added Spt 3, 2018


                returnString = returnString + lineDocNum + "~description2_l~" + desc2 + "|";
                returnString = returnString + lineDocNum + "~section_l~" + section + "|";
                returnString = returnString + lineDocNum + "~sectionLabel_l~" + sectionLabel + "|";
                returnString = returnString + lineDocNum + "~tab_l~" + tab + "|";
                //added new 
                returnString = returnString + lineDocNum + "~tabOption_l~" + tabOption + "|";
                returnString = returnString + lineDocNum + "~tabType_l~" + tabType + "|";

                returnString = returnString + lineDocNum + "~numRolls_l~" + numRolls + "|";
                returnString = returnString + lineDocNum + "~rollLength_l~" + rollLength + "|";
                returnString = returnString + lineDocNum + "~rollWidth_l~" + rollWidth + "|";
                returnString = returnString + lineDocNum + "~buildingLabel_l~" + buildLabel + "|";
                returnString = returnString + lineDocNum + "~isConfiguredPart_l~" + string(true) + "|";
                returnString = returnString + lineDocNum + "~insulationType_l~" + installationMethod + "|";
                returnString = returnString + lineDocNum + "~facing_l~" + facing + "|";
                returnString = returnString + lineDocNum + "~rollPlacementId_l~" + placeId + "|";


             }
             else {
             	lineIntegrationPartNumber = "";
             }
          }
          returnString = returnString + lineDocNum + "~integrationPartNumber_l~" + lineIntegrationPartNumber + "|";
          returnString = returnString + lineDocNum + "~lineNumber_l~" + lineNumber + "|";
          returnString = returnString + lineDocNum + "~lineCustomSequenceNumber_l~" + lineSeqStr + "|";
          returnString = returnString + lineDocNum + "~configurationDataJSON_l~" + configData + "|";
          returnString = returnString + lineDocNum + "~partDescScrubbed_l~" + util.scrubXmlSoapInput(lineDesc) + "|";
          returnString = returnString + lineDocNum + "~partDescForOutputDoc_l~" + replace(lineDesc, " x ", "X") + "|";
          
    }  // end line_default
    //if(lower(action) == "line_default" OR lower(action) == "save") { // set up xml for pricing
    if(saveAction <> -1) { // set up xml for pricing  
       print "SETTING UP XML";
        if(lineIntegrationPartNumber <> "" and len(lineActualUOM) > 0) {     
            pricingXml = pricingXml + "<items>";
            pricingXml = pricingXml + "<actualUOM>" + lineActualUOM + "</actualUOM>";
            pricingXml = pricingXml + "<itemNumber>" + lineIntegrationPartNumber + "</itemNumber>"
                         + "<lineNumber>" + lineDocNum + "</lineNumber>"
                         + "<quantity>" + string(lineQty) + "</quantity>" 
                         + "</items>";
        }    
    }
    jsonput(LineObj, "IntegrationPartNumber", lineIntegrationPartNumber);
    jsonput(LineObj, "PartNumber", linePartNumber);
    jsonput(LineObj, "Label", lineModelName);
    jsonput(lineDocJObj, lineDocNum, LineObj);
} // end for loop - line iteration

//pricing Call
soapRequest = "";
response = "";
if(len(pricingXml) > 0) {
    print "LEN PRICING XML: " + string(len(pricingXml));
    envVars = String[]{"jdeUsername", "jdePassword"};
    envVarDict = util.getEnvironmentVariableValues(_system_supplier_company_name, envVars);
    templateFile = "$BASE_PATH$/Templates/pricing.xml";
    templateDict = dict("string");
    put(templateDict, "Username", get(envVarDict, "jdeUsername"));
    put(templateDict, "Password", get(envVarDict, "jdePassword"));
    put(templateDict, "RequestId", _system_buyside_id);
    put(templateDict, "ShipToId", shipToPartyID_t);
    put(templateDict, "BusinessUnit", businessUnit_t);
    put(templateDict, "PricingLines", pricingXml);
    soapRequest = applytemplate(templateFile, templateDict);
    
    soapRequest = replace(soapRequest, "&lt;", "<");
    soapRequest = replace(soapRequest, "&gt;", ">");
    //print soapRequest;
    response = util.sendAPIRequest("endPoint_Price", "POST", soapRequest);    
    if(len(response) > 0 and not startswith(response, "ERROR")) {
      params = json();      
      jsonput(params, "missingPrice", missingPrice_t);
    	pricedValuesJson = util.setPricedValues(response, params);

      returnString = returnString + jsonget(pricedValuesJson, "returnString", "string", "");
      missingPrice = jsonget(pricedValuesJson,"missingPrice", "boolean", false);
      // Send notification to inside sales if missing price identified
      if (missingPrice) {
        if (isPartnerUser_t) {
          warningMessage = warningMessage + "\tThere are missing prices on your quote. Please click on 'Submit Proposal' to notify the Service Partners team and they will reach out to you shortly.\t";
        } else {
          warningMessage = warningMessage + "\tThere are missing prices on your quote.";
        }        
      }
    }
    //print response;
}


// end pricing call

//process lines
displayPartsJList = jsonpathgetmultiple(lineDocJObj, "$.*[?(@.LineType=='CONFIG_PART' || @.LineType=='PART')].IntegrationPartNumber");
displayPartsList = util.jsonStringArrayToArray(displayPartsJList);
partsList = util.prependBUForPartNumbers(businessUnit_t, displayPartsList); // part Numbers have Business Unit code prepended in db

partsDetailObj  = util.listPartDetails(partsList);
validParts = jsonkeys(partsDetailObj);

docList = jsonkeys(lineDocJObj);
configPartNotInDB = false;
for doc in docList { 
    docObj = jsonget(lineDocJObj, doc, "json");
    docType = jsonget(docObj, "LineType");
    docPartNumber = jsonget(docObj, "PartNumber");
    intPartNumber = jsonget(docObj, "IntegrationPartNumber");
    buPartNumber = businessUnit_t + "-" + intPartNumber;
    mVarName = jsonget(docObj, "ModelVarName");

    if(docType == "CONFIG_PART" OR docType == "PART") {
        if(findinarray(validParts , buPartNumber) <> -1 OR intPartNumber == "F") {
            returnString = returnString + doc + "~partExistsInDB_l~" + "true" + "|";
        }
        else {
            configPartNotInDB = true;
            returnString = returnString + doc + "~partExistsInDB_l~" + "false" + "|";
        }
    }
    if(mVarName == "newPart") {
          returnString = returnString + doc + "~_part_display_number~" + intPartNumber + "|";
    //      returnString = returnString + doc + "~_part_number~" + intPartNumber + "|";
    //      returnString = returnString + doc + "~_model_name~" + intPartNumber + "|";
    }
}
returnString = returnString + "1" + "~lastConfiguredModelDocNum_t~" + lastConfiguredModelDocNum + "|";
if(configPartNotInDB) {
			warningMessage = warningMessage + "One or more configured Parts(in red) do not exist in Database. These will need to be created before the quote can be ordered.\t";
}
returnString = returnString + "1" + "~warningMessages_t~" + warningMessage  + "|"; 
returnString = returnString + "1" + "~jdeFetchPriceRequestPayload_t~" + soapRequest  + "|"; 
returnString = returnString + "1" + "~jdeFetchPriceResponsePayload_t~" + response  + "|"; 

return returnString;
```

## Revision History
 Rev. Date |Developer            |Notes / Comments
 ----------|---------------------|-------------------------------------------------------------------------
 07/21/17  |rlamsal              |Initial version
 2019-12-12|jonathan.serna@piercewashington.com|
