# Building Item Breakdown String Construct
Returns the code delimited string to form the Quote Summary table of the Quote Summary output Document


## Syntax
```
buildingItemBreakdownStringConstruct(inputDocNum);
```

## Returns
Type|Description
-|-
```String```|

## Parameters
Name|Type|Description
-|-|-
```inputDocNum```|```String```|param inputDocNum String

## Attributes
Name|Description
-|-
```currency_t```|
```_document_number```|
```_parent_doc_number```|
```_part_custom_field8```|
```_part_custom_field9```|
```_part_custom_field11```|
```_part_custom_field15```|
```listPrice_l```|
```netAmount_l```|
```netPrice_l```|
```requestedQuantity_l```|
```integrationPartNumber_l```|
```section_l```|
```tab_l```|
```buildingLabel_l```|
```_model_name```|
```_part_desc```|
```accessoryType_l```|
```facing_l```|
```insulationType_l```|
```_part_display_number```|
```isLinerPart_l```|
```linerDesc_l```|
```_model_variable_name```|
```tabType_l```|
```tabOption_l```|

## Functions
Type|Function Signature
-|-
```String```|[numberFormat(Float number, Integer places, String thousandSeparator, String decimalSeparator)](numberFormat)

## Source
```Java
retStr = "";

FORMATTED_QTY_PLACES = 0;
FORMATTED_QTY_THOU_SEP = ",";

rowDelim = "#@#";
colDelim = "#$#";
builidingDelim = "@!@";

miscQuantityTotalDict = dict("integer");
miscQuantityUOMDict = dict("string");
miscAmtTotalDict = dict("float");
miscDescriptionDict = dict("string");

for buildingLine in transactionLine {
	// Loop only for Buildings and Miscellaneous Parts
    //if((buildingLine._parent_doc_number == "" OR isnull(buildingLine._parent_doc_number)) AND upper(buildingLine._part_desc) <> "FREIGHT" AND (buildingLine.integrationPartNumber_l=="" OR isnull(buildingLine.integrationPartNumber_l)) AND buildingLine._model_variable_name=="building") {
    if((buildingLine._parent_doc_number == "" OR isnull(buildingLine._parent_doc_number)) AND upper(buildingLine._part_desc) <> "FREIGHT" AND buildingLine._model_variable_name=="building") {
		parentDocNum = buildingLine._document_number;
		buildingLabel = buildingLine.buildingLabel_l;
        buildingTotalPrice = 0.0;
        buildingTotalQty = 0;
        
        roofBottomQuantityTotalDict = dict("integer");
        roofBottomQuantityUOMDict = dict("string");
        roofBottomAmtTotalDict = dict("float");
        roofBottomInsulationTypeDict = dict("string");
        roofBottomFacingDict = dict("string");
        roofBottomTabTypeDict = dict("string"); // new sept 23 2019
        roofBottomTabOptionDict = dict("string"); // new sept 23 2019
        
        roofTopQuantityTotalDict = dict("integer");
        roofTopQuantityUOMDict = dict("string");
        roofTopAmtTotalDict = dict("float");
        roofTopInsulationTypeDict = dict("string");
        roofTopFacingDict = dict("string");
        
        linerRoofInsulationTypeStr = "";
        linerRoofQuantityTotal = 0;
        linerRoofPriceTotal = 0.0;
        linerRoofDescArr = string[];
        
        wallQuantityTotalDict = dict("integer");
        wallQuantityUOMDict = dict("string");
        wallAmtTotalDict = dict("float");
        wallInsulationTypeDict = dict("string");
        wallFacingDict = dict("string");
        wallTabTypeDict = dict("string"); // new nov 26 2019
        wallTabOptionDict = dict("string"); // new nov 26 2019
        
        linerWallInsulationTypeStr = ""; 
        linerWallQuantityTotal = 0;
        linerWallPriceTotal = 0.0;
        linerWallDescArr = string[];
        
        accessories = "";
        accessoriesQuantityTotalDict = dict("integer");
        accessoriesUOMDict = dict("string");
        accessoriesPriceTotal = 0.0;
        
        linerOptionalAccessories = "Additional Accessories: ";
        linerMandatoryAccessories = "Accessories included in system price:  Banding, Fasteners, Tape, and Adhesive";
        
        linerRoofAccessoryStr = "";
        linerRoofAccessoriesQuantityTotalDict = dict("integer");
        linerRoofAccessoriesUOMDict = dict("string");
        linerRoofAccessoriesPriceTotal = 0.0;
        
        linerWallAccessoryStr = "";
        linerWallAccessoriesQuantityTotalDict = dict("integer");
        linerWallAccessoriesUOMDict = dict("string");
        linerWallAccessoriesPriceTotal = 0.0;
        
        combinedWallsBool = false;
		
        for childLine in transactionLine {
			// Only for child items of the Building            
			if(childLine._parent_doc_number == parentDocNum) {
                
                // For the Roof, Walls and Accessories
                if(childLine.integrationPartNumber_l<>"" AND NOT(isnull(childLine.integrationPartNumber_l))) { 
                    
                    childTypeKey = "";
                    if(childLine._part_custom_field8 <> "") {                        
                        // If it is roll, then Thickness + R-Value + Tab
                        childTypeKey = childLine._part_custom_field9 + "_" + childLine._part_custom_field8 + "_" + childLine.tab_l;
                    }
                    // } else {
                        // // If it is liner, then Liner Description.
                        // childTypeKey = childLine.linerDesc_l;
                    // }
                    
                    if(NOT childLine.isLinerPart_l) {
                        if(startswith(upper(childLine.section_l), "ROOF")) {
                            if(childLine.section_l == "Roof-Top Layer") {
                                // For Section = Roof-Top Layer
                                // Quantity:
                                quantity = 0;
                                if(containskey(roofTopQuantityTotalDict, childTypeKey)) {
                                    quantity = get(roofTopQuantityTotalDict, childTypeKey);
                                }
                                quantity = quantity + childLine.requestedQuantity_l;
                                put(roofTopQuantityTotalDict, childTypeKey, quantity);
                                
                                // Quantity UOM:
                                if(NOT(containskey(roofTopQuantityUOMDict, childTypeKey))) {
                                    put(roofTopQuantityUOMDict, childTypeKey, childLine._part_custom_field15);
                                }
                                
                                // Total Net Amount:
                                amount = 0.0;
                                if(containskey(roofTopAmtTotalDict, childTypeKey)) {
                                    amount = get(roofTopAmtTotalDict, childTypeKey);
                                }
                                amount = amount + childLine.netAmount_l;
                                put(roofTopAmtTotalDict, childTypeKey, amount);
                                
                                // Insulation Type
                                if(NOT(containskey(roofTopInsulationTypeDict, childTypeKey))) {
                                    put(roofTopInsulationTypeDict, childTypeKey, childLine.insulationType_l);
                                }
                                
                                // Facing
                                if(NOT(containskey(roofTopFacingDict, childTypeKey))) {
                                    put(roofTopFacingDict, childTypeKey, childLine.facing_l);
                                }
                                
                            } //elif(childLine.section_l == "Roof-Bottom Layer" OR childLine.section_l == "Roof") { 
                            else {
                                //For Section = Roof-Bottom Layer OR Roof OR Roof Liner
                                // Quantity:
                                quantity = 0;
                                if(containskey(roofBottomQuantityTotalDict, childTypeKey)) {
                                    quantity = get(roofBottomQuantityTotalDict, childTypeKey);
                                }
                                quantity = quantity + childLine.requestedQuantity_l;
                                put(roofBottomQuantityTotalDict, childTypeKey, quantity);
                                
                                // Quantity UOM:
                                if(NOT(containskey(roofBottomQuantityUOMDict, childTypeKey))) {
                                    put(roofBottomQuantityUOMDict, childTypeKey, childLine._part_custom_field15);
                                }
                                
                                // Total Net Amount:
                                amount = 0.0;
                                if(containskey(roofBottomAmtTotalDict, childTypeKey)) {
                                    amount = get(roofBottomAmtTotalDict, childTypeKey);
                                }
                                amount = amount + childLine.netAmount_l;
                                put(roofBottomAmtTotalDict, childTypeKey, amount);
                                
                                // Insulation Type
                                if(NOT(containskey(roofBottomInsulationTypeDict, childTypeKey))) {
                                    put(roofBottomInsulationTypeDict, childTypeKey, childLine.insulationType_l);
                                }
                                
                                // Facing
                                if(NOT(containskey(roofBottomFacingDict, childTypeKey))) {
                                    put(roofBottomFacingDict, childTypeKey, childLine.facing_l);
                                }

                                // new sept 23 2019
                                // Tab Type
                                if(NOT(containskey(roofBottomTabTypeDict, childTypeKey))) {
                                    put(roofBottomTabTypeDict, childTypeKey, childLine.tabType_l);
                                }

                                // Tab Option
                                if(NOT(containskey(roofBottomTabOptionDict, childTypeKey))) {
                                    put(roofBottomTabOptionDict, childTypeKey, childLine.tabOption_l);
                                }

                            }
                        } elif(startswith(upper(childLine.section_l), "ACCESSORYPART")) { 
                            //For Section=Accessories.
                            dashPos = find(childLine.section_l, "-");
                            accessoryKey = substring(childLine.section_l, dashPos+1);
                            if(startswith(upper(accessoryKey), "DOUBLE FACE TAPE") OR startswith(upper(accessoryKey), "DOUBLE FACED TAPE")) {
                                accessoryKey = "Double Faced Tape";
                            }
                            // Quantity:
                            quantity = 0;
                            if(containskey(accessoriesQuantityTotalDict, accessoryKey)) {
                                quantity = get(accessoriesQuantityTotalDict, accessoryKey);
                            }
                            quantity = quantity + childLine.requestedQuantity_l;
                            put(accessoriesQuantityTotalDict, accessoryKey, quantity);
                            
                            // Quantity UOM:
                            if(NOT(containskey(accessoriesUOMDict, accessoryKey))) {
                                uom = "";
                                if(childLine._part_custom_field15 <> "" AND NOT(isnull(childLine._part_custom_field15))) {
                                    uom = childLine._part_custom_field15;
                                }
                                put(accessoriesUOMDict, accessoryKey, uom);
                            }
                            
                            accessoriesPriceTotal = accessoriesPriceTotal + childLine.netAmount_l;
                        } elif(startswith(upper(childLine.section_l), "SIDEWALL") OR startswith(upper(childLine.section_l), "ENDWALL") OR startswith(upper(childLine.section_l), "COMBINEDWALLS")) {
                            // For Section = S/W1, S/W2, E/W1, E/W2, CombinedWalls.
                            
                            if(startswith(upper(childLine.section_l), "COMBINEDWALLS")) {
                                combinedWallsBool = true;
                            }
                            
                            // Quantity:
                            quantity = 0;
                            if(containskey(wallQuantityTotalDict, childTypeKey)) {
                                quantity = get(wallQuantityTotalDict, childTypeKey);
                            }
                            quantity = quantity + childLine.requestedQuantity_l;
                            put(wallQuantityTotalDict, childTypeKey, quantity);
                            
                            // Quantity UOM:
                            if(NOT(containskey(wallQuantityUOMDict, childTypeKey))) {
                                put(wallQuantityUOMDict, childTypeKey, childLine._part_custom_field15);
                            }
                            
                            // Total Net Amount
                            amount = 0.0;
                            if(containskey(wallAmtTotalDict, childTypeKey)) {
                                amount = get(wallAmtTotalDict, childTypeKey);
                            }
                            amount = amount + childLine.netAmount_l;
                            put(wallAmtTotalDict, childTypeKey, amount);
                            
                            // Insulation Type
                            if(NOT(containskey(wallInsulationTypeDict, childTypeKey))) {
                                put(wallInsulationTypeDict, childTypeKey, childLine.insulationType_l);
                            }
                            
                            // Facing
                            if(NOT(containskey(wallFacingDict, childTypeKey))) {
                                put(wallFacingDict, childTypeKey, childLine.facing_l);
                            }

                            // new nov 26 2019
                            // Tab Type
                            if(NOT(containskey(wallTabTypeDict, childTypeKey))) {
                                put(wallTabTypeDict, childTypeKey, childLine.tabType_l);
                            }

                            // Tab Option
                            if(NOT(containskey(wallTabOptionDict, childTypeKey))) {
                                put(wallTabOptionDict, childTypeKey, childLine.tabOption_l);
                            }
                        }
                    } else {
                        // Liner Systems
                        if(startswith(upper(childLine.section_l), "ROOF")) {
                            if(linerRoofInsulationTypeStr == "") {
                                linerRoofInsulationTypeStr = childLine.linerDesc_l;
                            }
                            if(startswith(upper(childLine.section_l), "ROOF-ACCESSORIES")) { 
                                //For Section=ROOF-ACCESSORIES.
                                section = "ROOF-ACCESSORIES-";
                                accessoryKey = substring(childLine.section_l, len(section));
                                
                                // Quantity:
                                quantity = 0;
                                if(containskey(linerRoofAccessoriesQuantityTotalDict, accessoryKey)) {
                                    quantity = get(linerRoofAccessoriesQuantityTotalDict, accessoryKey);
                                }
                                quantity = quantity + childLine.requestedQuantity_l;
                                put(linerRoofAccessoriesQuantityTotalDict, accessoryKey, quantity);
                                
                                // Quantity UOM:
                                if(NOT(containskey(linerRoofAccessoriesUOMDict, accessoryKey))) {
                                    uom = "";
                                    if(childLine._part_custom_field15 <> "" AND NOT(isnull(childLine._part_custom_field15))) {
                                        uom = childLine._part_custom_field15;
                                    }
                                    put(linerRoofAccessoriesUOMDict, accessoryKey, uom);
                                }
                                
                                linerRoofAccessoriesPriceTotal = linerRoofAccessoriesPriceTotal + childLine.netAmount_l;
                            } else {
                                // For Section = Roof-Top Layer or Roof-Bottom Layer
                                // Quantity:
                                // linerRoofQuantityTotal = linerRoofQuantityTotal + childLine.requestedQuantity_l;
                                if(linerRoofQuantityTotal == 0) {
                                    linerRoofQuantityTotal = childLine.requestedQuantity_l;
                                }
                                
                                // Total Net Amount:
                                linerRoofPriceTotal = linerRoofPriceTotal + childLine.netAmount_l;
                                   
                                // Insulation Type String
                                if(childLine._part_custom_field9 <> "" AND childLine._part_custom_field8 <> "") {
                                    descStr = childLine._part_custom_field9 + "\" " + "(" + childLine._part_custom_field8 + ")";
                                    if(findinarray(linerRoofDescArr,descStr) == -1) {
                                        append(linerRoofDescArr, descStr);
                                    }
                                }
                                //linerRoofInsulationTypeStr = linerRoofInsulationTypeStr + childLine.rollWidth_l + "\" " + "(" + childLine._part_custom_field8 + ")" + " + ";
                            }
                        } elif(startswith(upper(childLine.section_l), "SIDEWALL") OR startswith(upper(childLine.section_l), "ENDWALL") OR startswith(upper(childLine.section_l), "COMBINEDWALLS") OR startswith(upper(childLine.section_l), "WALLS")) {
                            // For Section = S/W1, S/W2, E/W1, E/W2, CombinedWalls or any of its liner.
                            if(linerWallInsulationTypeStr == "") {
                                linerWallInsulationTypeStr = childLine.linerDesc_l;
                            }
                            if(startswith(upper(childLine.section_l), "WALLS-ACCESSORIES")) { 
                                //For Section=WALLS-ACCESSORIES.
                                section = "WALLS-ACCESSORIES-";
                                accessoryKey = substring(childLine.section_l, len(section));
                                
                                // Quantity:
                                quantity = 0;
                                if(containskey(linerWallAccessoriesQuantityTotalDict, accessoryKey)) {
                                    quantity = get(linerWallAccessoriesQuantityTotalDict, accessoryKey);
                                }
                                quantity = quantity + childLine.requestedQuantity_l;
                                put(linerWallAccessoriesQuantityTotalDict, accessoryKey, quantity);
                                
                                // Quantity UOM:
                                if(NOT(containskey(linerWallAccessoriesUOMDict, accessoryKey))) {
                                    uom = "";
                                    if(childLine._part_custom_field15 <> "" AND NOT(isnull(childLine._part_custom_field15))) {
                                        uom = childLine._part_custom_field15;
                                    }
                                    put(linerWallAccessoriesUOMDict, accessoryKey, uom);
                                }
                                
                                linerWallAccessoriesPriceTotal = linerWallAccessoriesPriceTotal + childLine.netAmount_l;
                            } else {
                                // For Section = Walls / Side Walls / End Walls / Combined Walls
                                // Quantity:
                                // linerWallQuantityTotal = linerWallQuantityTotal + childLine.requestedQuantity_l;
                                if(linerWallQuantityTotal == 0) {
                                    linerWallQuantityTotal = childLine.requestedQuantity_l;
                                }
                                
                                // Total Net Amount:
                                linerWallPriceTotal = linerWallPriceTotal + childLine.netAmount_l;
                                
                                // Insulation Type String
                                if(childLine._part_custom_field9 <> "" AND childLine._part_custom_field8 <> "") {
                                    descStr = childLine._part_custom_field9 + "\" " + "(" + childLine._part_custom_field8 + ")";
                                    if(findinarray(linerWallDescArr,descStr) == -1) {
                                        append(linerWallDescArr, descStr);
                                    }
                                }
                                //linerWallInsulationTypeStr = linerWallInsulationTypeStr + childLine.rollWidth_l + "\" " + "(" + childLine._part_custom_field8 + ")" + " + ";                             
                            }
                        }
                    }
                }
            } 
		}
        
        // For Roof Liners
        if(sizeofarray(linerRoofDescArr) > 0) {
            linerRoofInsulationTypeStr = linerRoofInsulationTypeStr + " " + join(linerRoofDescArr, " + ");
        } else {
            // No Roof Liners.
            linerRoofInsulationTypeStr = "";
        }
        
        // For Wall Liners
        if(sizeofarray(linerWallDescArr) > 0) {
            linerWallInsulationTypeStr = linerWallInsulationTypeStr + " " + join(linerWallDescArr, " + ");
        } else {
            // No Wall Liners.
            linerWallInsulationTypeStr = "";
        }
        
        // Totalling the amounts and quantities to form the Building Totals
        // Roof bottom amt total
        roofBottomAmts = keys(roofBottomAmtTotalDict);        
        for each1 in roofBottomAmts {
            buildingTotalPrice = buildingTotalPrice + get(roofBottomAmtTotalDict, each1);
        }

        // Roof bottom qty total
        roofBottomQties = keys(roofBottomQuantityTotalDict);        
        for each1 in roofBottomQties {
            buildingTotalQty = buildingTotalQty + get(roofBottomQuantityTotalDict, each1);
        }
        
        // Roof top amt total
        roofTopAmts = keys(roofTopAmtTotalDict);
        for each2 in roofTopAmts {
            buildingTotalPrice = buildingTotalPrice + get(roofTopAmtTotalDict, each2);
        }

        // Roof top qty total
        roofTopQties = keys(roofTopQuantityTotalDict);
        for each2 in roofTopQties {
            buildingTotalQty = buildingTotalQty + get(roofTopQuantityTotalDict, each2);
        }
        
        // Wall amt total
        wallAmts = keys(wallAmtTotalDict);
        for each3 in wallAmts {
            buildingTotalPrice = buildingTotalPrice + get(wallAmtTotalDict, each3);
        }

        // Wall qty total
        wallQties = keys(wallQuantityTotalDict);
        for each3 in wallQties {
            buildingTotalQty = buildingTotalQty + get(wallQuantityTotalDict, each3);
        }

        buildingTotalPrice = buildingTotalPrice + accessoriesPriceTotal;
        
        buildingTotalPrice = buildingTotalPrice + linerRoofPriceTotal + linerWallPriceTotal + linerRoofAccessoriesPriceTotal + linerWallAccessoriesPriceTotal;
        buildingTotalQty   = buildingTotalQty +  linerRoofQuantityTotal + linerWallQuantityTotal;
        
        /*print "buildingDesc: " + buildingDesc; print "";
        print "roofQuantityTotalDict: "; print roofQuantityTotalDict; print "";
        print "roofQuantityUOMDict: "; print roofQuantityUOMDict; print "";
        print "roofUnitPriceDict: "; print roofUnitPriceDict; print "";
        print "roofUnitPriceUOMDict: "; print roofUnitPriceUOMDict; print "";
        print "roofAmtTotalDict: "; print roofAmtTotalDict; print "";
        print "wallQuantityTotalDict: "; print wallQuantityTotalDict; print "";
        print "wallQuantityUOMDict: "; print wallQuantityUOMDict; print "";
        print "wallUnitPriceDict: "; print wallUnitPriceDict; print "";
        print "wallUnitPriceUOMDict: "; print wallUnitPriceUOMDict; print "";
        print "wallAmtTotalDict: "; print wallAmtTotalDict; print "";
        print "accessories: " + accessories; print "";
        print "buildingTotalPrice: " + string(buildingTotalPrice); print "";*/
        
        // Forming the return string for each building       
        retStr = retStr + upper(buildingLabel) + rowDelim;
        roofBottomKeys = keys(roofBottomQuantityTotalDict);
        roofTopKeys = keys(roofTopQuantityTotalDict);
        for eachBottomRoof in roofBottomKeys {
            splitRoofDet = split(eachBottomRoof, "_");
            splitRoofDetArrSize = sizeofarray(splitRoofDet);
            tempStr = "ROOF BOTTOM";
            if((sizeofarray(roofTopKeys) == 0) OR (splitRoofDetArrSize == 1)) {
                tempStr = "ROOF";
            }
            retStr = retStr + tempStr + colDelim; // Col 1
            if(splitRoofDetArrSize > 1) { 
                // If it is a Roll.
                retStr = retStr + splitRoofDet[0] + "\"" + " [" + splitRoofDet[1] + "] " +  get(roofBottomInsulationTypeDict, eachBottomRoof) + colDelim; // Col 2: Insulation & Systems
                tab = splitRoofDet[2];
                /*if(tab == "") {
                    tab = "Long";
                } else {
                    tab = tab + "\"";
                }*/
                if(tab <> "") {
                    // new sept 23 2019
                    insulationType = get(roofBottomInsulationTypeDict, eachBottomRoof);
                    tabType = get(roofBottomTabTypeDict, eachBottomRoof);
                    tabOption = get(roofBottomTabOptionDict, eachBottomRoof);
                    if ((insulationType == "Standard MBI System" OR insulationType == "Sag and Bag System") AND tabType == "Tape Tab") {
                        tab = " [" + tab + "\" ";
                        if (tabOption == "1-6\" Single Tape Tab (SI)") {
                            tab = tab + "Tape Tab (SI)]";
                        } elif (tabOption == "2-3\" Single Tape Tab (SO)") {
                            tab = tab + "Tape Tab (SO)]";
                        } elif (tabOption == "2-3\" Double Tape Tabs (DO)") {
                            tab = tab + "Tape Tabs (DO)]";
                        } elif (tabOption == "2-3\" Double Tape Tabs (DI)") {
                            tab = tab + "Tape Tabs (DI)]";
                        } else {
                            tab = tab + "Tab]";
                        }
                    } else {
                        tab = " [" + tab + "\" Tab]";
                    }
                }
                // retStr = retStr + get(roofBottomFacingDict, eachBottomRoof) + " [" + tab + " Tab]" + colDelim; // Col 3: Facing
                retStr = retStr + get(roofBottomFacingDict, eachBottomRoof) + tab + colDelim; // Col 3: Facing
            } else {
                // If it is a liner.
                retStr = retStr + splitRoofDet[0] + colDelim; // Col 2: Part Number
                retStr = retStr + " " + colDelim; // Col 3: Facing will be empty
            } 
            formattedRoofBottomQty = util.numberFormat(get(roofBottomQuantityTotalDict, eachBottomRoof), FORMATTED_QTY_PLACES, FORMATTED_QTY_THOU_SEP, ".");
            retStr = retStr + formattedRoofBottomQty + " " + get(roofBottomQuantityUOMDict, eachBottomRoof) + colDelim; // Col 4: Quantity
            retStr = retStr + formatascurrency(get(roofBottomAmtTotalDict, eachBottomRoof), currency_t) + colDelim + rowDelim; // Col 5: Total
        }
        
        for eachTopRoof in roofTopKeys {
            splitRoofDet = split(eachTopRoof, "_");
            tempStr = "ROOF TOP";
            if(sizeofarray(roofBottomKeys) == 0) {
                tempStr = "ROOF";
            }
            retStr = retStr + tempStr + colDelim; // Col 1
            retStr = retStr + splitRoofDet[0] + "\"" + " [" + splitRoofDet[1] + "] " +  get(roofTopInsulationTypeDict, eachTopRoof) + colDelim; // Col 2: Insulation & Systems
            tab = splitRoofDet[2];
            /*if(tab == "") {
                tab = "Long";
            } else {
                tab = tab + "\"";
            }*/
            if(tab <> "") {
                tab = " [" + tab + "\" Tab]";
            }
            // retStr = retStr + get(roofTopFacingDict, eachTopRoof) + " [" + tab + " Tab]" + colDelim; // Col 3: Facing
            // new sept 23 2019
            insulationType = get(roofTopInsulationTypeDict, eachTopRoof);
            if (insulationType == "Long Tab Banded System" OR insulationType == "Sag and Bag System") {
                retStr = retStr + "UNFACED" + colDelim; // Col 3: Facing
            } else {
                retStr = retStr + get(roofTopFacingDict, eachTopRoof) + tab + colDelim; // Col 3: Facing
            }
            formattedRoofTopQuantity = util.numberFormat(get(roofTopQuantityTotalDict, eachTopRoof), FORMATTED_QTY_PLACES, FORMATTED_QTY_THOU_SEP, ".");
            retStr = retStr + formattedRoofTopQuantity + " " + get(roofTopQuantityUOMDict, eachTopRoof) + colDelim; // Col 4: Quantity
            retStr = retStr + formatascurrency(get(roofTopAmtTotalDict, eachTopRoof), currency_t) + colDelim + rowDelim; // Col 5: Total
        }
        
        // Liner Roof
        if(linerRoofInsulationTypeStr <> "") {
            retStr = retStr + "LINER_ROOF" + colDelim + linerRoofInsulationTypeStr + colDelim;
            formattedLinerRoofQtyTotal = util.numberFormat(linerRoofQuantityTotal, FORMATTED_QTY_PLACES, FORMATTED_QTY_THOU_SEP, ".");
            retStr = retStr + formattedLinerRoofQtyTotal + " SF" + colDelim;
            retStr = retStr + formatascurrency(linerRoofPriceTotal, currency_t) + colDelim;
            retStr = retStr + rowDelim;
            retStr = retStr + "LINER-MAND-ACC_" + linerMandatoryAccessories + rowDelim;
            roofLinerAccessoryKey = keys(linerRoofAccessoriesQuantityTotalDict);
            if(sizeofarray(roofLinerAccessoryKey) > 0) {
                linerRoofAccessoryStr = linerOptionalAccessories;
                for eachAcc in roofLinerAccessoryKey {
                    linerRoofAccessoryStr = linerRoofAccessoryStr + "(" + string(get(linerRoofAccessoriesQuantityTotalDict,eachAcc)) + ") " + get(linerRoofAccessoriesUOMDict,eachAcc) + " " + eachAcc + ", ";
                }
                if(endswith(linerRoofAccessoryStr, ", ")) {
                    linerRoofAccessoryStr = substring(linerRoofAccessoryStr, 0, -2);
                }
                retStr = retStr + "LINER-OPT-ACC_" + linerRoofAccessoryStr + colDelim + formatascurrency(linerRoofAccessoriesPriceTotal, currency_t) + colDelim;
                retStr = retStr + rowDelim;
            }
            retStr = retStr + "LINER-MAND-ACC_" + rowDelim; // For an empty line after Roof Liner.
        }
        
        wallKeys = keys(wallQuantityTotalDict);
        for eachWall in wallKeys {
            splitWallDet = split(eachWall, "_");
            tempStr = "WALLS";
            if(combinedWallsBool) { tempStr = "COMBINED WALLS"; }
            retStr = retStr + tempStr + colDelim; // Col 1
            if(sizeofarray(splitWallDet) > 1) {
                // If it is a Roll.
                retStr = retStr + splitWallDet[0] + "\"" + " [" + splitWallDet[1] + "] " +  get(wallInsulationTypeDict, eachWall) + colDelim; // Col 2: Insulation & Systems
                tab = splitWallDet[2];
                /*if(tab == "") {
                    tab = "Long";
                } else {
                    tab = tab + "\"";
                }*/
                if(tab <> "") {
                    // new sept 23 2019
                    insulationType = get(wallInsulationTypeDict, eachWall);
                    tabType = get(wallTabTypeDict, eachWall);
                    tabOption = get(wallTabOptionDict, eachWall);
                    if ((insulationType == "Standard MBI System" OR insulationType == "Sag and Bag System") AND tabType == "Tape Tab") {
                        tab = " [" + tab + "\" ";
                        if (tabOption == "1-6\" Single Tape Tab (SI)") {
                            tab = tab + "Tape Tab (SI)]";
                        } elif (tabOption == "2-3\" Single Tape Tab (SO)") {
                            tab = tab + "Tape Tab (SO)]";
                        } elif (tabOption == "2-3\" Double Tape Tabs (DO)") {
                            tab = tab + "Tape Tabs (DO)]";
                        } elif (tabOption == "2-3\" Double Tape Tabs (DI)") {
                            tab = tab + "Tape Tabs (DI)]";
                        } else {
                            tab = tab + "Tab]";
                        }
                    } else {
                        tab = " [" + tab + "\" Tab]";
                    }
                }
                /*if(tab <> "") {
                    tab = " [" + tab + "\" Tab]";
                }*/
                // retStr = retStr + get(wallFacingDict, eachWall) + " [" + tab + " Tab]" + colDelim; // Col 3: Facing
                retStr = retStr + get(wallFacingDict, eachWall) + tab + colDelim; // Col 3: Facing
            } else {
                // If it is a Liner.
                retStr = retStr + splitWallDet[0] + colDelim; // Col 2: Integration Part Number
                retStr = retStr + " " + colDelim; // Col 3: Facing will be blank.
            }
            formattedWallQty = util.numberFormat(get(wallQuantityTotalDict, eachWall), FORMATTED_QTY_PLACES, FORMATTED_QTY_THOU_SEP, ".");
            retStr = retStr + formattedWallQty + " " + get(wallQuantityUOMDict, eachWall) + colDelim; // Col 4: Quantity
            retStr = retStr + formatascurrency(get(wallAmtTotalDict, eachWall), currency_t) + colDelim + rowDelim; // Col 5: Total
        }
        
        // Liner Wall
        if(linerWallInsulationTypeStr <> "") {
            retStr = retStr + "LINER_WALLS" + colDelim + linerWallInsulationTypeStr + colDelim;
            formattedLinerWallQtyTotal = util.numberFormat(linerWallQuantityTotal, FORMATTED_QTY_PLACES, FORMATTED_QTY_THOU_SEP, ".");
            retStr = retStr + formattedLinerWallQtyTotal + " SF" + colDelim;
            retStr = retStr + formatascurrency(linerWallPriceTotal, currency_t) + colDelim;
            retStr = retStr + rowDelim;
            retStr = retStr + "LINER-MAND-ACC_" + linerMandatoryAccessories + rowDelim;
            wallLinerAccessoryKey = keys(linerWallAccessoriesQuantityTotalDict);
            if(sizeofarray(wallLinerAccessoryKey) > 0) {
                linerWallAccessoryStr = linerOptionalAccessories;
                for eachAcc in wallLinerAccessoryKey {
                    linerWallAccessoryStr = linerWallAccessoryStr + "(" + string(get(linerWallAccessoriesQuantityTotalDict,eachAcc)) + ") " + get(linerWallAccessoriesUOMDict,eachAcc) + " " + eachAcc + ", ";
                }
                if(endswith(linerWallAccessoryStr, ", ")) {
                    linerWallAccessoryStr = substring(linerWallAccessoryStr, 0, -2);
                }
                retStr = retStr + "LINER-OPT-ACC_" + linerWallAccessoryStr + colDelim + formatascurrency(linerWallAccessoriesPriceTotal, currency_t) + colDelim;
                retStr = retStr + rowDelim;
            }
        }
        
        accKeys = keys(accessoriesQuantityTotalDict);
        if(sizeofarray(accKeys) > 0) {
            accessories = "Accessories: ";        
            for eachAcc in accKeys {
                accessories = accessories + " ["+ string(get(accessoriesQuantityTotalDict, eachAcc)) + "] " + get(accessoriesUOMDict, eachAcc) + " " + eachAcc + ", ";
            }            
            if(endswith(accessories, ", ")) {
                accessories = substring(accessories, 0, -2);
                accessories = accessories + ".";
            }
            accessories = accessories + colDelim + formatascurrency(accessoriesPriceTotal, currency_t) + colDelim;
            retStr = retStr + accessories + rowDelim;
        }
        
        formattedBuildingTotalQty = util.numberFormat(buildingTotalQty, FORMATTED_QTY_PLACES, FORMATTED_QTY_THOU_SEP, ".");
        retStr = retStr + "TOTAL" + colDelim + formattedBuildingTotalQty + " SF" + colDelim + formatascurrency(buildingTotalPrice, currency_t) + rowDelim;
        retStr = retStr + builidingDelim;
	} // elif((buildingLine._parent_doc_number == "" OR isnull(buildingLine._parent_doc_number)) AND upper(buildingLine._part_desc) <> "FREIGHT" AND buildingLine._model_variable_name=="" AND buildingLine.integrationPartNumber_l<>"" AND NOT(isnull(buildingLine.integrationPartNumber_l))) {
    elif((buildingLine._parent_doc_number == "" OR isnull(buildingLine._parent_doc_number)) AND upper(buildingLine._part_desc) <> "FREIGHT" AND buildingLine._model_variable_name=="") {
        // For the parts added through quick key to be grouped under Miscellaneous 
        childTypeKey = buildingLine.integrationPartNumber_l;            
        
        quantity = 0;
        if(containskey(miscQuantityTotalDict, childTypeKey)) {
            quantity = get(miscQuantityTotalDict, childTypeKey);
        }
        quantity = quantity + buildingLine.requestedQuantity_l;
        put(miscQuantityTotalDict, childTypeKey, quantity);
        
        // Quantity UOM:
        if(NOT(containskey(miscQuantityUOMDict, childTypeKey))) {
            put(miscQuantityUOMDict, childTypeKey, buildingLine._part_custom_field15);
        }
        
        // Total Net Amount
        amount = 0.0;
        if(containskey(miscAmtTotalDict, childTypeKey)) {
            amount = get(miscAmtTotalDict, childTypeKey);
        }
        amount = amount + buildingLine.netAmount_l;
        put(miscAmtTotalDict, childTypeKey, amount);
        
        // Adhoc Part Description: Part Number + Part Description
        if(NOT(containskey(miscDescriptionDict, childTypeKey))) {
            put(miscDescriptionDict, childTypeKey, "MISC_"+buildingLine._part_display_number+": "+buildingLine._part_desc);
        }
    }
}

// Forming the string for the Miscellaneous group.
miscKeys = keys(miscQuantityTotalDict);
if(sizeofarray(miscKeys) > 0) {
    retStr = retStr + "MISCELLANEOUS" + rowDelim;
    for eachMisc in miscKeys {
        formattedMiscQty = util.numberFormat(get(miscQuantityTotalDict,eachMisc), FORMATTED_QTY_PLACES, FORMATTED_QTY_THOU_SEP, ".");
        retStr = retStr + get(miscDescriptionDict,eachMisc) + colDelim; // Description.
        retStr = retStr + formattedMiscQty + " " + get(miscQuantityUOMDict,eachMisc) + colDelim; // Quantity.
        retStr = retStr + formatascurrency(get(miscAmtTotalDict,eachMisc), currency_t) + colDelim + rowDelim; // Total Amount.
    }
    retStr = retStr + builidingDelim;
}

return retStr;
```

## Revision History
 Rev. Date |Developer            |Notes / Comments
 ----------|---------------------|-------------------------------------------------------------------------
 2019-11-14|Jserna               |Updates per Phase II workshop
 2019-11-26|Jserna               |SP-46 - Tab type condition not evaluating to true
 2019-11-26|Jserna               |SP-46 Adding Tab type logic for Walls per Greg's feedback
