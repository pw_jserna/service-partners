# Welcome to CPQ Site Documentation

## Functions in Module
<!-- Index Start -->
### commerce


### oraclecpqo


### LibFuncs

* [Building Accessory String Construct](commerce\oraclecpqo\LibFuncs/buildingAccessoryStringConstruct.md) - Returns the code delimited string to form the Accessory Table under the Quote Summary table of the Quote Summary output Document.
* [Building Item Breakdown String Construct](commerce\oraclecpqo\LibFuncs/buildingItemBreakdownStringConstruct.md) - Returns the code delimited string to form the Quote Summary table of the Quote Summary output Document
* [Create Order](commerce\oraclecpqo\LibFuncs/createOrder.md) - Submit the Order Payload to JDE API
* [Format Attribute Values For PDF Output](commerce\oraclecpqo\LibFuncs/formatAttributeValuesForPDFOutput.md) - Format Attribute Values as required for the quote output document.
* [Invoke modify action](commerce\oraclecpqo\LibFuncs/invokeModifyAction.md)
* [Order Lines HTML](commerce\oraclecpqo\LibFuncs/orderLinesHTML.md) - returns tabular display of Order Lines as html string
* [Pre Formula Pricing](commerce\oraclecpqo\LibFuncs/preFormulaPricing.md) - pricing Script sets base line level attribute values by iterating through the lines in the transaction.
* [Set bottom line discounts](commerce\oraclecpqo\LibFuncs/setBottomLineDiscounts.md)
* [Set customer account defaults](commerce\oraclecpqo\LibFuncs/setCustomerAccountDefaults.md)
* [Set Line Tab Descriptions](commerce\oraclecpqo\LibFuncs/setLineTabDescriptions.md)
* [Store building discounts](commerce\oraclecpqo\LibFuncs/storeBuildingDiscounts.md) - Stores building discounts applied by the user.

### utils

* [Cap First Letter Of Word](utils/capFirstLetterOfWord.md) - Capitalize the first letter and lower rest of the letters for each word in the input string
* [Construct Building Rec Item String](utils/constructBuildingRecItemString.md)
* [External customer quote access](utils/externalCustomerQuoteAccess.md) - Determines whether a quote should be forwarded to an external customer.
* [Float to formatted currency](utils/floatToFormattedCurrency.md)
* [Get account details](utils/getAccountDetails.md) - Calls CPQ internal rest service to retrieve accounts based on provided query params.
* [Get line tab description](utils/getLineTabDescription.md) - Gets the appropriate tab description for a provided insulation type, tab type, and tab option.
* [Get tab description](utils/getTabDescription.md) - Gets the appropriate tab description for a provided insulation type, tab type, and tab option.
 

* [Get tab descriptions](utils/getTabDescriptions.md)
* [List Part Details](utils/listPartDetails.md) - return a JSON data for the list of input part numbers by looking up the parts db
<!-- Index End -->