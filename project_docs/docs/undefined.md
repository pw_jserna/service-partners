# undefined



## Syntax
```
undefined();
```

## Returns
Type|Description
-|-
``````|





## Revision History
 Rev. Date |Developer            |Notes / Comments
 ----------|---------------------|-------------------------------------------------------------------------
 2020-03-10|                     |
